using GameBrains.Extensions.MonoBehaviours;
using GameBrains.Fuzzy;
using UnityEngine;

namespace Testing
{
    public class W27TestFuzzySets : ExtendedMonoBehaviour
    {
        #region Members and Properties

        [Header("Tests Fuzzy Sets")] 
        [SerializeField] bool testFuzzySetTriangle;
        [SerializeField] bool testFuzzySetSingleton;
        [SerializeField] bool testFuzzySetLeftShoulder;
        [SerializeField] bool testFuzzySetRightShoulder;
        
        // TODO: Complete unfinished testing.
        
        [Header("Tests Fuzzy Operators")] 
        [SerializeField] bool testFzSet;
        
        [Header("Tests Fuzzy Operators")] 
        [SerializeField] bool testFzAND;
        [SerializeField] bool testFzOR;
        [SerializeField] bool testFzFairly;
        [SerializeField] bool testFzVery;
        
        [Header("Tests Other Stuff")] 
        [SerializeField] bool testOtherStuff;

        #endregion Members and Properties

        #region Update

        public override void Update()
        {
            base.Update();

            #region Test Fuzzy Sets

            #region Test Fuzzy Set Triangle

            if (testFuzzySetTriangle)
            {
                testFuzzySetTriangle = false;

                var dumb = new FuzzySetTriangle(80f, 80f, 20f);

                for (int testPoint = 0; testPoint <= 140; testPoint += 5)
                {
                    Log.Debug($"Dumb DOM at {testPoint} is {dumb.CalculateDom(testPoint)}");
                }
                
                var average = new FuzzySetTriangle(100f, 20f, 20f);

                for (int testPoint = 0; testPoint <= 140; testPoint += 5)
                {
                    Log.Debug($"Average DOM at {testPoint} is {average.CalculateDom(testPoint)}");
                }
                
                var clever = new FuzzySetTriangle(120f, 20f, 20f);

                for (int testPoint = 0; testPoint <= 140; testPoint += 5)
                {
                    Log.Debug($"Clever DOM at {testPoint} is {clever.CalculateDom(testPoint)}");
                }
            }

            #endregion Test Fuzzy Set Triangle

            #region Test Fuzzy Set Singleton

            if (testFuzzySetSingleton)
            {
                testFuzzySetSingleton = false;
                
                var thresholdForA = new FuzzySetSingleton(80f, 0f, 0f);
                
                for (int testPoint = 0; testPoint <= 100; testPoint += 10)
                {
                    Log.Debug($"ThresholdForA DOM at {testPoint} is {thresholdForA.CalculateDom(testPoint)}");
                }
            }

            #endregion Test Fuzzy Set Singleton
            
            #region Test Fuzzy Set Left Shoulder

            if (testFuzzySetLeftShoulder)
            {
                testFuzzySetLeftShoulder = false;
                
                var timeToLive = new FuzzySetLeftShoulder(50f, 50f, 50f);
                
                for (int testPoint = 0; testPoint <= 100; testPoint += 10)
                {
                    Log.Debug($"TimeToLive DOM at {testPoint} is {timeToLive.CalculateDom(testPoint)}");
                }
            }

            #endregion Test Fuzzy Set Left Shoulder
            
            #region Test Fuzzy Set Right Shoulder

            if (testFuzzySetRightShoulder)
            {
                testFuzzySetRightShoulder = false;
                
                var timeToRespawn = new FuzzySetRightShoulder(60f, 30f, 40f);
                
                for (int testPoint = 0; testPoint <= 100; testPoint += 10)
                {
                    Log.Debug($"TimeToRespawn DOM at {testPoint} is {timeToRespawn.CalculateDom(testPoint)}");
                }
            }

            #endregion Test Fuzzy Set Right Shoulder

            #endregion Test Fuzzy Sets
            
            #region Test FzSet

            if (testFzSet)
            {
                testFzSet = false;

                var dumb = new FuzzySetTriangle(80f, 80f, 20f);

                var fzSetDumb = new FzSet(dumb);
                
                var average = new FuzzySetTriangle(100f, 20f, 20f);
                
                var fzSetAverage = new FzSet(average);

                var fzAnd = new FzAnd(fzSetDumb, fzSetAverage);

                var dom = fzAnd.GetDom();
                
                // TODO: This doesn't accomplish anything?
                Log.Debug($"fzAnd.GetDom is {dom}");
            }

            #endregion Test FzSet
            
            #region Test Fz Operators
            
            #endregion Test Fz Operators
        }

        #endregion Update
    }
}