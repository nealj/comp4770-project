using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Entities.Types;
using GameBrains.Extensions.MonoBehaviours;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace Testing
{
    public sealed class W29TestGoalOrientedBehaviour : ExtendedMonoBehaviour
    {
        #region Members and Properties

        [Header("Options")]
        [SerializeField] bool showPath = true;
        [SerializeField] bool smoothPath;
        [SerializeField] bool showClosestNodeVisualizer;
        [SerializeField] bool showClosestToEntityWithTypeVisualizer;
        [SerializeField] bool showDirectPathVisualizer;

        [Header("Respawn")]
        [SerializeField] bool respawnPathfinder;
        [SerializeField] VectorXYZ spawnPointPathfinder;
        [SerializeField] bool respawnThinker;
        [SerializeField] VectorXYZ spawnPointThinker;

        [Header("Thinker Tests", order = 0)]
        [Space(order = 1)]

        [Header("Test Move to Location", order = 2)]
        [SerializeField] bool testMoveToLocation;
        [SerializeField] VectorXZ moveToDestination;

        [Header("Test Move to Random Location")]
        [SerializeField] bool testMoveToRandomLocation;

        [Header("Test Seek to Location")]
        [SerializeField] bool testSeekToLocation;
        [SerializeField] VectorXZ seekToDestination;

        [Header("Test Arrive to Location")]
        [SerializeField] bool testArriveAtLocation;
        [SerializeField] VectorXZ arriveAtDestination;

        [Header("Test Strafe")]
        [SerializeField] bool testStrafe;

        [Header("Test Explore")]
        [SerializeField] bool testExplore;

        [Header("Test Get Entity of Type")]
        [SerializeField] bool testGetEntityWithTypes;
        [SerializeField] EntityTypes entityTypesToGet;

        [Header("Test Hunt Target")]
        [SerializeField] bool testHuntTarget;

        [Header("Test Attack Target")]
        [SerializeField] bool testAttackTarget;

        [Header("Test Adjust Range")]
        [SerializeField] bool testAdjustRange;
        [SerializeField] float idealRange = 20f;

        [Header("References")]
        [SerializeField] ThinkingAgent thinkingAgent;
        ThinkingData thinkingData;
        [SerializeField] PathfindingAgent pathfindingAgent;
        PathfindingData pathfindingData;
        [SerializeField] PathPlanner pathPlanner;

        #endregion Members and Properties

        #region Update

        public override void Update()
        {
            base.Update();
            
            #region Set Up

            if (pathfindingAgent != null)
            {
                pathfindingAgent.verbosity = verbosity;
                pathfindingData = pathfindingAgent.Data;
                pathfindingData.verbosity = (ExtendedScriptableObject.VerbosityStates)verbosity;
                pathfindingData = pathfindingAgent.Data;
                pathfindingData.ShowPath = showPath;
                pathfindingData.SmoothPath = smoothPath;
                pathfindingData.ShowClosestNodeVisualizer = showClosestNodeVisualizer;
                pathfindingData.ShowClosestToEntityWithTypeVisualizer = showClosestToEntityWithTypeVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: PathfindingAgent not set.");
                return;
            }

            if (pathPlanner != null)
            {
                pathPlanner.ShowDirectPathVisualizer = showDirectPathVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: PathPlanner not set.");
                return;
            }

            if (thinkingAgent != null)
            {
                thinkingAgent.verbosity = verbosity;
                thinkingData = thinkingAgent.Data;
                thinkingData.verbosity = (ExtendedScriptableObject.VerbosityStates)verbosity;
                thinkingData.ShowPath = showPath;
                thinkingData.SmoothPath = smoothPath;
                thinkingData.ShowClosestNodeVisualizer = showClosestNodeVisualizer;
                thinkingData.ShowClosestToEntityWithTypeVisualizer = showClosestToEntityWithTypeVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: ThinkingAgent not set.");
                return;
            }

            #endregion Set Up
            
            #region Respawn

            if (respawnPathfinder)
            {
                respawnPathfinder = false;
                pathfindingAgent.Spawn(spawnPointPathfinder);
            }

            if (respawnThinker)
            {
                respawnThinker = false;
                thinkingAgent.Spawn(spawnPointThinker);
            }

            #endregion Respawn

            #region Thinker Tests

            if (testMoveToLocation)
            {
                testMoveToLocation = false;

                thinkingData.Brain.Terminate();
                var subgoal = MoveToLocation.CreateInstance(thinkingAgent, moveToDestination);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }
            
            if (testMoveToRandomLocation)
            {
                testMoveToRandomLocation = false;

                thinkingData.Brain.Terminate();
                var location = new VectorXZ(Random.Range(-49, 49), Random.Range(-36, 36));
                var subgoal = MoveToLocation.CreateInstance(thinkingAgent, location);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testSeekToLocation)
            {
                testSeekToLocation = false;

                thinkingData.Brain.Terminate();
                var subgoal = SeekToLocation.CreateInstance(thinkingAgent, seekToDestination);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testArriveAtLocation)
            {
                testArriveAtLocation = false;

                thinkingData.Brain.Terminate();
                var subgoal = ArriveAtLocation.CreateInstance(thinkingAgent, arriveAtDestination);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testStrafe)
            {
                testStrafe = false;

                thinkingData.Brain.Terminate();
                var subgoal = Strafe.CreateInstance(thinkingAgent, pathfindingData);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testExplore)
            {
                testExplore = false;

                thinkingData.Brain.Terminate();
                var subgoal = Explore.CreateInstance(thinkingAgent);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testGetEntityWithTypes)
            {
                testGetEntityWithTypes = false;

                thinkingData.Brain.Terminate();
                var subgoal = GetEntityWithTypes.CreateInstance(thinkingAgent, entityTypesToGet);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testHuntTarget)
            {
                testHuntTarget = false;

                thinkingData.Brain.Terminate();
                var subgoal = HuntTarget.CreateInstance(thinkingAgent);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testAttackTarget)
            {
                testAttackTarget = false;

                thinkingData.Brain.Terminate();
                var subgoal = AttackTarget.CreateInstance(thinkingAgent);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testAdjustRange)
            {
                testAdjustRange = false;

                thinkingData.Brain.Terminate();
                var subgoal = AdjustRange.CreateInstance(thinkingAgent, pathfindingData, idealRange);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            #endregion Thinker Tests
        }

        #endregion Update
    }
}