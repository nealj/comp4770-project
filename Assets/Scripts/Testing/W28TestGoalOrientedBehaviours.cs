using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Entities.Types;
using GameBrains.EventSystem;
using GameBrains.Extensions.MonoBehaviours;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;
using GameBrains.Motion.Steering.VelocityBased;
using GameBrains.Visualization;
using UnityEngine;

namespace Testing
{
    public class W28TestGoalOrientedBehaviours : ExtendedMonoBehaviour
    {
        #region Members and Properties
        
        [Header("Options")]
        [SerializeField] bool showPath = true;
        [SerializeField] bool smoothPath;
        [SerializeField] bool showClosestNodeVisualizer = true;
        [SerializeField] bool showClosestToEntityWithTypeVisualizer = true;
        [SerializeField] bool showDirectPathVisualizer = true;
        
        [Header("Respawn")]
        [SerializeField] bool respawnPathfinder;
        [SerializeField] VectorXYZ spawnPointPathfinder;
        [SerializeField] bool respawnThinker;
        [SerializeField] VectorXYZ spawnPointThinker;

        [Header("Can Move To Test", order = 0)]
        [Space(order = 1)]

        [Header("Test Can Move To", order = 2)]
        [SerializeField] bool testCanMoveTo;
        [SerializeField] VectorXZ canMoveToDestination;
        [SerializeField] float castRadius = 1f;

        [Header("Pathfinder Tests", order = 0)]
        [Space(order = 1)]
        
        [Header("Test Path to Location", order = 2)]
        [SerializeField] bool testPathToLocation;
        [SerializeField] VectorXZ destination;
        
        [Header("Test Path to Entity with Type")]
        [SerializeField] bool testPathToEntityWithTypes;
        [SerializeField] EntityTypes entityTypes;
        
        [Header("Test Path to Random Location")]
        [SerializeField] bool testPathToRandomLocation;
        
        [Header("Thinker Tests", order = 0)]
        [Space(order = 1)]

        [Header("Test Move to Location", order = 2)]
        [SerializeField] bool testMoveToLocation;
        [SerializeField] VectorXZ moveToDestination;

        [Header("Test Move to Random Location")]
        [SerializeField] bool testMoveToRandomLocation;

        [Header("Test Seek to Location")]
        [SerializeField] bool testSeekToLocation;
        [SerializeField] VectorXZ seekToDestination;

        [Header("Test Arrive to Location")]
        [SerializeField] bool testArriveAtLocation;
        [SerializeField] VectorXZ arriveAtDestination;

        [Header("Test Splice Path")]
        [SerializeField] bool testSplicePath;
        [SerializeField] VectorXZ splicePathDestination;
        
        [Header("References")]
        [SerializeField] ThinkingAgent thinkingAgent;
        ThinkingData thinkingData;
        [SerializeField] PathfindingAgent pathfindingAgent;
        PathfindingData pathfindingData;
        [SerializeField] PathPlanner pathPlanner;
        Seek seek;
        Path path;
        CapsuleCastVisualizer capsuleCastVisualizer;

        #endregion Members and Properties

        #region Update

        public override void Update()
        {
            base.Update();

            #region Set Up

            if (pathfindingAgent != null)
            {
                pathfindingAgent.verbosity = verbosity;
                pathfindingData = pathfindingAgent.Data;
                pathfindingData.verbosity = (ExtendedScriptableObject.VerbosityStates)verbosity;
                pathfindingData = pathfindingAgent.Data;
                pathfindingData.ShowPath = showPath;
                pathfindingData.SmoothPath = smoothPath;
                pathfindingData.ShowClosestNodeVisualizer = showClosestNodeVisualizer;
                pathfindingData.ShowClosestToEntityWithTypeVisualizer = showClosestToEntityWithTypeVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: PathfindingAgent not set.");
                return;
            }

            if (pathPlanner != null)
            {
                pathPlanner.ShowDirectPathVisualizer = showDirectPathVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: PathPlanner not set.");
                return;
            }

            if (thinkingAgent != null)
            {
                thinkingAgent.verbosity = verbosity;
                thinkingData = thinkingAgent.Data;
                thinkingData.verbosity = (ExtendedScriptableObject.VerbosityStates)verbosity;
                thinkingData.ShowPath = showPath;
                thinkingData.SmoothPath = smoothPath;
                thinkingData.ShowClosestNodeVisualizer = showClosestNodeVisualizer;
                thinkingData.ShowClosestToEntityWithTypeVisualizer = showClosestToEntityWithTypeVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: ThinkingAgent not set.");
                return;
            }

            #endregion Set Up

            #region Respawn

            if (respawnPathfinder)
            {
                respawnPathfinder = false;
                pathfindingAgent.Spawn(spawnPointPathfinder);
            }

            if (respawnThinker)
            {
                respawnThinker = false;
                thinkingAgent.Spawn(spawnPointThinker);
            }

            #endregion Respawn

            #region Test Can Move To

            if (testCanMoveTo)
            {
                testCanMoveTo = false;

                if (capsuleCastVisualizer == null)
                {
                    capsuleCastVisualizer
                        = ScriptableObject.CreateInstance<CapsuleCastVisualizer>();
                }

                bool clear = thinkingData.CanMoveTo(
                    (VectorXYZ)canMoveToDestination,
                    capsuleCastVisualizer,
                    true,
                    false,
                    castRadius);

                if (VerbosityDebug)
                {
                    Log.Debug($"CanMoveTo is {(clear ? "clear" : "blocked")}.");
                }
            }

            #endregion Test Can Move To
            
            #region Pathfinder Tests
            
            if (testPathToLocation)
            {
                testPathToLocation = false;

                pathfindingData.FindPathTo(destination);
            }
            
            if (testPathToRandomLocation)
            {
                testPathToRandomLocation = false;

                var location = new VectorXZ(Random.Range(-49, 49), Random.Range(-36, 36));
                pathfindingData.FindPathTo(location);
            }

            if (testPathToEntityWithTypes)
            {
                testPathToEntityWithTypes = false;

                pathfindingData.FindPathTo(entityTypes);
            }
            
            #endregion Pathfinder Tests

            #region Thinker Tests

            if (testMoveToLocation)
            {
                testMoveToLocation = false;

                var subgoal = MoveToLocation.CreateInstance(thinkingAgent, moveToDestination);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testMoveToRandomLocation)
            {
                testMoveToRandomLocation = false;

                var location = new VectorXZ(Random.Range(-49, 49), Random.Range(-36, 36));
                var subgoal = MoveToLocation.CreateInstance(thinkingAgent, location);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testSeekToLocation)
            {
                testSeekToLocation = false;

                var subgoal = SeekToLocation.CreateInstance(thinkingAgent, seekToDestination);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testArriveAtLocation)
            {
                testArriveAtLocation = false;

                var subgoal = ArriveAtLocation.CreateInstance(thinkingAgent, arriveAtDestination);
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingData.Brain.PushSubgoal(subgoal);
            }

            if (testSplicePath)
            {
                testSplicePath = false;

                if (EventManager.Instance != null)
                {
                    if (VerbosityDebug)
                    {
                        Log.Debug($"{GetType().Name} location before seek:{thinkingData.Location}");
                    }
                    
                    RemoveAndDestroySeek();
                    seek = Seek.CreateInstance(thinkingData, splicePathDestination);
                    seek.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                    SetParameters(seek);
                    thinkingAgent.Data.AddSteeringBehaviour(seek);
                    
                    EventManager.Instance.Subscribe<PathToLocationReadyEventPayload>(
                        Events.PathToLocationReady,
                        OnPathToLocationReady);
                
                    EventManager.Instance.Enqueue(
                        Events.PathToLocationRequest,
                        new PathToLocationRequestEventPayload(thinkingAgent,
                            splicePathDestination));
                }
            }

            #endregion Thinker Tests
        }

        #region Set Up Can Move To Test

        void SetParameters(Seek sb)
        {
            sb.NoStop = false;
            sb.NoSlow = false;
            sb.NeverCompletes = false;
            sb.LinearStopAtSpeed = 0.1f;
            sb.SlowEnoughLinearSpeed = 0.5f;
            sb.LinearDrag = 1.015f;
            sb.CloseEnoughDistance = 1f;
        }

        void RemoveAndDestroySeek()
        {
            if (seek != null)
            {
                thinkingData.RemoveSteeringBehaviour(seek);
                Destroy(seek);
                seek = null;
            }
        }

        #endregion Set Up Can Move To Test

        #endregion Update

        #region Event Handlers
        
        bool OnPathToLocationReady(Event<PathToLocationReadyEventPayload> eventArg)
        {
            PathToLocationReadyEventPayload payload = eventArg.EventData;
        
            if (payload.pathfindingAgent != thinkingAgent) // event not for us
            {
                return false;
            }
        
            if (EventManager.Instance != null)
            {
                EventManager.Instance.Unsubscribe<PathToLocationReadyEventPayload>(
                    Events.PathToLocationReady,
                    OnPathToLocationReady);
            }

            path = payload.path;

            path.Show(thinkingData.ShowPath);
            
            // Simulate delay for a long pathfinding request.
            Invoke(nameof(Smooth), 5);

            return true;
        }

        void Smooth()
        {
            RemoveAndDestroySeek();

            thinkingData.Velocity = VectorXZ.zero; // hard stop
            
            if (VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} location after seek:{thinkingData.Location}");
            }

            path = Path.SmoothPath(thinkingAgent, path);
            
            if (VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} adding FollowPath\n{path}");
            }

            Invoke(nameof(CancelPath), 5);
        }

        void CancelPath()
        {
            path = Path.CancelPath(path);
        }

        #endregion Event Handlers
    }
}