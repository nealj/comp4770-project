using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.Armory;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Extensions.MonoBehaviours;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace Testing
{
    public sealed class W32TestWeaponsAndProjectiles : ExtendedMonoBehaviour
    {
        #region Members and Properties
        
        [Header("Respawn")]
        [SerializeField] bool respawnThinker;
        [SerializeField] VectorXYZ spawnPointThinker;
        
        [Header("Test Weapons and Projectiles")]
        [SerializeField] bool testAddBolt;
        [SerializeField] bool testAddPellet;
        [SerializeField] bool testAddRocket;
        [SerializeField] bool testAddSlug;

        [Header("References")]
        [SerializeField] ThinkingAgent thinkingAgent;
        ThinkingData thinkingData;
        WeaponBlaster blaster;
        WeaponRailgun railgun;
        WeaponRocketLauncher rocketLauncher;
        WeaponShotgun shotgun;

        #endregion Members and Properties

        #region Update

        public override void Update()
        {
            base.Update();

            #region Set Up

            if (thinkingAgent != null)
            {
                thinkingAgent.verbosity = verbosity;
                thinkingData = thinkingAgent.Data;
                thinkingData.verbosity = (ExtendedScriptableObject.VerbosityStates)verbosity;
                
                blaster = new WeaponBlaster(thinkingAgent);
                railgun = new WeaponRailgun(thinkingAgent);
                rocketLauncher = new WeaponRocketLauncher(thinkingAgent);
                shotgun = new WeaponShotgun(thinkingAgent);
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: ThinkingAgent not set.");
                return;
            }

            #endregion Set Up

            #region Respawn

            if (respawnThinker)
            {
                respawnThinker = false;
                thinkingAgent.Spawn(spawnPointThinker);
            }

            #endregion Respawn

            #region Weapon and Projectile Tests
            
            if (testAddBolt)
            {
                testAddBolt = false;
                blaster.AddBolt(thinkingAgent, thinkingAgent.Data.HeadingVectorXYZ * 50f);
            }
            
            if (testAddPellet)
            {
                testAddPellet = false;
                shotgun.AddPellet(thinkingAgent, thinkingAgent.Data.HeadingVectorXYZ * 50f);
            }
            
            if (testAddRocket)
            {
                testAddRocket = false;
                rocketLauncher.AddRocket(thinkingAgent, thinkingAgent.Data.HeadingVectorXYZ * 50f);
            }

            if (testAddSlug)
            {
                testAddSlug = false;
                railgun.AddSlug(thinkingAgent, thinkingAgent.Data.HeadingVectorXYZ * 50f);
            }

            #endregion Thinker Tests
        }

        #endregion Update
    }
}