using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Extensions.MonoBehaviours;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace Testing
{
    public sealed class W31TestArbitrationAndEvaluators : ExtendedMonoBehaviour
    {
        #region Members and Properties

        [Header("Options")]
        [SerializeField] bool showPath = true;
        [SerializeField] bool smoothPath;
        [SerializeField] bool showClosestNodeVisualizer;
        [SerializeField] bool showClosestToEntityWithTypeVisualizer;
        [SerializeField] bool showDirectPathVisualizer;

        [Header("Respawn")]
        [SerializeField] bool respawnThinker;
        [SerializeField] VectorXYZ spawnPointThinker;

        [Header("Test Damage Thinker")]
        [SerializeField] bool testDamageThinker;

        [Header("Test Terminate Brain")]
        [SerializeField] bool testTerminateBrain;

        [Header("References")]
        [SerializeField] ThinkingAgent thinkingAgent;
        ThinkingData thinkingData;
        [SerializeField] PathPlanner pathPlanner;

        #endregion Members and Properties

        #region Update

        public override void Update()
        {
            base.Update();

            #region Set Up

            if (thinkingAgent != null)
            {
                thinkingAgent.verbosity = verbosity;
                thinkingData = thinkingAgent.Data;
                thinkingData.verbosity = (ExtendedScriptableObject.VerbosityStates)verbosity;
                thinkingData.ShowPath = showPath;
                thinkingData.SmoothPath = smoothPath;
                thinkingData.ShowClosestNodeVisualizer = showClosestNodeVisualizer;
                thinkingData.ShowClosestToEntityWithTypeVisualizer = showClosestToEntityWithTypeVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: ThinkingAgent not set.");
                return;
            }
            
            if (pathPlanner != null)
            {
                pathPlanner.ShowDirectPathVisualizer = showDirectPathVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: PathPlanner not set.");
                return;
            }

            #endregion Set Up

            #region Respawn

            if (respawnThinker)
            {
                respawnThinker = false;
                thinkingAgent.Spawn(spawnPointThinker);
            }

            #endregion Respawn

            #region Thinker Tests

            if (testDamageThinker)
            {
                testDamageThinker = false;

                thinkingAgent.DecreaseHealth(5);
            }
            
            if (testTerminateBrain)
            {
                testTerminateBrain = false;

                thinkingData.Brain.Terminate();
            }

            #endregion Thinker Tests
        }

        #endregion Update
    }
}