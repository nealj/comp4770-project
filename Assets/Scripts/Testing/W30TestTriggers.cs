using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Entities.Types;
using GameBrains.Extensions.MonoBehaviours;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace Testing
{
    public sealed class W30TestTriggers : ExtendedMonoBehaviour
    {
        #region Members and Properties

        [Header("Options")]
        [SerializeField] bool showPath = true;
        [SerializeField] bool smoothPath;
        [SerializeField] bool showClosestNodeVisualizer;
        [SerializeField] bool showClosestToEntityWithTypeVisualizer;
        [SerializeField] bool showDirectPathVisualizer;

        [Header("Respawn")]
        [SerializeField] bool respawnThinker;
        [SerializeField] VectorXYZ spawnPointThinker;

        [Header("Test Get Entity with Types")]
        [SerializeField] bool testGetEntityWithTypes;
        // TODO: We need to handle a stuck agent
        [SerializeField] float closeEnoughDistanceForGetEntityWitType = 1.5f;

        [Header("Test Get Blue Flag")]
        [SerializeField] bool testGetBlueFlag;
        [SerializeField] float closeEnoughDistanceForGetBlueFlag = 0.5f;

        [Header("Test Get Health")]
        [SerializeField] bool testGetHealth;
        [SerializeField] float closeEnoughDistanceForGetHealth = 0.5f;

        [Header("Test Get Shotgun")]
        [SerializeField] bool testGetShotgun;
        [SerializeField] float closeEnoughDistanceForGetShotgun = 0.5f;

        [Header("Test Get Rocket Launcher")]
        [SerializeField] bool testGetRocketLauncher;
        [SerializeField] float closeEnoughDistanceForGetRocketLauncher = 0.5f;

        [Header("Test Get Railgun")]
        [SerializeField] bool testGetRailgun;
        [SerializeField] float closeEnoughDistanceForGetRailgun = 0.5f;

        [Header("References")]
        [SerializeField] ThinkingAgent thinkingAgent;
        ThinkingData thinkingData;
        [SerializeField] PathPlanner pathPlanner;
        [SerializeField] EntityTypes entityTypeToGet;
        [SerializeField] EntityTypes healthEntityType;
        [SerializeField] EntityTypes shotgunEntityType;
        [SerializeField] EntityTypes rocketLauncherEntityType;
        [SerializeField] EntityTypes railgunEntityType;
        [SerializeField] EntityTypes blueFlagEntityTypes;

        #endregion Members and Properties

        #region Update

        public override void Update()
        {
            base.Update();

            #region Set Up

            if (thinkingAgent != null)
            {
                thinkingAgent.verbosity = verbosity;
                thinkingData = thinkingAgent.Data;
                thinkingData.verbosity = (ExtendedScriptableObject.VerbosityStates)verbosity;
                thinkingData.ShowPath = showPath;
                thinkingData.SmoothPath = smoothPath;
                thinkingData.ShowClosestNodeVisualizer = showClosestNodeVisualizer;
                thinkingData.ShowClosestToEntityWithTypeVisualizer = showClosestToEntityWithTypeVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: ThinkingAgent not set.");
                return;
            }
            
            if (pathPlanner != null)
            {
                pathPlanner.ShowDirectPathVisualizer = showDirectPathVisualizer;
            }
            else
            {
                Debug.LogWarning($"{GetType().Name}: PathPlanner not set.");
                return;
            }

            #endregion Set Up

            #region Respawn

            if (respawnThinker)
            {
                respawnThinker = false;
                thinkingAgent.Spawn(spawnPointThinker);
            }

            #endregion Respawn

            #region Thinker Tests

            if (testGetEntityWithTypes)
            {
                testGetEntityWithTypes = false;

                thinkingData.Brain.Terminate();
                var subgoal = GetEntityWithTypes.CreateInstance(
                    thinkingAgent, 
                    entityTypeToGet,
                    closeEnoughDistanceForGetEntityWitType);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingAgent.Data.Brain.PushSubgoal(subgoal);
            }

            if (testGetHealth)
            {
                testGetHealth = false;

                thinkingData.Brain.Terminate();
                var subgoal = GetEntityWithTypes.CreateInstance(
                    thinkingAgent,
                    healthEntityType,
                    closeEnoughDistanceForGetHealth);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingAgent.Data.Brain.PushSubgoal(subgoal);
            }

            if (testGetBlueFlag)
            {
                testGetBlueFlag = false;

                thinkingData.Brain.Terminate();
                var subgoal = GetEntityWithTypes.CreateInstance(
                    thinkingAgent,
                    blueFlagEntityTypes,
                    closeEnoughDistanceForGetBlueFlag);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingAgent.Data.Brain.PushSubgoal(subgoal);
            }

            if (testGetShotgun)
            {
                testGetShotgun = false;

                thinkingData.Brain.Terminate();
                var subgoal = GetEntityWithTypes.CreateInstance(
                    thinkingAgent,
                    shotgunEntityType,
                    closeEnoughDistanceForGetShotgun);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingAgent.Data.Brain.PushSubgoal(subgoal);
            }

            if (testGetRocketLauncher)
            {
                testGetRocketLauncher = false;

                thinkingData.Brain.Terminate();
                var subgoal = GetEntityWithTypes.CreateInstance(
                    thinkingAgent,
                    rocketLauncherEntityType,
                    closeEnoughDistanceForGetRocketLauncher);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingAgent.Data.Brain.PushSubgoal(subgoal);
            }

            if (testGetRailgun)
            {
                testGetRailgun = false;

                thinkingData.Brain.Terminate();
                var subgoal = GetEntityWithTypes.CreateInstance(
                    thinkingAgent,
                    railgunEntityType,
                    closeEnoughDistanceForGetRailgun);
                // TODO: Create common verbosity states enum.
                subgoal.verbosity = (ExtendedScriptableObject.VerbosityStates)(int)verbosity;
                thinkingAgent.Data.Brain.PushSubgoal(subgoal);
            }

            #endregion Thinker Tests
        }

        #endregion Update
    }
}