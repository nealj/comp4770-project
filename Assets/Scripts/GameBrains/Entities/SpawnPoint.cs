using UnityEngine;

namespace GameBrains.Entities
{
    public class SpawnPoint : Entity
    {
        public bool IsAvailable
        {
            get
            {
                int maxColliders = 10;
                Collider[] hitColliders = new Collider[maxColliders];
                var size = Physics.OverlapSphereNonAlloc(transform.position, Data.Radius, hitColliders);
			
                foreach (Collider hitCollider in hitColliders)
                {
                    if (hitCollider != null && hitCollider.GetComponent<ThinkingAgent>() != null)
                    {
                        return false;
                    }
                }
			
                return true;
            }
        }
    }
}