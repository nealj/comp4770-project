using GameBrains.Armory;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Entities.Triggers
{
	public enum WeaponGiverTypes { Railgun, RocketLauncher, Shotgun, BombLauncher }

	public sealed class TriggerWeaponGiver : TriggerRespawning
	{
		public WeaponGiverTypes weaponGiverType;
		public WeaponType givenWeaponType;
		
		public override void Awake()
		{
			base.Awake();

			switch (weaponGiverType)
			{
				case WeaponGiverTypes.Railgun:
					EntityTypes.Add(Parameters.Instance.RailgunEntityType);
					givenWeaponType = Parameters.Instance.RailgunWeaponType;
					break;
				case WeaponGiverTypes.Shotgun:
					EntityTypes.Add(Parameters.Instance.ShotgunEntityType);
					givenWeaponType = Parameters.Instance.ShotgunWeaponType;
					break;
				case WeaponGiverTypes.RocketLauncher:
					EntityTypes.Add(Parameters.Instance.RocketLauncherEntityType);
					givenWeaponType = Parameters.Instance.RocketLauncherWeaponType;
					break;
				case WeaponGiverTypes.BombLauncher:
					EntityTypes.Add(Parameters.Instance.BombLauncherEntityType);
					givenWeaponType = Parameters.Instance.BombLauncherWeaponType;
					break;
			}

			TimeBetweenRespawns = Parameters.Instance.WeaponRespawnDelay;
		}
	
		public void OnTriggerEnter(Collider triggeringCollider)
		{
			if (!CanTrigger(triggeringCollider.gameObject)) { return; }
			
			if (IsActive)
			{
				TriggeringAgent = triggeringCollider.GetComponent<ThinkingAgent>();
			
				if (TriggeringAgent != null)
				{
					TriggeringAgent.Data.WeaponSystem.AddWeapon(givenWeaponType);
					
					if (VerbosityDebug)
					{
						Log.Debug($"Adding weapon {weaponGiverType} to {TriggeringAgent.ShortName}.");
					}

					Deactivate();
				}
			}
		}
	}
}