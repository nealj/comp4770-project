using UnityEngine;

namespace GameBrains.Entities.Triggers
{
    public abstract class Trigger : Entity
    {
        Renderer triggerRenderer;
        int triggerLayer;

        public override bool IsActive
        {
            get => base.IsActive;
            set
            {
                base.IsActive = value;
                triggerRenderer.enabled = value;
                // Ignore when inactive
                gameObject.layer = value ? triggerLayer : LayerMask.NameToLayer("Ignore Raycast");
            }
        }

        public ThinkingAgent TriggeringAgent { get; protected set; }

        public LayerMask CanTriggerMask
        {
            get => canTriggerMask;
            set => canTriggerMask = value;
        }

        [SerializeField] LayerMask canTriggerMask;

        public override void Awake()
        {
            base.Awake();
            triggerLayer = gameObject.layer;
            triggerRenderer = GetComponentInChildren<Renderer>();
            IsActive = true;
        }

        protected bool CanTrigger(GameObject candidateObject)
        {
            var candidateLayerMask = 1 << candidateObject.layer;
            return (CanTriggerMask & candidateLayerMask) == candidateLayerMask;
        }
    }
}