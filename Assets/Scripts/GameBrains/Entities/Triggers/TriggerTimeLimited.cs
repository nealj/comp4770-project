using UnityEngine;

namespace GameBrains.Entities.Triggers
{
    public abstract class TriggerTimeLimited : Trigger
    {
        public float Lifetime { get; protected set; }

        public override void Update()
        {
            base.Update();

            if (Application.IsPlaying(this))
            {
                Lifetime -= Time.deltaTime;

                if (Lifetime <= 0)
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}