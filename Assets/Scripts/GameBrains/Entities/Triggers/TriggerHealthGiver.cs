using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Entities.Triggers
{
	public sealed class TriggerHealthGiver : TriggerRespawning
	{
		public int HealthGiven { get; private set; }
	
		public override void Awake()
		{
			base.Awake();
		
			EntityTypes.Add(Parameters.Instance.HealthEntityType);
			HealthGiven = Parameters.Instance.DefaultHealthGiven;
			TimeBetweenRespawns = Parameters.Instance.HealthRespawnDelay;
		}
	
		public void OnTriggerEnter(Collider triggeringCollider)
		{
			if (!CanTrigger(triggeringCollider.gameObject)) { return; }

			if (IsActive)
			{
				TriggeringAgent = triggeringCollider.GetComponent<ThinkingAgent>();
			
				if (TriggeringAgent != null)
				{
					if (VerbosityDebug)
					{
						Log.Debug($"Adding health {HealthGiven} to {TriggeringAgent}.");
					}

					TriggeringAgent.IncreaseHealth(HealthGiven);

					Deactivate();
				}
			}
		}
	}
}