//ADDED Class
using GameBrains.Armory;
using GameBrains.GameManagement;
using UnityEngine;


namespace GameBrains.Entities.Triggers
{
	public sealed class TriggerFlag : TriggerRespawning
	{


		public override void Awake()
		{
			base.Awake();
			
			
			//EntityTypes.Add(Parameters.Instance.BlueFlagEntityType);//.FlagEntityType);
			//FlagGiven = Parameters.Instance.DefaultFlagGiven;		//HealthGiven
			TimeBetweenRespawns = Parameters.Instance.FlagRespawnDelay;
		}

		public void OnTriggerEnter(Collider triggeringCollider)
		{
			//if (!CanTrigger(triggeringCollider.gameObject)) { return; }

			if (IsActive)
			{
				TriggeringAgent = triggeringCollider.GetComponent<ThinkingAgent>();

				//Agent and flag same colour, so do nothing 
				if (TriggeringAgent.Color == this.Color)
				{
					return;
				}

				if (TriggeringAgent != null)
				{
					if (VerbosityDebug)
					{
						Log.Debug($"Adding Flag {TriggeringAgent}.");
					}

					TriggeringAgent.IncrementTeamScore();   //increment team score
					Deactivate();
				}
			}
		}


	}
}