using UnityEngine;

namespace GameBrains.Entities.Triggers
{
    public abstract class TriggerRespawning : Trigger
    {
        public float TimeBetweenRespawns { get; protected set; }
        public float TimeUntilRespawn { get; protected set; }

        public override void Awake()
        {
            base.Awake();

            TimeBetweenRespawns = 0;
            TimeUntilRespawn = 0;
        }

        public override void Update()
        {
            base.Update();

            if (Application.IsPlaying(this))
            {
                TimeUntilRespawn -= Time.deltaTime;

                if (TimeUntilRespawn <= 0 && !IsActive)
                {
                    IsActive = true;
                }
            }
        }

        protected void Deactivate()
        {
            IsActive = false;
            TimeUntilRespawn = TimeBetweenRespawns;
        }
    }
}