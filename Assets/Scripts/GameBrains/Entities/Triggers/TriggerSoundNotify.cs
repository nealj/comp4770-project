using System.ComponentModel;
using GameBrains.Entities;
using GameBrains.EventSystem;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Entities.Triggers
{
	public sealed class TriggerSoundNotify : TriggerTimeLimited
	{
		public ThinkingAgent NoiseMakingAgent { get; set; }
	
		public override void Awake()
		{
			base.Awake();
		
			EntityTypes.Add(Parameters.Instance.SoundNotifierEntityType);
			Lifetime = Parameters.Instance.SoundTriggerLifetime;
		}
	
		public void OnTriggerEnter(Collider triggeringCollider)
		{
			if (!CanTrigger(triggeringCollider.gameObject)) { return; }
			
			TriggeringAgent = triggeringCollider.GetComponent<ThinkingAgent>();
			
			if (TriggeringAgent != null && TriggeringAgent != NoiseMakingAgent)
			{
				if (VerbosityDebug)
				{
					Log.Debug($"{TriggeringAgent} triggered {NoiseMakingAgent}'s sound trigger.");
				}

				EventManager.Instance.Enqueue(
					Events.NoiseMade,
					new NoiseMadeEventPayload(
						TriggeringAgent,
						NoiseMakingAgent));
			}
		}
	}
}

#region Events

// ReSharper disable once CheckNamespace
namespace GameBrains.EventSystem // NOTE: Don't change this namespace
{
	public static partial class Events
	{
		[Description("Noise Made Event.")]
		public static readonly EventType NoiseMade = (EventType) Count++;
	}

	public struct NoiseMadeEventPayload
	{
		public ThinkingAgent noiseHearer;
		public ThinkingAgent noiseMaker;

		public NoiseMadeEventPayload(ThinkingAgent noiseHearer, ThinkingAgent noiseMaker)
		{
			this.noiseHearer = noiseHearer;
			this.noiseMaker = noiseMaker;
		}
	}
}

#endregion Events