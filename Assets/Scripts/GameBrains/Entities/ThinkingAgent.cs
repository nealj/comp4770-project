using GameBrains.Entities.EntityData;
using GameBrains.EventSystem;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace GameBrains.Entities
{
    public class ThinkingAgent : PathfindingAgent
    {
        #region Motion

        public new ThinkingData Data => data as ThinkingData;

        #endregion Motion

        #region Awake

        public override void Awake()
        {
            base.Awake();
            data = (ThinkingData) transform;
        }

        #endregion Awake
        
        #region Enable/Disable

        public override void OnEnable()
        {
            base.OnEnable();

            SubscribeToThinkingEvents();
        }

        public override void OnDisable()
        {
            base.OnDisable();

            UnsubscribeFromThinkingEvents();
        }

        #endregion Enable/Disable
        
        #region Subscribe and Unsubscribe

        void SubscribeToThinkingEvents()
        {
            if (EventManager.Instance != null)
            {
                EventManager.Instance.Subscribe<DamageInflicted>( Events.DamageInflicted, OnDamageInflicted);
                EventManager.Instance.Subscribe<EntityDestroyed>( Events.EntityDestroyed, OnEntityDestroyed);
                EventManager.Instance.Subscribe<WeaponSound>( Events.WeaponSound, OnWeaponSound);
            }
            else if (Application.isPlaying)
            {
                Debug.LogWarning("Event manager missing. Unable to subscribe to thinking events.");
            }
        }

        void UnsubscribeFromThinkingEvents()
        {
            // If the EventManger got destroyed first, no need to unsubscribe
            if (EventManager.Instance != null)
            {
                EventManager.Instance.Unsubscribe<DamageInflicted>(Events.DamageInflicted, OnDamageInflicted);
                EventManager.Instance.Unsubscribe<EntityDestroyed>(Events.EntityDestroyed, OnEntityDestroyed);
                EventManager.Instance.Unsubscribe<WeaponSound>(Events.WeaponSound, OnWeaponSound);
            }
        }

        #endregion Subscribe and Unsubscribe

        #region Sense, think, act

        protected override void Think(float deltaTime)
        {
            base.Think(deltaTime);

            Data.Brain.Process();
            
            Data.SensoryMemory.UpdateVision();

            Data.TargetingSystem.UpdateTarget();
            
            Data.WeaponSystem.SelectWeapon();
        }
        
        protected override void Act(float deltaTime)
        {
            base.Act(deltaTime);

            if (Data.Position.y < -1f) // out of bounds
            {
                EventManager.Instance.Fire(Events.Message, EventManager.ReceiverIDIrrelevant, $"{ShortName} went out of bounds and died.");
                IsActive = false;
                return;
            }

            Data.WeaponSystem.TakeAimAndShoot(deltaTime);
        }

        #endregion Sense, think, act

        #region Spawn

        public override void Spawn(VectorXYZ spawnPoint)
        {
            base.Spawn(spawnPoint);

            Data.Brain.RemoveAllSubgoals();
            Data.TargetingSystem.ClearTarget();
            Data.WeaponSystem.Initialize();
            RestoreHealthToMaximum();
            
            EventManager.Instance.Fire( Events.Message, EventManager.ReceiverIDIrrelevant, $"{ShortName} respawned at {spawnPoint}.");
        }

        #endregion Spawn
        
        #region Thinking Events
        
        bool OnDamageInflicted(Event<DamageInflicted> eventArg)
        {
            DamageInflicted eventData = eventArg.EventData;

            // Check if event for us.
            if (eventData.victim != this) { return false; }
            if (IsInactive || IsSpawning) { return false; }

            DecreaseHealth((int)eventData.damageInflicted);

            if (IsActive) { return true; }
            
            EventManager.Instance.Fire( Events.Message, EventManager.ReceiverIDIrrelevant, $"{ShortName} was killed by {eventData.shooter.ShortName}.");

            var entityDestroyedData = new EntityDestroyed(eventData.shooter, eventData.victim);
            EventManager.Instance.Enqueue(Events.EntityDestroyed, entityDestroyedData);

            return true;
        }

        bool OnEntityDestroyed(Event<EntityDestroyed> eventArg)
        {
            EntityDestroyed eventData = eventArg.EventData;

            // Check if event is for us.
            if (eventData.shooter != this) { return false; }

            IncrementScore();
            
            EventManager.Instance.Fire( Events.Message, EventManager.ReceiverIDIrrelevant, $"{ShortName} scored for the {TeamName} Team ({Score}-{eventData.victim.Score}) by killing {eventData.victim.ShortName}.");

            if (Data.TargetingSystem.Target == eventData.victim) { Data.TargetingSystem.ClearTarget(); }

            return true;
        }

        bool OnWeaponSound(Event<WeaponSound> eventArg)
        {
            WeaponSound eventData = eventArg.EventData;

            // Check if event is for us.
            if (eventData.noiseHearer != this) { return false; }

            Data.SensoryMemory.UpdateWithSoundSource(eventData.noiseMaker);

            return true;
        }

        #endregion Thinking Methods
    }
}