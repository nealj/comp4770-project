using GameBrains.Armory;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using GameBrains.Memory;
using UnityEngine;

namespace GameBrains.Entities.EntityData
{
    [System.Serializable]
    public class ThinkingData : PathfindingData
    {
        #region Creators

        public new static ThinkingData CreateInstance(Transform t)
        {
            ThinkingData thinkingData = ScriptableObject.CreateInstance<ThinkingData>();
            Initialize(t, thinkingData);
            return thinkingData;
        }

        protected static void Initialize(
            Transform t,
            ThinkingData thinkingData)
        {
            PathfindingData.Initialize(t, thinkingData);
            thinkingData.Brain = Brain.CreateInstance(thinkingData.Owner);
            thinkingData.Brain.verbosity = (VerbosityStates)thinkingData.Owner.verbosity;
            thinkingData.TargetingSystem
                = TargetingSystem.CreateInstance(thinkingData.Owner);
            // TODO: Make memory span a parameter
            thinkingData.SensoryMemory
                = SensoryMemory.CreateInstance(thinkingData.Owner, 5f);
            thinkingData.WeaponSystem
                = new WeaponSystem(
                    thinkingData.Owner,
                    Parameters.Instance.AgentReactionTime,
                    Parameters.Instance.AgentAimAccuracy,
                    Parameters.Instance.AgentAimPersistenceTime);
        }

        #endregion Creators

        #region Casting

        public new ThinkingAgent Owner => owner as ThinkingAgent;

        public static implicit operator ThinkingData(Transform t)
        {
            return CreateInstance(t);
        }

        #endregion Casting

        public Brain Brain { get; set; }
        
        public SensoryMemory SensoryMemory { get; set; }

        public TargetingSystem TargetingSystem { get; set; }
        
        public WeaponSystem WeaponSystem  { get; set; }

        public float FieldOfView { get; protected set; } = 180f;
        
        public int Score { get; set; }

        public float HitIndicatorTimer { get; set; }

        public bool Hit { get; set; }

        #region Methods

        public bool IsTargetInFieldOfView(VectorXYZ targetPosition)
        {
            float angle
                = VectorXYZ.Angle(
                    targetPosition - Owner.Data.Position,
                    Owner.transform.forward);
            return angle < FieldOfView / 2;
        }

        #endregion Methods
    }
}