using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.GameManagement;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators
{
	public abstract class Evaluator
    {
	    #region Constructors

	    protected Evaluator(ThinkingAgent thinkingAgent, float characterBias)
        {
	        ThinkingAgent = thinkingAgent;
            CharacterBias = characterBias;
        }

	    #endregion Constructors

		#region Members and Properties

		protected static Parameters Parameters => Parameters.Instance;

		protected ThinkingAgent ThinkingAgent { get; set; }
		protected ThinkingData ThinkingData => ThinkingAgent.Data;
		protected Brain Brain => ThinkingData.Brain;

		protected float CharacterBias { get; }

		#endregion Members and Properties

		#region Methods

		public abstract float CalculateDesirability();
		
		public abstract void SetGoal();

		#endregion Methods
	}
}