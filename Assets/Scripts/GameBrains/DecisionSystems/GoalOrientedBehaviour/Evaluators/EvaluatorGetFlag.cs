//ADDED EvaluatorGetFlag Class
using GameBrains.Entities;
using GameBrains.Entities.Types;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators
{
	public class EvaluatorGetFlag : Evaluator
	{
		#region Constructors

		public EvaluatorGetFlag(ThinkingAgent thinkingAgent, float characterBias)
			: base(thinkingAgent, characterBias)
		{
		}

		#endregion Constructors

		#region Members and Properties

		float desirability;

		#endregion Members and Properties

		#region Methods

		public override float CalculateDesirability()
		{
			float distanceFactor = 0f;
			//Blue agent
			if (ThinkingAgent.Color == Color.blue)
			{
				// first grab the distance to the closest instance of a flag item
				distanceFactor = Feature.DistanceToItem(ThinkingAgent, Parameters.BlueFlagEntityType);
			}

			//Red agent
			else if (ThinkingAgent.Color == Color.red)
			{
				// first grab the distance to the closest instance of a flag item
				distanceFactor = Feature.DistanceToItem(ThinkingAgent, Parameters.RedFlagEntityType);
			}

			// if the distance feature is rated with a value of 1 it means that the
			// item is either not present on the map or too far away to be worth
			// considering, therefore the desirability is zero
			if (Mathf.Approximately(distanceFactor, 1f)) { desirability = 0f; return desirability; }

			//Team flag score
			int flagFactor = Feature.GetFlagScore(ThinkingAgent);

			//here I set desirability to increase as the score they capture more flags the 1 is for when they start, prety much they become hooked on capturing flags
			desirability =
				Parameters.AgentFlagGoalTweaker * (flagFactor + 1f);

			// bias the value according to the personality of the agent
			desirability *= CharacterBias;

			// ensure the value is in the range 0 to 1
			desirability = Mathf.Clamp(desirability, 0f, 1f);
			return desirability;
		}

		public override void SetGoal()
		{
			if (ThinkingAgent.Color == Color.blue) { Brain.AddGoalGetItemWithTypes(new EntityTypes(Parameters.BlueFlagEntityType)); }
			if (ThinkingAgent.Color == Color.red) { Brain.AddGoalGetItemWithTypes(new EntityTypes(Parameters.RedFlagEntityType)); }
		}

		public override string ToString()
		{
			return $"H: {desirability:F2}";
		}

		#endregion Methods
	}
}