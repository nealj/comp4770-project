using GameBrains.Entities;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators
{
	public class EvaluatorExplore : Evaluator
	{
		#region Constructors

		public EvaluatorExplore(ThinkingAgent thinkingAgent, float characterBias)
			: base(thinkingAgent, characterBias)
		{
		}

		#endregion Constructors

		#region Members and Properties

		float desirability;

		#endregion Members and Properties

		#region Methods

		public override float CalculateDesirability()
		{
			// small fixed value so explore is desired if nothing else is
			desirability = Parameters.AgentExploreDesirability;
			desirability *= CharacterBias;

			// ensure the value is in the range 0 to 1
			desirability = Mathf.Clamp(desirability, 0f, 1f);

			return desirability;
		}
		
		public override void SetGoal() { Brain.AddGoalExplore(); }
		
		public override string ToString() { return $"E: {desirability:F2}"; }

		#endregion Methods
	}
}