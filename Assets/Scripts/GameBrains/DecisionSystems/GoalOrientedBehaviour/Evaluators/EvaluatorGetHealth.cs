using GameBrains.Entities;
using GameBrains.Entities.Types;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators
{
	public class EvaluatorGetHealth : Evaluator
	{
		#region Constructors

		public EvaluatorGetHealth(ThinkingAgent thinkingAgent, float characterBias)
			: base(thinkingAgent, characterBias)
		{
		}

		#endregion Constructors

		#region Members and Properties

		float desirability;

		#endregion Members and Properties

		#region Methods

		public override float CalculateDesirability()
		{
			// first grab the distance to the closest instance of a health item
			float distanceFactor = Feature.DistanceToItem(ThinkingAgent, Parameters.HealthEntityType);

			// if the distance feature is rated with a value of 1 it means that the
			// item is either not present on the map or too far away to be worth
			// considering, therefore the desirability is zero
			if (Mathf.Approximately(distanceFactor, 1f)) { desirability = 0f; return desirability; }

			float healthFactor = Feature.Health(ThinkingAgent);

			// the desirability of finding a health item is proportional to the amount
			// of health remaining and inversely proportional to the distance from the
			// nearest instance of a health item.
			desirability =
				Parameters.AgentHealthGoalTweaker *
				(1f - healthFactor) / distanceFactor;

			// bias the value according to the personality of the agent
			desirability *= CharacterBias;

			// ensure the value is in the range 0 to 1
			desirability = Mathf.Clamp(desirability, 0f, 1f);

			return desirability;
		}

		public override void SetGoal()
		{
			Brain.AddGoalGetItemWithTypes(new EntityTypes(Parameters.HealthEntityType));
		}

		public override string ToString()
		{
			return $"H: {desirability:F2}";
		}

		#endregion Methods
    }
}