using System;
using GameBrains.Armory;
using GameBrains.Entities;
using GameBrains.Entities.Types;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators
{
	public static class Feature
    {
        #region Members and Properties

        static Parameters Parameters => Parameters.Instance;

        #endregion Members and Properties

        #region Methods

        public static float Health(ThinkingAgent thinkingAgent)
        {
            return thinkingAgent.Health / (float)thinkingAgent.MaximumHealth;
        }

        //ADDED to return the flag score once set up 
        public static int GetFlagScore(ThinkingAgent thinkingAgent)
        {
            int score = 0;
			foreach (ThinkingAgent eachThinkingAgent in EntityManager.FindAll<ThinkingAgent>())
			{
				if (eachThinkingAgent.Color == thinkingAgent.Color)
				{
					//Debug.Log($"{thinkingAgent.name} : {thinkingAgent.TeamScore} ");
					score += eachThinkingAgent.TeamFlagScore;
				}
				//Debug.Log(thinkingAgent.Color + " " + score);
			}
            return score;
        }


        public static float DistanceToItem(ThinkingAgent thinkingAgent, EntityType entityType)
        {
            return DistanceToItem(thinkingAgent, new EntityTypes(entityType));
        }

        public static float DistanceToItem(ThinkingAgent thinkingAgent, EntityTypes entityTypes)
        {
            // determine the distance to the closest instance with the given entity types
            float distanceToItem = thinkingAgent.Data.CostToClosestEntityWithTypes(entityTypes);

            // if the previous method returns a negative value then there is no item of
            // the specified type present in the game world at this time.
            if (distanceToItem < 0f)
            {
                return 1f;
            }

            // these values represent cutoffs. Any distance over maxDistance results in
            // a value of 0, and value below minDistance results in a value of 1
            // TODO: These should be map parameters
            const float maxDistance = 75.0f;
            const float minDistance = 10.0f;

            distanceToItem = Mathf.Clamp(distanceToItem, minDistance, maxDistance);

            return distanceToItem / maxDistance;
        }

        public static float IndividualWeaponStrength(ThinkingAgent thinkingAgent, WeaponType weaponType)
        {
            // grab a pointer to the gun (if the agent owns an instance)
            Weapon weapon = thinkingAgent.Data.WeaponSystem.GetWeaponFromInventory(weaponType);

            if (weapon != null)
            {
                return weapon.RoundsRemaining / GetMaxRoundsBotCanCarryForWeapon(weaponType);
            }

            return 0.0f;
        }

        public static float TotalWeaponStrength(ThinkingAgent thinkingAgent)
        {
            float maxRoundsForShotgun           = GetMaxRoundsBotCanCarryForWeapon(Parameters.ShotgunWeaponType);
            float maxRoundsForRailgun           = GetMaxRoundsBotCanCarryForWeapon(Parameters.RailgunWeaponType);
            float maxRoundsForRocketLauncher    = GetMaxRoundsBotCanCarryForWeapon(Parameters.RocketLauncherWeaponType);
            //ADDED bomb launcher 
            float maxRoundsForBombLauncher      = GetMaxRoundsBotCanCarryForWeapon(Parameters.BombLauncherWeaponType);

            float totalRoundsCarriable = maxRoundsForShotgun + maxRoundsForRailgun + maxRoundsForRocketLauncher + maxRoundsForBombLauncher;

            float numSlugs      = thinkingAgent.Data.WeaponSystem.GetRoundsRemaining(Parameters.RailgunWeaponType);
            float numCartridges = thinkingAgent.Data.WeaponSystem.GetRoundsRemaining(Parameters.ShotgunWeaponType);
            float numRockets    = thinkingAgent.Data.WeaponSystem.GetRoundsRemaining(Parameters.RocketLauncherWeaponType);
            float numBombs      = thinkingAgent.Data.WeaponSystem.GetRoundsRemaining(Parameters.BombLauncherWeaponType);

            // the value of the tweaker (must be in the range 0-1) indicates how much
            // desirability value is returned even if an agent has not picked up any weapons.
            // (it basically adds in an amount for an agent's persistent weapon -- the blaster)
            const float tweaker = 0.1f;

            return tweaker + (1 - tweaker) * (numSlugs + numCartridges + numRockets + numBombs) / totalRoundsCarriable;
        }

        #endregion Methods

        #region Private Methods

        static float GetMaxRoundsBotCanCarryForWeapon(WeaponType weaponType)
        {
            if (weaponType == Parameters.RailgunWeaponType)
            {
                return Parameters.RailgunMaximumRoundsCarried;
            }

            if (weaponType == Parameters.RocketLauncherWeaponType)
            {
                return Parameters.RocketLauncherMaximumRoundsCarried;
            }

            if (weaponType == Parameters.BombLauncherWeaponType)
            {
                return Parameters.BombLauncherMaximumRoundsCarried;
            }

            if (weaponType == Parameters.ShotgunWeaponType)
            {
                return Parameters.ShotgunMaximumRoundsCarried;
            }

            throw new Exception("Trying to calculate ammo of unknown weapon.");
        }

        #endregion Private Methods
	}
}