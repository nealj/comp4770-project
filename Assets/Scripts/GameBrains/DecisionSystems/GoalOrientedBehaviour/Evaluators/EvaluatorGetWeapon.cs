using GameBrains.Armory;
using GameBrains.Entities;
using GameBrains.Entities.Types;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators
{
    public class EvaluatorGetWeapon : Evaluator
    {
        #region Constructors

        public EvaluatorGetWeapon(ThinkingAgent thinkingAgent, float characterBias, WeaponType weaponType)
            : base(thinkingAgent, characterBias)
        {
            this.weaponType = weaponType;
        }

        #endregion Constructors

        #region Members and Properties

        readonly WeaponType weaponType;
        float desirability;

        #endregion Members and Properties

        #region Methods

        public override float CalculateDesirability()
        {
            // Grab the distance to the closest instance of the weapon type.
            float distance = Feature.DistanceToItem(ThinkingAgent, WeaponGiverEntityTypes);

            // If the distance feature is rated with a value of 1, it means that the
            // item is either not present on the map or too far away to be worth
            // considering, therefore the desirability is zero.
            if (Mathf.Approximately(distance, 1f)) { desirability = 0f; return desirability; }

            // Value used to tweak the desirability.
            float tweaker = 1.0f;
            if (weaponType == Parameters.RailgunWeaponType)
            {
                tweaker = Parameters.AgentRailgunGoalTweaker;
            }
            else if (weaponType == Parameters.RocketLauncherWeaponType)
            {
                tweaker = Parameters.AgentRocketLauncherGoalTweaker;
            }
            else if (weaponType == Parameters.BombLauncherWeaponType)
            {
                tweaker = Parameters.AgentBombLauncherGoalTweaker;
            }
            else if (weaponType == Parameters.ShotgunWeaponType)
            {
                tweaker = Parameters.AgentShotgunGoalTweaker;
            }

            float health = Feature.Health(ThinkingAgent);

            float weaponStrength = Feature.IndividualWeaponStrength(ThinkingAgent, weaponType);

            desirability = (tweaker * health * (1f - weaponStrength)) / distance;

            desirability *= CharacterBias;

            // ensure the value is in the range 0 to 1
            desirability = Mathf.Clamp(desirability, 0f, 1f);

            return desirability;
        }

        public override void SetGoal()
        {
            Brain.AddGoalGetItemWithTypes(WeaponGiverEntityTypes);
        }

        EntityTypes WeaponGiverEntityTypes
        {
            get
            {
                var entityTypes = new EntityTypes();
                if (weaponType == Parameters.RailgunWeaponType)
                {
                    entityTypes.Add(Parameters.RailgunEntityType);
                }
                else if (weaponType == Parameters.ShotgunWeaponType)
                {
                    entityTypes.Add(Parameters.ShotgunEntityType);
                }
                else if (weaponType == Parameters.RocketLauncherWeaponType)
                {
                    entityTypes.Add(Parameters.RocketLauncherEntityType);
                }
                else if (weaponType == Parameters.BombLauncherWeaponType)
                {
                    entityTypes.Add(Parameters.BombLauncherEntityType);
                }
                return entityTypes;
            }
        }

        public override string ToString()
        {
            string weaponShortName = "";
            if (weaponType == Parameters.BlasterWeaponType) { weaponShortName = "BL"; }
            else if (weaponType == Parameters.RailgunWeaponType) { weaponShortName = "RG"; }
            else if (weaponType == Parameters.ShotgunWeaponType) { weaponShortName = "SG";}
            else if (weaponType == Parameters.RocketLauncherWeaponType) { weaponShortName = "RL"; }
            else if (weaponType == Parameters.BombLauncherWeaponType) { weaponShortName = "BO"; }
            return $"{weaponShortName}: {desirability:F2}";
        }

        #endregion Methods
	}
}