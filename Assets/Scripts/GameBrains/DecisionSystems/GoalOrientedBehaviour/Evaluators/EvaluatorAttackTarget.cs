using GameBrains.Entities;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators
{
	public class EvaluatorAttackTarget : Evaluator
	{
		#region Constructors

		public EvaluatorAttackTarget(ThinkingAgent thinkingAgent, float characterBias)
			: base(thinkingAgent, characterBias) { }

		#endregion Constructors

		#region Members and Properties

		float desirability;

		#endregion Members and Properties

		#region Methods

		public override float CalculateDesirability()
		{
			desirability = 0.0f;

			// only do the calculation if there is a target present
			if (ThinkingData.TargetingSystem.IsTargetPresent)
			{
				float tweaker = Parameters.AgentAggroGoalTweaker;
				
				float healthFactor = Feature.Health(ThinkingAgent);
				
				float totalWeaponStrength = Feature.TotalWeaponStrength(ThinkingAgent);

				desirability = tweaker * healthFactor * totalWeaponStrength;

				// bias the value according to the personality of the agent
				desirability *= CharacterBias;

				// ensure the value is in the range 0 to 1
				desirability = Mathf.Clamp(desirability, 0f, 1f);
			}

			return desirability;
		}
		
		public override void SetGoal() { Brain.AddGoalAttackTarget(); }
		
		public override string ToString() { return $"A: {desirability:F2}"; }

		#endregion Methods
	}
}