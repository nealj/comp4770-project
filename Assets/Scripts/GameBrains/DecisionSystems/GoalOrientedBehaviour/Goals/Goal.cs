using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.EventSystem;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals
{
    public abstract class Goal : ExtendedScriptableObject
    {
        #region Members and Properties
        
        public ThinkingAgent ThinkingAgent { get; set; }
        public ThinkingData ThinkingData => ThinkingAgent.Data;
        
        public GoalType GoalType { get; protected set; }
        
        public static Parameters Parameters => Parameters.Instance;
        public static EventManager EventManager => EventManager.Instance;

        static int nextId;
        static int NextID => nextId++;

        public int ID => id;
        //[ReadOnly]
        [SerializeField] int id = NextID;
        
        #endregion Members and Properties

        #region Status

        public enum StatusTypes
        {
            Inactive,
            Active,
            Completed,
            Failed
        }

        public StatusTypes Status { get; protected set; }

        public bool IsComplete => Status == StatusTypes.Completed;

        public bool IsActive => Status == StatusTypes.Active;

        public bool IsInactive => Status == StatusTypes.Inactive;

        public bool HasFailed => Status == StatusTypes.Failed;

        #endregion Status

        #region Subgoals

        public virtual void PushSubgoal(Goal goal)
        {
            Debug.LogWarning("Cannot push goals to atomic goals.");
        }

        public virtual void InsertUnderTopSubgoal(Goal subgoal)
        {
            Debug.LogWarning("Cannot insert goals to atomic goals.");
        }

        public virtual void RemoveAllSubgoals() { }

        #endregion Subgoals

        #region Activate/Process/Terminate

        public virtual void Activate()
        {
            if (VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} activating with status {Status}.");
            }
            
            Status = StatusTypes.Active;
        }

        public virtual StatusTypes Process()
        {
            if (VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} processing with status {Status}.");
            }
            
            ActivateIfInactive();
            
            return StatusTypes.Completed;
        }

        public virtual void Terminate()
        {
            if (VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} terminating with status {Status}.");
            }
        }
        
        #endregion Activate/Process/Terminate
        
        #region Methods

        protected void ReactivateIfFailed()
        {
            ReactivateIf(HasFailed);
        }
        
        protected void ReactivateIf(bool condition)
        {
            if (HasFailed && VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} has failed and is being reactivated with status {Status}.");
            }
            
            if (condition)
            {
                Status = StatusTypes.Inactive;
            }
        }

        protected void ActivateIfInactive()
        {
            if (IsInactive)
            {
                if (VerbosityDebug)
                {
                    Log.Debug($"{GetType().Name} is inactive and being activated.");
                }
                
                Activate();
            }
        }

        protected static void CleanUpGoal(Goal goal)
        {
            goal.RemoveAllSubgoals();
            goal.Terminate();
            Destroy(goal);
        }

        protected void AddMessage(string message)
        {
            EventManager.Instance.Fire(Events.Message, ThinkingAgent.ID, message);
        }

        protected void ShowThots()
        {
            int indent = 0;
            var shortName = ThinkingAgent.ShortName;
            var health = ThinkingAgent.Health;
            var currentWeapon = ThinkingData.WeaponSystem != null ? ThinkingData.WeaponSystem.CurrentWeapon : null;
            var weaponType = currentWeapon != null ? currentWeapon.WeaponType.ToString() : "None";
            var weaponStrength
                = currentWeapon != null
                    ? ThinkingData.WeaponSystem.GetRoundsRemaining(currentWeapon.WeaponType)
                    : 0;
            var stats = $"Stats = [ H: {health} WT: {weaponType} WS: {weaponStrength}";
            var messageString = $"{shortName}'s {stats}]";
            AddMessage(messageString);
            
            var evaluatorString = $"Evaluators = {ThinkingData.Brain.EvaluatorsToString()}";
            AddMessage(evaluatorString);
            
            ShowThots(ref indent);
            
            AddMessage(" ");
        }

        public virtual void ShowThots(ref int indent)
        {
            string textColorString = "<color=black>";
            if (IsComplete) { textColorString = "<color=blue>"; }
            else if (IsInactive) { textColorString = "<color=black>"; }
            else if (HasFailed) { textColorString = "<color=magenta>"; }
            else if (IsActive) { textColorString = "<color=cyan>"; }
            
            var indentString = new string(' ', indent);
            var messageString = $"{textColorString}{indentString}{this}</color>";
            
            if (VerbosityDebug)
            {
                Log.Debug(messageString);
            }
            
            AddMessage(messageString);
        }

        public override string ToString()
        {
            return GetType().Name;
        }
        
        #endregion Methods
    }
}