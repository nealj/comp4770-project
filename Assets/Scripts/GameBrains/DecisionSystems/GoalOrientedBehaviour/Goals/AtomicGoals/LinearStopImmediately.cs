using GameBrains.Entities;
using GameBrains.Motion.Steering.VelocityBased;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class LinearStopImmediately : Goal
    {
        #region Creators

        public static LinearStopImmediately CreateInstance(ThinkingAgent thinkingAgent)
        {
            var linearStopImmediately = CreateInstance<LinearStopImmediately>();
            Initialize(linearStopImmediately, thinkingAgent);
            linearStopImmediately.CreateMove();
            linearStopImmediately.CreateLook();
            return linearStopImmediately;
        }
        
        public static LinearStopImmediately CreateInstance(
            ThinkingAgent thinkingAgent,
            LinearStop move,
            AngularStop look)
        {
            var linearStopImmediately = CreateInstance<LinearStopImmediately>();
            Initialize(linearStopImmediately, thinkingAgent);
            linearStopImmediately.SetMove(move);
            linearStopImmediately.SetLook(look);
            return linearStopImmediately;
        }

        protected static void Initialize(
            LinearStopImmediately linearStopImmediately,
            ThinkingAgent thinkingAgent)
        {
            linearStopImmediately.ThinkingAgent = thinkingAgent;
        }

        protected virtual void CreateMove()
        {
            SetMove(LinearStop.CreateInstance(ThinkingData));
        }

        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }

        protected virtual void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = false;
                Move.NeverCompletes = false;
            }
        }
        
        protected virtual void CreateLook()
        {
            SetLook(null);
        }
        
        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        protected virtual void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = false;
                Look.NeverCompletes = false;
            }
        }

        #endregion Creators

        #region Members and Properties

        LinearStop Move { get; set; }
        AngularStop Look { get; set; }
        
        protected bool MoveHasCompleted => Move == null || !Move.LinearStopActive;
        
        protected bool LookHasCompleted => Look == null || !Look.AngularStopActive;
        
        protected virtual bool HasCompleted => MoveHasCompleted && LookHasCompleted;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Move == null) { CreateMove(); }
            if (Look == null) { CreateLook(); }
            if (Move != null) { ThinkingData.AddSteeringBehaviour(Move); }
            if (Look != null) { ThinkingData.AddSteeringBehaviour(Look); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            if (HasCompleted)
            {
                Status = StatusTypes.Completed;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods
        
        void CleanUpSteering()
        {
            if (Move != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Move);
                Destroy(Move);
                Move = null;
            }

            if (Look != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Look);
                Destroy(Look);
                Look = null;
            }
        }

        public override string ToString() { return $"{base.ToString()}"; }

        #endregion Methods
    }
}