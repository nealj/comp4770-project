using GameBrains.Actuators.Motion.Steering.VelocityBased;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Extensions.Vectors;
using GameBrains.Motion.Steering.VelocityBased;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class AdjustRange : Goal
    {
        #region Creators

        public static AdjustRange CreateInstance(
            ThinkingAgent thinkingAgent,
            KinematicData targetKinematicData,
            float closeEnoughDistance = 1f,
            float idealRange = 5)
        {
            var adjustRange = CreateInstance<AdjustRange>();
            Initialize(adjustRange, thinkingAgent, targetKinematicData, idealRange, closeEnoughDistance);
            adjustRange.CreateMove();
            adjustRange.CreateLook();
            return adjustRange;
        }
        
        public static AdjustRange CreateInstance(
            ThinkingAgent thinkingAgent,
            KinematicData targetKinematicData,
            LinearStop move,
            AngularStop look,
            float closeEnoughDistance = 1f,
            float idealRange = 5f
        )
        {
            var adjustRange = CreateInstance<AdjustRange>();
            Initialize(adjustRange, thinkingAgent, targetKinematicData, idealRange, closeEnoughDistance);
            adjustRange.SetMove(move);
            adjustRange.SetLook(look);
            return adjustRange;
        }

        protected static void Initialize(
            AdjustRange adjustRange,
            ThinkingAgent thinkingAgent,
            KinematicData targetKinematicData,
            float idealRange,
            float closeEnoughDistance)
        {
            adjustRange.ThinkingAgent = thinkingAgent;
            adjustRange.TargetKinematicData = targetKinematicData;
            adjustRange.CloseEnoughDistance = closeEnoughDistance;
            adjustRange.IdealRange = idealRange;
        }

        protected virtual void CreateMove()
        {
            SetMove(Seek.CreateInstance(ThinkingData));
        }

        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }
        
        protected virtual void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = true;
                Move.NeverCompletes = false;
                
                if (Move is Seek seek) { seek.CloseEnoughDistance = CloseEnoughDistance; }
            }
        }
        
        protected virtual void CreateLook()
        {
            SetLook(Face.CreateInstance(ThinkingData));
        }
        
        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        protected virtual void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = true;
                Look.NeverCompletes = true;
                
                if (Look is Face face) { face.OtherTargetKinematicData = TargetKinematicData; }
            }
        }
        
        #endregion Creators

        #region Members and Properties

        public float CloseEnoughDistance { get; set; }
        
        public float IdealRange { get; set; }
        
        protected LinearStop Move { get; set; }
        protected AngularStop Look { get; set; }
        
        KinematicData TargetKinematicData { get; set; }

        VectorXZ idealLocation;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            ThinkingData.AddSteeringBehaviour(Move);
            ThinkingData.AddSteeringBehaviour(Look);
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            if (!ThinkingData.TargetingSystem.IsTargetWithinFieldOfView)
            {
                Status = StatusTypes.Completed;
                return Status;
            }

            VectorXZ directionAwayFromTarget
                = ThinkingData.Location - TargetKinematicData.Location;
            float distanceFromTarget = directionAwayFromTarget.magnitude;

            if (Mathf.Abs(distanceFromTarget - IdealRange) <= CloseEnoughDistance)
            {
                idealLocation = ThinkingData.Location;
                Status = StatusTypes.Completed;
            }
            else
            {
                idealLocation = TargetKinematicData.Location;
                if (Mathf.Approximately(distanceFromTarget, 0))
                {
                    idealLocation += (VectorXZ)Random.onUnitSphere * IdealRange;
                }
                else
                {
                    idealLocation += directionAwayFromTarget.normalized * IdealRange;
                }

                Move.TargetLocation = idealLocation;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            ThinkingData.RemoveSteeringBehaviour(Move);
            ThinkingData.RemoveSteeringBehaviour(Look);
        }

        #endregion Terminate

        #region Methods

        public override string ToString()
        {
            return $"{base.ToString()} idealLocation={idealLocation}";
        }

        #endregion Methods
    }
}