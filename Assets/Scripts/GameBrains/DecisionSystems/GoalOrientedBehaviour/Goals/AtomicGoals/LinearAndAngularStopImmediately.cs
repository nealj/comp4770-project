using GameBrains.Entities;
using GameBrains.Motion.Steering.VelocityBased;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class LinearAndAngularStopImmediately : Goal
    {
        #region Creators

        public static LinearAndAngularStopImmediately CreateInstance(ThinkingAgent thinkingAgent)
        {
            var stopMovingAndTurningImmediately = CreateInstance<LinearAndAngularStopImmediately>();
            Initialize(stopMovingAndTurningImmediately, thinkingAgent);
            stopMovingAndTurningImmediately.CreateMove();
            stopMovingAndTurningImmediately.CreateLook();
            return stopMovingAndTurningImmediately;
        }
        
        public static LinearAndAngularStopImmediately CreateInstance(
            ThinkingAgent thinkingAgent,
            LinearStop move,
            AngularStop look)
        {
            var stopMovingAndTurningImmediately = CreateInstance<LinearAndAngularStopImmediately>();
            Initialize(stopMovingAndTurningImmediately, thinkingAgent);
            stopMovingAndTurningImmediately.SetMove(move);
            stopMovingAndTurningImmediately.SetLook(look);
            return stopMovingAndTurningImmediately;
        }

        protected static void Initialize(
            LinearAndAngularStopImmediately linearAndAngularStopImmediately,
            ThinkingAgent thinkingAgent)
        {
            linearAndAngularStopImmediately.ThinkingAgent = thinkingAgent;
        }

        protected virtual void CreateMove()
        {
            SetMove(LinearStop.CreateInstance(ThinkingData));
        }

        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }

        protected virtual void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = false;
                Move.NeverCompletes = false;
            }
        }
        
        protected virtual void CreateLook()
        {
            SetLook(AngularStop.CreateInstance(ThinkingData));
        }
        
        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        protected virtual void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = false;
                Look.NeverCompletes = false;
            }
        }

        #endregion Creators

        #region Members and Properties

        LinearStop Move { get; set; }
        AngularStop Look { get; set; }
        
        protected bool MoveHasCompleted => Move == null || !Move.LinearStopActive;
        
        protected bool LookHasCompleted => Look == null || !Look.AngularStopActive;
        
        protected virtual bool HasCompleted => MoveHasCompleted && LookHasCompleted;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Move == null) { CreateMove(); }
            if (Look == null) { CreateLook(); }
            if (Move != null) { ThinkingData.AddSteeringBehaviour(Move); }
            if (Look != null) { ThinkingData.AddSteeringBehaviour(Look); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            if (HasCompleted)
            {
                Status = StatusTypes.Completed;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods
        
        void CleanUpSteering()
        {
            if (Move != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Move);
                Destroy(Move);
                Move = null;
            }

            if (Look != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Look);
                Destroy(Look);
                Look = null;
            }
        }

        public override string ToString() { return $"{base.ToString()}"; }

        #endregion Methods
    }
}