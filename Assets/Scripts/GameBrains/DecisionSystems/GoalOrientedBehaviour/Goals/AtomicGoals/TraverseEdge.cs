using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.Actuators.Motion.Steering.VelocityBased;
using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Motion.Steering.VelocityBased;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class TraverseEdge : Goal
    {
        #region Creators

        public static TraverseEdge CreateInstance(
            ThinkingAgent thinkingAgent, 
            PathEdge edgeToTraverse,
            float closeEnoughDistance = 0.5f)
        {
            var traverseEdge = CreateInstance<TraverseEdge>();
            Initialize(traverseEdge, thinkingAgent, edgeToTraverse, closeEnoughDistance);
            traverseEdge.CreateMove();
            traverseEdge.CreateLook();
            return traverseEdge;
        }
        
        public static TraverseEdge CreateInstance(
            ThinkingAgent thinkingAgent, 
            PathEdge edgeToTraverse,
            LinearStop move,
            AngularStop look,
            float closeEnoughDistance = 0.5f)
        {
            var traverseEdge = ScriptableObject.CreateInstance<TraverseEdge>();
            Initialize(traverseEdge, thinkingAgent, edgeToTraverse, closeEnoughDistance);
            traverseEdge.SetMove(move);
            traverseEdge.SetLook(look);
            return traverseEdge;
        }

        protected static void Initialize(
            TraverseEdge traverseEdge,
            ThinkingAgent thinkingAgent,
            PathEdge edgeToTraverse,
            float closeEnoughDistance)
        {
            traverseEdge.ThinkingAgent = thinkingAgent;
            traverseEdge.EdgeToTraverse = edgeToTraverse;
            traverseEdge.Destination = edgeToTraverse.toLocation;
            traverseEdge.CloseEnoughDistance = closeEnoughDistance;
        }
        
        protected virtual void CreateMove()
        {
            SetMove(Seek.CreateInstance(ThinkingData, Destination));
        }
        
        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }

        void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = true;
                Move.NeverCompletes = false;
                
                if (Move is Seek seek) { seek.CloseEnoughDistance = CloseEnoughDistance; }
            }
        }

        protected virtual void CreateLook()
        {
            SetLook(FaceHeading.CreateInstance(ThinkingData));
        }

        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = true;
                Look.NeverCompletes = true;
            }
        }

        #endregion Creators

        #region Members and Properties

        public float CloseEnoughDistance { get; set; }
        
        protected PathEdge EdgeToTraverse { get; set; }
        protected VectorXZ Destination { get; set; }

        protected LinearStop Move { get; set; }
        protected AngularStop Look { get;  set; }

        protected bool MoveHasCompleted => Move == null || !Move.LinearStopActive;
        
        protected bool LookHasCompleted => Look == null || !Look.AngularStopActive;
        
        protected virtual bool HasCompleted => MoveHasCompleted;

        #endregion Members and properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Move == null) { CreateMove(); }
            if (Look == null) { CreateLook(); }
            if (Move != null) { ThinkingData.AddSteeringBehaviour(Move); }
            if (Look != null) { ThinkingData.AddSteeringBehaviour(Look); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();
            
            EdgeToTraverse.Show(ThinkingAgent.Data.ShowPath);

            if (HasCompleted)
            {
                Status = StatusTypes.Completed;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            EdgeToTraverse = PathEdge.CancelEdge(EdgeToTraverse);
            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods

        void CleanUpSteering()
        {
            if (Move != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Move);
                Destroy(Move);
                Move = null;
            }

            if (Look != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Look);
                Destroy(Look);
                Look = null;
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()} destination={EdgeToTraverse.toLocation}";
        }

        #endregion Methods
    }
}