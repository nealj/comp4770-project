using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Motion.Steering.VelocityBased;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class ArriveAtLocation : SeekToLocation
    {
        #region Creators

        public new static ArriveAtLocation CreateInstance(
            ThinkingAgent thinkingAgent, 
            VectorXZ destination,
            float closeEnoughDistance = 0.5f)
        {
            var arriveAtPosition = CreateInstance<ArriveAtLocation>();
            Initialize(arriveAtPosition, thinkingAgent, destination, closeEnoughDistance);
            arriveAtPosition.CreateMove();
            arriveAtPosition.CreateLook();
            return arriveAtPosition;
        }
        
        public new static ArriveAtLocation CreateInstance(
            ThinkingAgent thinkingAgent, 
            VectorXZ destination,
            LinearStop move,
            AngularStop look,
            float closeEnoughDistance = 0.5f)
        {
            var arriveAtPosition = ScriptableObject.CreateInstance<ArriveAtLocation>();
            Initialize(arriveAtPosition, thinkingAgent, destination, closeEnoughDistance);
            arriveAtPosition.SetMove(move);
            arriveAtPosition.SetLook(look);
            return arriveAtPosition;
        }

        protected static void Initialize(
            ArriveAtLocation arriveAtLocation,
            ThinkingAgent thinkingAgent,
            VectorXZ destination,
            float closeEnoughDistance)
        {
            arriveAtLocation.CloseEnoughDistance = closeEnoughDistance;
            SeekToLocation.Initialize(arriveAtLocation, thinkingAgent, destination, closeEnoughDistance);
        }

        protected override void CreateMove()
        {
            SetMove(Arrive.CreateInstance(ThinkingData, Destination));
        }
        
        protected override void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = false;
                Move.NeverCompletes = false;
                
                if (Move is Seek seek) { seek.CloseEnoughDistance = CloseEnoughDistance; }
            }
        }

        #endregion Creators
    }
}