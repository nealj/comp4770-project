using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Motion.Steering.VelocityBased;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class WaitForATime : Goal
    {
        #region Creators

        public static WaitForATime CreateInstance(ThinkingAgent thinkingAgent, float timeToWait = 1f)
        {
            var waitForATime = CreateInstance<WaitForATime>();
            Initialize(waitForATime, thinkingAgent, timeToWait);
            waitForATime.CreateMove();
            waitForATime.CreateLook();
            return waitForATime;
        }
        
        public static WaitForATime CreateInstance(
            ThinkingAgent thinkingAgent,
            LinearStop move,
            AngularStop look,
            float timeToWait = 1f)
        {
            var waitForATime = CreateInstance<WaitForATime>();
            Initialize(waitForATime, thinkingAgent, timeToWait);
            waitForATime.SetMove(move);
            waitForATime.SetLook(look);
            return waitForATime;
        }

        protected static void Initialize(WaitForATime waitForATime, ThinkingAgent thinkingAgent, float timeToWait)
        {
            waitForATime.ThinkingAgent = thinkingAgent;
            waitForATime.TimeToWait = timeToWait;
            waitForATime.TimeAtStart = Time.time;
        }

        protected virtual void CreateMove()
        {
            SetMove(LinearStop.CreateInstance(ThinkingData));
        }

        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }

        protected virtual void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = true;
                Move.NeverCompletes = false;
            }
        }
        
        protected virtual void CreateLook()
        {
            SetLook(null);
        }
        
        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        protected virtual void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = false;
                Look.NeverCompletes = true;
            }
        }
        
        #endregion Creators

        #region Members and Properties

        protected LinearStop Move { get; set; }
        protected AngularStop Look { get; set; }

        public float TimeToWait { get; set; }
        public float TimeAtStart { get; set; }
        
        protected bool MoveHasCompleted => Move == null || !Move.LinearStopActive;
        
        protected bool LookHasCompleted => Look == null || !Look.AngularStopActive;
        
        protected virtual bool HasCompleted => Time.time >= TimeAtStart + TimeToWait;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Move == null) { CreateMove(); }
            if (Look == null) { CreateLook(); }
            if (Move != null) { ThinkingData.AddSteeringBehaviour(Move); }
            if (Look != null) { ThinkingData.AddSteeringBehaviour(Look); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            if (HasCompleted)
            {
                Status = StatusTypes.Completed;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods
        
        void CleanUpSteering()
        {
            if (Move != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Move);
                Destroy(Move);
                Move = null;
            }

            if (Look != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Look);
                Destroy(Look);
                Look = null;
            }
        }

        // public override string ToString() { return base.ToString(); }

        #endregion Methods
    }
}