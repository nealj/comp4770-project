using GameBrains.Actuators.Motion.Steering.VelocityBased;
using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Motion.Steering.VelocityBased;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class SeekToLocation : Goal
    {
        #region Creators

        public static SeekToLocation CreateInstance(
            ThinkingAgent thinkingAgent, 
            VectorXZ destination,
            float closeEnoughDistance = 1f)
        {
            var seekToLocation = CreateInstance<SeekToLocation>();
            Initialize(seekToLocation, thinkingAgent, destination, closeEnoughDistance);
            seekToLocation.CreateMove();
            seekToLocation.CreateLook();
            return seekToLocation;
        }
        
        public static SeekToLocation CreateInstance(
            ThinkingAgent thinkingAgent, 
            VectorXZ destination,
            LinearStop move,
            AngularStop look, 
            float closeEnoughDistance = 1f)
        {
            var seekToLocation = CreateInstance<SeekToLocation>();
            Initialize(seekToLocation, thinkingAgent, destination, closeEnoughDistance);
            seekToLocation.SetMove(move);
            seekToLocation.SetLook(look);
            return seekToLocation;
        }

        protected static void Initialize(
            SeekToLocation seekToLocation,
            ThinkingAgent thinkingAgent,
            VectorXZ destination,
            float closeEnoughDistance)
        {
            seekToLocation.ThinkingAgent = thinkingAgent;
            seekToLocation.Destination = destination;
            seekToLocation.CloseEnoughDistance = closeEnoughDistance;
        }

        protected virtual void CreateMove()
        {
            SetMove(Seek.CreateInstance(ThinkingData, Destination));
        }

        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }

        protected virtual void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = true;
                Move.NeverCompletes = false;
                
                if (Move is Seek seek) { seek.CloseEnoughDistance = CloseEnoughDistance; }
            }
        }
        
        protected virtual void CreateLook()
        {
            SetLook(FaceHeading.CreateInstance(ThinkingData));
        }
        
        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        protected virtual void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = true;
                Look.NeverCompletes = true;
            }
        }

        #endregion Creators

        #region Members and Properties

        public float CloseEnoughDistance { get; set; }
        
        protected VectorXZ Destination { get; set; }
        protected LinearStop Move { get; set; }
        protected AngularStop Look { get; set; }
        
        protected bool MoveHasCompleted => Move == null || !Move.LinearStopActive;
        
        protected bool LookHasCompleted => Look == null || !Look.AngularStopActive;
        
        protected virtual bool HasCompleted => MoveHasCompleted;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Move == null) { CreateMove(); }
            if (Look == null) { CreateLook(); }
            if (Move != null) { ThinkingData.AddSteeringBehaviour(Move); }
            if (Look != null) { ThinkingData.AddSteeringBehaviour(Look); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            if (HasCompleted)
            {
                Status = StatusTypes.Completed;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods

        void CleanUpSteering()
        {
            if (Move != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Move);
                Destroy(Move);
                Move = null;
            }

            if (Look != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Look);
                Destroy(Look);
                Look = null;
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()} destination={Destination}";
        }

        #endregion Methods
    }
}