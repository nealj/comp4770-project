using GameBrains.Actuators.Motion.Steering.VelocityBased;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Extensions.Vectors;
using GameBrains.Motion.Steering.VelocityBased;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class Strafe : Goal
    {
        #region Creators

        public static Strafe CreateInstance(
            ThinkingAgent agent, 
            KinematicData targetKinematicData,
            float closeEnoughDistance = 0.1f)
        {
            var strafe = CreateInstance<Strafe>();
            Initialize(strafe, agent, targetKinematicData, closeEnoughDistance);
            strafe.CreateMove();
            strafe.CreateLook();
            return strafe;
        }
        
        public static Strafe CreateInstance(
            ThinkingAgent thinkingAgent, 
            KinematicData targetKinematicData,
            LinearStop move,
            AngularStop look,
            float closeEnoughDistance = 0.1f
            )
        {
            var strafe = CreateInstance<Strafe>();
            Initialize(strafe, thinkingAgent, targetKinematicData, closeEnoughDistance);
            strafe.SetMove(move);
            strafe.SetLook(look);
            return strafe;
        }

        protected static void Initialize(
            Strafe strafe,
            ThinkingAgent thinkingAgent,
            KinematicData targetKinematicData,
            float closeEnoughDistance)
        {
            strafe.ThinkingAgent = thinkingAgent;
            strafe.TargetKinematicData = targetKinematicData;
            strafe.CloseEnoughDistance = closeEnoughDistance;
            strafe.Clockwise = Random.Range(0, 2) == 0;
        }

        protected virtual void CreateMove()
        {
            SetMove(Seek.CreateInstance(ThinkingData));
        }

        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }

        protected virtual void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = true;
                Move.NeverCompletes = false;

                if (Move is Seek seek) { seek.CloseEnoughDistance = CloseEnoughDistance; }
            }
        }
        
        protected virtual void CreateLook()
        {
            SetLook(Face.CreateInstance(ThinkingData));
        }
        
        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        protected virtual void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = true;
                Look.NeverCompletes = true;

                if (Look is Face face) { face.OtherTargetKinematicData = TargetKinematicData; }
            }
        }

        #endregion Creators

        #region Members and Properties
        
        public float CloseEnoughDistance { get; set; }
        
        protected LinearStop Move { get; set; }
        protected AngularStop Look { get; set; }
        
        KinematicData TargetKinematicData { get; set; }

        VectorXYZ strafeTarget;

        bool Clockwise { get; set; }
        
        protected bool MoveHasCompleted => Move == null || !Move.LinearStopActive;
        
        protected bool LookHasCompleted => Look == null || !Look.AngularStopActive;
        
        protected virtual bool HasCompleted => MoveHasCompleted;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Move == null) { CreateMove(); }
            if (Look == null) { CreateLook(); }
            if (Move != null) { ThinkingData.AddSteeringBehaviour(Move); }
            if (Look != null) { ThinkingData.AddSteeringBehaviour(Look); }

            //TODO: Make parameter.
            if (Random.value <= 0.33f) { Clockwise = !Clockwise; }
            if (Clockwise)
            {
                if (ThinkingData.CanStepRight(out strafeTarget))
                {
                    Move.TargetLocation = (VectorXZ)strafeTarget;
                }
                else
                {
                    Status = StatusTypes.Inactive;
                    Clockwise = !Clockwise;
                }
            }
            else
            {
                if (ThinkingData.CanStepLeft(out strafeTarget))
                {
                    Move.TargetLocation = (VectorXZ)strafeTarget;
                }
                else
                {
                    Status = StatusTypes.Inactive;
                    Clockwise = !Clockwise;
                }
            }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            // If target goes out of view terminate.
            if (!ThinkingData.TargetingSystem.IsTargetWithinFieldOfView)
            {
                Status = StatusTypes.Completed;
                //TODO: Yes, brain surgery. This is an abuse. Perhaps Strafe should be a Composite.
                ThinkingData.Brain.InsertUnderTopSubgoal(
                    LinearAndAngularStopImmediately.CreateInstance(ThinkingAgent));
            }

            // Else if agent reaches the target position set status to inactive so
            // the goal is reactivated on the next update-step.
            else if (ThinkingData.IsAtPosition(strafeTarget, CloseEnoughDistance))
            {
                Status = StatusTypes.Inactive;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods
        
        void CleanUpSteering()
        {
            if (Move != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Move);
                Destroy(Move);
                Move = null;
            }

            if (Look != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Look);
                Destroy(Look);
                Look = null;
            }
        }

        public override string ToString()
        {
            return $"{base.ToString()} strafeTarget={strafeTarget}";
        }

        #endregion Methods
    }
}