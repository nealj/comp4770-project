using GameBrains.Entities;
using GameBrains.Motion.Steering.VelocityBased;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class AngularStopImmediately : Goal
    {
        #region Creators

        public static AngularStopImmediately CreateInstance(ThinkingAgent thinkingAgent)
        {
            var angularStopImmediately = CreateInstance<AngularStopImmediately>();
            Initialize(angularStopImmediately, thinkingAgent);
            angularStopImmediately.CreateMove();
            angularStopImmediately.CreateLook();
            return angularStopImmediately;
        }
        
        public static AngularStopImmediately CreateInstance(
            ThinkingAgent thinkingAgent,
            LinearStop move,
            AngularStop look)
        {
            var angularStopImmediately = CreateInstance<AngularStopImmediately>();
            Initialize(angularStopImmediately, thinkingAgent);
            angularStopImmediately.SetMove(move);
            angularStopImmediately.SetLook(look);
            return angularStopImmediately;
        }

        protected static void Initialize(
            AngularStopImmediately angularStopImmediately, 
            ThinkingAgent thinkingAgent)
        {
            angularStopImmediately.ThinkingAgent = thinkingAgent;
        }

        protected virtual void CreateMove()
        {
            SetMove(null);
        }

        protected virtual void SetMove(LinearStop move)
        {
            Move = move;
            InitializeMove();
        }

        protected virtual void InitializeMove()
        {
            if (Move != null)
            {
                Move.NoStop = false;
                Move.NoSlow = false;
                Move.NeverCompletes = false;
            }
        }
        
        protected virtual void CreateLook()
        {
            SetLook(AngularStop.CreateInstance(ThinkingData));
        }
        
        protected virtual void SetLook(AngularStop look)
        {
            Look = look;
            InitializeLook();
        }

        protected virtual void InitializeLook()
        {
            if (Look != null)
            {
                Look.NoStop = false;
                Look.NoSlow = false;
                Look.NeverCompletes = false;
            }
        }

        #endregion Creators

        #region Members and Properties

        LinearStop Move { get; set; }
        AngularStop Look { get; set; }
        
        protected bool MoveHasCompleted => Move == null || !Move.LinearStopActive;
        
        protected bool LookHasCompleted => Look == null || !Look.AngularStopActive;
        
        protected virtual bool HasCompleted => MoveHasCompleted && LookHasCompleted;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Move == null) { CreateMove(); }
            if (Look == null) { CreateLook(); }
            if (Move != null) { ThinkingData.AddSteeringBehaviour(Move); }
            if (Look != null) { ThinkingData.AddSteeringBehaviour(Look); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            if (HasCompleted)
            {
                Status = StatusTypes.Completed;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods
        
        void CleanUpSteering()
        {
            if (Move != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Move);
                Destroy(Move);
                Move = null;
            }

            if (Look != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Look);
                Destroy(Look);
                Look = null;
            }
        }

        //public override string ToString() { return base.ToString(); }

        #endregion Methods
    }
}