using GameBrains.Entities;
using GameBrains.Motion.Steering.VelocityBased;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals
{
    public class WanderIndefinitely : Goal
    {
        #region Creators

        public static WanderIndefinitely CreateInstance(ThinkingAgent agent)
        {
            var wanderIndefinitely = CreateInstance<WanderIndefinitely>();
            Initialize(wanderIndefinitely, agent);
            wanderIndefinitely.CreateWander();
            return wanderIndefinitely;
        }
        
        public static WanderIndefinitely CreateInstance(
            ThinkingAgent thinkingAgent,
            Wander wander)
        {
            var wanderIndefinitely = CreateInstance<WanderIndefinitely>();
            Initialize(wanderIndefinitely, thinkingAgent);
            wanderIndefinitely.SetWander(wander);
            return wanderIndefinitely;
        }


        protected static void Initialize(WanderIndefinitely wanderIndefinitely, ThinkingAgent agent)
        {
            wanderIndefinitely.ThinkingAgent = agent;
        }

        protected virtual void CreateWander()
        {
            SetWander(Wander.CreateInstance(ThinkingData));
        }
        
        protected virtual void SetWander(Wander wander)
        {
            Wander = wander;
            InitializeWander();
        }

        protected virtual void InitializeWander()
        {
            if (Wander != null)
            {
                Wander.NoStop = false;
                Wander.NoSlow = false;
                Wander.NeverCompletes = false;
            }
        }

        #endregion Creators

        #region Members and Properties

        public Wander Wander { get; set;  }

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            if (Wander == null) { CreateWander(); }
            if (Wander != null) { ThinkingData.AddSteeringBehaviour(Wander); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            // Never completes so must be externally Terminated.
            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();
            
            CleanUpSteering();
        }

        #endregion Terminate

        #region Methods
        
        void CleanUpSteering()
        {
            if (Wander != null)
            {
                ThinkingData.RemoveSteeringBehaviour(Wander);
                Destroy(Wander);
                Wander = null;
            }
        }

        // public override string ToString() { return base.ToString(); }

        #endregion Methods
    }
}