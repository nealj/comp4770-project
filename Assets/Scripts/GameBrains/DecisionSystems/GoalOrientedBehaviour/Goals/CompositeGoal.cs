using System.Collections.Generic;
using GameBrains.Extensions.CollectionsExtensions;
using GameBrains.Extensions.DictionaryExtensions;
using GameBrains.Extensions.Lists;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals
{
    public abstract class CompositeGoal : Goal
    {
        #region Enable/Disable/Destroy

        public override void OnEnable()
        {
            base.OnEnable();
            // Normally the sub-goals are stacked but sometimes we can queue them.
            // See Command Queuing.
            // So we use a list instead of a stack for flexibility.
            SubgoalStack = new List<Goal>();
        }

        #endregion Enable/Disable/Destroy

        #region Process Subgoals

        protected StatusTypes ProcessSubgoals()
        {
            if (VerbosityDebug)
            {
                var subgoalsAsString = SubgoalStack.ToNumberedItemsString();
                Log.Debug($"{GetType().Name} processing subgoals\n{subgoalsAsString}");
            }

            // Remove all completed and failed goals from the front of the subgoal list
            while (!SubgoalStack.IsEmpty() &&
                   (SubgoalStack.PeekInStack().IsComplete || SubgoalStack.PeekInStack().HasFailed))
            {
                CleanUpGoal(SubgoalStack.Pop());
            }

            if (VerbosityDebug)
            {
                var subgoalsAsString = SubgoalStack.ToNumberedItemsString();
                subgoalsAsString = subgoalsAsString.IsNullOrEmpty() ? "." : $"\n{subgoalsAsString}";
                Log.Debug(
                    $"{GetType().Name} removed completed and failed subgoals{subgoalsAsString}");
            }

            // If any subgoals remain, process the one at the front of the top of the stack
            if (!SubgoalStack.IsEmpty())
            {
                // Grab the status of the front-most subgoal
                var frontSubgoal = SubgoalStack.PeekInStack();
                StatusTypes statusOfSubGoals = frontSubgoal.Process();

                // We have to test for the special case where the front-most reports
                // 'completed' *and* the subgoal list contains additional goals. When this
                // is the case, to ensure the parent keeps processing its subgoal list we
                // must return the 'active' status.
                if (statusOfSubGoals == StatusTypes.Completed && SubgoalStack.Count > 1)
                {
                    Status = StatusTypes.Active;
                    if (VerbosityDebug)
                    {
                        var subgoalsAsString = SubgoalStack.ToNumberedItemsString();
                        subgoalsAsString = subgoalsAsString.IsNullOrEmpty() ? "." : $"\n{subgoalsAsString}";
                        Log.Debug($"{GetType().Name} processed front subgoal {frontSubgoal} but subgoals remain with status {Status}{subgoalsAsString}");
                    }

                    return Status;
                }

                Status = statusOfSubGoals;

                if (VerbosityDebug)
                {
                    Log.Debug($"{GetType().Name} processed front subgoal {frontSubgoal} with status {Status}.");
                }

                return Status;
            }

            Status = StatusTypes.Completed;

            if (VerbosityDebug)
            {
                var subgoalsAsString = SubgoalStack.ToNumberedItemsString();
                subgoalsAsString = subgoalsAsString.IsNullOrEmpty() ? "." : $"\n{subgoalsAsString}";
                Log.Debug($"{GetType().Name} processed all subgoals with status {Status}{subgoalsAsString}");
            }

            // no more subgoals to process
            return Status;
        }

        #endregion Process Subgoals

        #region Subgoals

        public List<Goal> SubgoalStack { get; private set; }

        public override void PushSubgoal(Goal subgoal)
        {
            if (VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} adding subgoal {subgoal}.");
            }
            
            SubgoalStack.Push(subgoal);
        }
        
        public override void InsertUnderTopSubgoal(Goal subgoal)
        {
            SubgoalStack.InsertUnderTop(subgoal);
        }

        public override void RemoveAllSubgoals()
        {
            if (VerbosityDebug)
            {
                var subgoalsAsString = SubgoalStack.ToNumberedItemsString();
                subgoalsAsString = subgoalsAsString.IsNullOrEmpty() ? "." : $"\n{subgoalsAsString}";
                Log.Debug($"{GetType().Name} removing all subgoals{subgoalsAsString}");
            }
            
            foreach (Goal goal in SubgoalStack)
            {
                CleanUpGoal(goal);
            }

            SubgoalStack.Clear();
        }

        #endregion Subgoals

        #region Methods
        
        public override void ShowThots(ref int indent)
        {
            base.ShowThots(ref indent);

            indent++;

            // Display like a queue
            for (int i = SubgoalStack.Count - 1; i >= 0; i--)
            {
                SubgoalStack[i].ShowThots(ref indent);
            }

            // Display like a stack.
            // foreach (Goal goal in SubgoalStack)
            // {
            // 	goal.ShowThots(ref indent);
            // }
        }

        // public override string ToString()
        // {
        //     return base.ToString();
        // }

        #endregion Methods
    }
}