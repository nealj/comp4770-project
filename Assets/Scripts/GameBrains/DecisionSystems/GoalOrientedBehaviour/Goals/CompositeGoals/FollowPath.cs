using System.Collections.Generic;
using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.Entities;
using GameBrains.Extensions.Lists;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class FollowPath : CompositeGoal
    {
        #region Creators

        public static FollowPath CreateInstance(
            ThinkingAgent thinkingAgent, 
            Path pathToFollow,
            float closeEnoughDistance = 0.5f)
        {
            var followPath = CreateInstance<FollowPath>();
            Initialize(followPath, thinkingAgent, pathToFollow, closeEnoughDistance);
            return followPath;
        }

        protected static void Initialize(
            FollowPath followPath, 
            ThinkingAgent thinkingAgent,
            Path pathToFollow,
            float closeEnoughDistance)
        {
            followPath.ThinkingAgent = thinkingAgent;
            followPath.PathToFollow = pathToFollow;
            followPath.CloseEnoughDistance = closeEnoughDistance;
        }

        #endregion Creators

        #region Members and Properties
        
        public float CloseEnoughDistance { get; set; }
        
        protected Path PathToFollow { get; set; }
        protected List<PathEdge> EdgesToTraverse => PathToFollow.PathEdges;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();
            
            if (ThinkingAgent.Data.SmoothPath)
            {
                PathToFollow = Path.SmoothPath(ThinkingAgent, PathToFollow);
            }
            
            if (EdgesToTraverse.IsEmpty())
            {
                Status = StatusTypes.Completed;

                return;
            }

            // If this goal is reactivated then there may be some existing subgoals that must be removed
            RemoveAllSubgoals();

            Status = StatusTypes.Active;
            PathEdge edgeToTraverse = EdgesToTraverse.Dequeue();
            PushSubgoal(TraverseEdge.CreateInstance(ThinkingAgent, edgeToTraverse, CloseEnoughDistance));
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();
            
            PathToFollow.Show(ThinkingAgent.Data.ShowPath);

            Status = ProcessSubgoals();

            // If there are no subgoals present check to see if the path still has edges remaining.
            // If it does then call activate to grab the next edge.
            if (Status != StatusTypes.Completed) { return Status; }

            if (!EdgesToTraverse.IsEmpty()) { Activate(); }
            
            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();
            
            PathToFollow = Path.CancelPath(PathToFollow);
        }

        #endregion Terminate

        #region Methods

        public string ToStringWithPath()
        {
            return $"{base.ToString()}\n{EdgesToTraverse.ToNumberedItemsString()}";
        }
        
        public override string ToString()
        {
            return $"{base.ToString()} destination={PathToFollow.Destination}";
        }

        #endregion Methods
    }
}