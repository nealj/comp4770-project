using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.Entities;
using GameBrains.Entities.Triggers;
using GameBrains.Entities.Types;
using GameBrains.EventSystem;
using GameBrains.Extensions.Lists;
using GameBrains.Extensions.Vectors;
using GameBrains.Map;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class GetEntityWithTypes : CompositeGoal
    {
        #region Creators

        public static GetEntityWithTypes CreateInstance(
            ThinkingAgent thinkingAgent, 
            EntityTypes entityTypesToGet,
            float closeEnoughDistance = 0.5f)
        {
            var getEntityWithTypes = CreateInstance<GetEntityWithTypes>();
            Initialize(getEntityWithTypes, thinkingAgent, entityTypesToGet, closeEnoughDistance);
            return getEntityWithTypes;
        }

        protected static void Initialize(
            GetEntityWithTypes getEntityWithTypes,
            ThinkingAgent thinkingAgent,
            EntityTypes entityTypesToGet,
            float closeEnoughDistance)
        {
            getEntityWithTypes.ThinkingAgent = thinkingAgent;
            getEntityWithTypes.EntityTypesToGet = entityTypesToGet;
            getEntityWithTypes.CloseEnoughDistance = closeEnoughDistance;
        }

        #endregion Creators

        #region Members and Properties

        public float CloseEnoughDistance { get; set; }
        
        EntityTypes EntityTypesToGet { get; set; }

        Entity EntityWithTypes { get; set; }
        Trigger TriggerEntity { get; set; }
        
        protected bool pathRequestPending;

        bool HasItemBeenStolen =>
            TriggerEntity &&
            !TriggerEntity.IsActive &&
            TriggerEntity.TriggeringAgent != ThinkingAgent &&
            ThinkingData.HasLineOfSight(EntityWithTypes.Data.Position);

        #endregion Members and properties

        #region Activate

        public override void Activate()
        {
            base.Activate();
            
            // If this goal is reactivated then there may be some existing subgoals that must be removed.
            RemoveAllSubgoals();

            if (EventManager.Instance != null)
            {
                EventManager.Instance.Subscribe<PathToEntityWithTypesReadyEventPayload>(
                    Events.PathToEntityWithTypesReady,
                    OnPathToEntityWithTypesReady);

                EventManager.Instance.Subscribe<NoPathToEntityWithTypesAvailableEventPayload>(
                    Events.NoPathToEntityWithTypesAvailable,
                    OnNoPathToEntityWithTypesAvailable);
            }
            else
            {
                Debug.LogWarning("Event manager missing. Unable to subscribe to pathfinding events.");
            }

            TriggerEntity = null;
            EntityWithTypes = null;

            if (EventManager.Instance != null)
            {
                pathRequestPending = true;
                // Request a path to the item.
                EventManager.Instance.Enqueue(
                    Events.PathToEntityWithTypesRequest,
                    new PathToEntityWithTypesRequestEventPayload(
                        ThinkingAgent,
                        EntityTypesToGet));
            }
            else
            {
                Debug.LogWarning("Event manager missing. Unable to request paths.");
            }

            // The agent may have to wait a few update cycles before a path is
            // calculated so for appearances sake it just wanders.
            PushSubgoal(WaitForATime.CreateInstance(ThinkingAgent, float.MaxValue));
            //PushSubgoal(WanderIndefinitely.CreateInstance(ThinkingAgent));
            //TODO: Create a subgoal to Wander but stay close by.
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            Status = HasItemBeenStolen ? StatusTypes.Failed : ProcessSubgoals();
            
            // If QuickPath finishes before we get a response from the PathPlanner, keep Active.
            if (Status == StatusTypes.Completed && pathRequestPending)
            {
                PushSubgoal(WaitForATime.CreateInstance(ThinkingAgent, float.MaxValue));
                Status = StatusTypes.Active;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            if (EventManager.Instance != null)
            {
                EventManager.Instance.Unsubscribe<PathToEntityWithTypesReadyEventPayload>(
                    Events.PathToEntityWithTypesReady,
                    OnPathToEntityWithTypesReady);

                EventManager.Instance.Unsubscribe<NoPathToEntityWithTypesAvailableEventPayload>(
                    Events.NoPathToEntityWithTypesAvailable,
                    OnNoPathToEntityWithTypesAvailable);
            }

            RemoveAllSubgoals(); //TODO: Is this needed?
            Status = StatusTypes.Completed;
        }

        #endregion Terminate

        #region Methods

        public override string ToString()
        {
            return $"{base.ToString()} entityTypes={EntityTypesToGet}";
        }

        #endregion Methods

        #region Event Handlers

        bool OnPathToEntityWithTypesReady(Event<PathToEntityWithTypesReadyEventPayload> eventArg)
        {
            PathToEntityWithTypesReadyEventPayload payload = eventArg.EventData;

            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }

            if (VerbosityDebug)
            {
                Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} found path to {payload.entityWithTypes.ShortName} using path\n{payload.path}</color>");
            }

            pathRequestPending = false;
            
            RemoveAllSubgoals();

            EntityWithTypes = payload.entityWithTypes;
            TriggerEntity = EntityWithTypes.GetComponent<Trigger>();

            Path splicePath = Path.SmoothPath(ThinkingAgent, payload.path);

            var edgesToFollow = splicePath.PathEdges;
            if (!edgesToFollow.IsEmpty() && 
                !ThinkingAgent.Data.CanMoveTo(MapData.SurfacePoint(edgesToFollow[0].fromLocation)))
            {
                // QuickPath lead us astray!
                Status = StatusTypes.Failed;
                
                Path.CancelPath(ref splicePath); // clean up

                if (VerbosityDebug)
                {
                    Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} splice path failed.</color>");
                }
            }
            else if (edgesToFollow.IsEmpty())
            {
                // QuickPath got us to the destination. Wow!
                Status = StatusTypes.Completed;
                
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} completed.</color>");
                }
            }
            else
            {
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} adding path subgoal FollowPath\n{splicePath}</color>");
                }

                var followPath = FollowPath.CreateInstance(ThinkingAgent, splicePath, CloseEnoughDistance);
                followPath.verbosity = verbosity;
                PushSubgoal(followPath);
            }

            return true;
        }

        bool OnNoPathToEntityWithTypesAvailable(Event<NoPathToEntityWithTypesAvailableEventPayload> eventArg)
        {
            NoPathToEntityWithTypesAvailableEventPayload payload = eventArg.EventData;

            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }

            pathRequestPending = false;
            Status = StatusTypes.Failed;
            
            if (VerbosityDebug)
            {
                Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} got NO path to {EntityTypesToGet}</color>");
            }
            
            return true;
        }

        #endregion Event Handlers
    }
}