using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.Entities;
using GameBrains.Entities.Triggers;
using GameBrains.Entities.Types;
using GameBrains.EventSystem;
using GameBrains.Extensions.Lists;
using GameBrains.Extensions.Vectors;
using GameBrains.Map;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class GetEntityOfType : CompositeGoal
    {
        #region Creators

        public static GetEntityOfType CreateInstance(
            ThinkingAgent thinkingAgent, 
            EntityType entityTypeToGet,
            float closeEnoughDistance = 0.5f)
        {
            var getEntityOfType = CreateInstance<GetEntityOfType>();
            Initialize(getEntityOfType, thinkingAgent, entityTypeToGet, closeEnoughDistance);
            return getEntityOfType;
        }

        protected static void Initialize(
            GetEntityOfType getEntityOfType,
            ThinkingAgent thinkingAgent,
            EntityType entityTypeToGet,
            float closeEnoughDistance)
        {
            getEntityOfType.ThinkingAgent = thinkingAgent;
            getEntityOfType.EntityTypeToGet = entityTypeToGet;
            getEntityOfType.CloseEnoughDistance = closeEnoughDistance;
        }

        #endregion Creators

        #region Members and Properties

        public float CloseEnoughDistance { get; set; }
        
        EntityType EntityTypeToGet { get; set; }

        Entity EntityOfType { get; set; }
        Trigger TriggerEntity { get; set; }

        bool HasItemBeenStolen =>
            TriggerEntity &&
            !TriggerEntity.IsActive &&
            TriggerEntity.TriggeringAgent != ThinkingAgent &&
            ThinkingData.HasLineOfSight(EntityOfType.Data.Position);

        #endregion Members and properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            EventManager.Instance.Subscribe<PathToEntityWithTypesReadyEventPayload>(
                Events.PathToEntityWithTypesReady,
                OnPathToEntityWithTypesReady);

            EventManager.Instance.Subscribe<NoPathToEntityWithTypesAvailableEventPayload>(
                Events.NoPathToEntityWithTypesAvailable,
                OnNoPathToEntityWithTypesAvailable);

            TriggerEntity = null;
            EntityOfType = null;

            // Request a path to the item.
            EventManager.Instance.Enqueue(
                Events.PathToEntityWithTypesRequest,
                new PathToEntityWithTypesRequestEventPayload(
                    ThinkingAgent,
                    new EntityTypes(EntityTypeToGet)));

            // The agent may have to wait a few update cycles before a path is
            // calculated so for appearances sake it just wanders.
            PushSubgoal(WaitForATime.CreateInstance(ThinkingAgent, float.MaxValue));
            //PushSubgoal(WanderIndefinitely.CreateInstance(ThinkingAgent));
            //TODO: Create a subgoal to Wander but stay close by.
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            Status = HasItemBeenStolen ? StatusTypes.Failed : ProcessSubgoals();

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            EventManager.Instance.Unsubscribe<PathToEntityWithTypesReadyEventPayload>(
                Events.PathToEntityWithTypesReady,
                OnPathToEntityWithTypesReady);

            EventManager.Instance.Unsubscribe<NoPathToEntityWithTypesAvailableEventPayload>(
                Events.NoPathToEntityWithTypesAvailable,
                OnNoPathToEntityWithTypesAvailable);

            RemoveAllSubgoals(); //TODO: Is this needed?
            Status = StatusTypes.Completed;
        }

        #endregion Terminate

        #region Methods

        public override string ToString()
        {
            return $"{base.ToString()} entityType={EntityTypeToGet}";
        }

        #endregion Methods

        #region Event Handlers

        bool OnPathToEntityWithTypesReady(Event<PathToEntityWithTypesReadyEventPayload> eventArg)
        {
            PathToEntityWithTypesReadyEventPayload payload = eventArg.EventData;

            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }

            if (VerbosityDebug)
            {
                Log.Debug(
                    $"<color=green>{ThinkingAgent.name} {GetType().Name} found path to {payload.entityWithTypes} using path\n{payload.path}</color>");
            }

            RemoveAllSubgoals();

            EntityOfType = payload.entityWithTypes;
            TriggerEntity = EntityOfType.GetComponent<Trigger>();

            Path splicePath = Path.SmoothPath(ThinkingAgent, payload.path);

            var edgesToFollow = splicePath.PathEdges;
            if (!edgesToFollow.IsEmpty() && 
                !ThinkingAgent.Data.CanMoveTo(MapData.SurfacePoint(edgesToFollow[0].fromLocation)))
            {
                // QuickPath lead us astray!
                Status = StatusTypes.Failed;
                
                Path.CancelPath(ref splicePath); // clean up

                if (VerbosityDebug)
                {
                    Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} splice path failed.</color>");
                }
            }
            else if (edgesToFollow.IsEmpty())
            {
                // QuickPath got us to the destination. Wow!
                Status = StatusTypes.Completed;
                
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} completed.</color>");
                }
            }
            else
            {
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} adding path subgoal FollowPath\n{splicePath}</color>");
                }

                PushSubgoal(FollowPath.CreateInstance(ThinkingAgent, splicePath, CloseEnoughDistance));
            }

            return true;
        }

        bool OnNoPathToEntityWithTypesAvailable(Event<NoPathToEntityWithTypesAvailableEventPayload> eventArg)
        {
            NoPathToEntityWithTypesAvailableEventPayload payload = eventArg.EventData;

            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }

            if (VerbosityDebug)
            {
                Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} got NO path to {EntityTypeToGet}</color>");
            }

            Status = StatusTypes.Failed;

            return true;
        }

        #endregion Event Handlers
    }
}