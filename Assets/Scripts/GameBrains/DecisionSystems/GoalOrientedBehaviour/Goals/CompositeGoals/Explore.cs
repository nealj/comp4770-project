using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.Actuators.Motion.Navigation.SearchGraph;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.Entities;
using GameBrains.EventSystem;
using GameBrains.Extensions.Lists;
using GameBrains.Extensions.Vectors;
using GameBrains.Map;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class Explore : CompositeGoal
    {
        #region Creators

        public static Explore CreateInstance(
            ThinkingAgent thinkingAgent,
            float closeEnoughDistance = 0.5f)
        {
            var explore = CreateInstance<Explore>();
            Initialize(explore, thinkingAgent, closeEnoughDistance);
            return explore;
        }

        protected static void Initialize(
            Explore explore, 
            ThinkingAgent thinkingAgent,
            float closeEnoughDistance)
        {
            explore.ThinkingAgent = thinkingAgent;
            explore.CloseEnoughDistance = closeEnoughDistance;
        }

        #endregion Creators

        #region Members and Properties

        public float CloseEnoughDistance { get; set; }
        
        protected VectorXZ? Destination { get; set; }

        protected bool pathRequestPending;

        protected Graph Graph
        {
            get
            {
                if (graph == null) { graph = FindObjectOfType<Graph>(); }

                return graph;
            }
        }
        Graph graph;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();
            
            // If this goal is reactivated then there may be some existing subgoals that must be removed.
            RemoveAllSubgoals();

            if (EventManager.Instance != null)
            {
                EventManager.Instance.Subscribe<PathToLocationReadyEventPayload>(
                    Events.PathToLocationReady,
                    OnPathToLocationReady);

                EventManager.Instance.Subscribe<NoPathToLocationAvailableEventPayload>(
                    Events.NoPathToLocationAvailable,
                    OnNoPathToLocationAvailable);
            }
            else
            {
                Debug.LogWarning("Event manager missing. Unable to subscribe to pathfinding events.");
            }

            if (!Destination.HasValue)
            {
                if (Graph.NodeCollection != null && Graph.NodeCollection.Nodes.Length != 0)
                {
                    Node[] nodes = Graph.NodeCollection.Nodes;
                    int index = Random.Range(0, nodes.Length);

                    // Grab a random location.
                    Destination = nodes[index].Location;
                }
                else
                {
                    Destination = VectorXZ.zero;
                }
            }

            if (EventManager.Instance != null)
            {
                pathRequestPending = true;
                EventManager.Instance.Enqueue(
                    Events.PathToLocationRequest,
                    new PathToLocationRequestEventPayload(ThinkingAgent, Destination.Value));
            }
            else
            {
                Debug.LogWarning("Event manager missing. Unable to request paths.");
            }

            // May have to wait a few update cycles before a path is
            // calculated so for appearances sake simply Arrive toward the
            // destination until a path has been found.
            // QuickPath.
            PushSubgoal(ArriveAtLocation.CreateInstance(ThinkingAgent, Destination.Value, CloseEnoughDistance));
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            Status = ProcessSubgoals();

            // If QuickPath finishes before we get a response from the PathPlanner, keep Active.
            if (Status == StatusTypes.Completed && pathRequestPending)
            {
                PushSubgoal(WaitForATime.CreateInstance(ThinkingAgent, float.MaxValue));
                Status = StatusTypes.Active;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            if (EventManager.Instance != null)
            {
                EventManager.Instance.Unsubscribe<PathToLocationReadyEventPayload>(
                    Events.PathToLocationReady,
                    OnPathToLocationReady);

                EventManager.Instance.Unsubscribe<NoPathToLocationAvailableEventPayload>(
                    Events.NoPathToLocationAvailable,
                    OnNoPathToLocationAvailable);
            }
            
            RemoveAllSubgoals(); //TODO: Is this needed?
            Status = StatusTypes.Completed;
        }

        #endregion Terminate

        #region Methods

        public override string ToString()
        {
            var destination
                = Destination.HasValue ? Destination.Value.ToString() : string.Empty;
            return $"{base.ToString()} destination={destination}";
        }

        #endregion Methods

        #region Event Handlers

        bool OnPathToLocationReady(Event<PathToLocationReadyEventPayload> eventArg)
        {
            PathToLocationReadyEventPayload payload = eventArg.EventData;
            
            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }

            if (VerbosityDebug)
            {
                Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} found path to {payload.path.Destination} using path\n{payload.path}</color>");
            }

            pathRequestPending = false;

            RemoveAllSubgoals();

            Path splicePath = Path.SmoothPath(ThinkingAgent, payload.path);

            var edgesToFollow = splicePath.PathEdges;
            if (!edgesToFollow.IsEmpty() && 
                !ThinkingAgent.Data.CanMoveTo(MapData.SurfacePoint(edgesToFollow[0].fromLocation)))
            {
                // QuickPath lead us astray!
                Status = StatusTypes.Failed;

                Path.CancelPath(ref splicePath); // clean up

                if (VerbosityDebug)
                {
                    Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} splice path failed.</color>");
                }
            }
            else if (edgesToFollow.IsEmpty())
            {
                // QuickPath got us to the destination. Wow!
                Status = StatusTypes.Completed;
                
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} completed.</color>");
                }
            }
            else
            {
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} adding path subgoal FollowPath\n{splicePath}</color>");
                }

                PushSubgoal(FollowPath.CreateInstance(ThinkingAgent, splicePath, CloseEnoughDistance));
            }

            return true;
        }

        bool OnNoPathToLocationAvailable(Event<NoPathToLocationAvailableEventPayload> eventArg)
        {
            NoPathToLocationAvailableEventPayload payload = eventArg.EventData;

            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }

            pathRequestPending = false;
            Status = StatusTypes.Failed;

            if (VerbosityDebug)
            {
                Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} got NO path.</color>");
            }

            RemoveAllSubgoals();

            // TODO: Yes, brain surgery. This is an abuse.
            ThinkingData.Brain.InsertUnderTopSubgoal(
                LinearAndAngularStopImmediately.CreateInstance(ThinkingAgent));

            return true;
        }

        #endregion Event Handlers
    }
}