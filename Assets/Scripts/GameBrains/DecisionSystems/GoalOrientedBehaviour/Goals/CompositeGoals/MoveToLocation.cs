using GameBrains.Actuators.Motion.Navigation.PathManagement;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.Entities;
using GameBrains.EventSystem;
using GameBrains.Extensions.Lists;
using GameBrains.Extensions.Vectors;
using GameBrains.Map;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class MoveToLocation : CompositeGoal
    {
        #region Creators

        public static MoveToLocation CreateInstance(
            ThinkingAgent thinkingAgent, 
            VectorXZ destination,
            float closeEnoughDistance = 0.5f)
        {
            var moveToLocation = CreateInstance<MoveToLocation>();
            Initialize(moveToLocation, thinkingAgent, destination, closeEnoughDistance);
            return moveToLocation;
        }

        protected static void Initialize(
            MoveToLocation moveToLocation,
            ThinkingAgent thinkingAgent,
            VectorXZ destination,
            float closeEnoughDistance)
        {
            moveToLocation.ThinkingAgent = thinkingAgent;
            moveToLocation.Destination = destination;
            moveToLocation.CloseEnoughDistance = closeEnoughDistance;
        }

        #endregion Creators

        #region Members and Properties

        public float CloseEnoughDistance { get; set; }
        
        protected VectorXZ Destination { get; set; }
        
        protected bool pathRequestPending;

        #endregion Members and properties

        #region Activate

        public override void Activate()
        {
            base.Activate();
            
            RemoveAllSubgoals();

            if (EventManager.Instance != null)
            {
                EventManager.Instance.Subscribe<PathToLocationReadyEventPayload>(
                    Events.PathToLocationReady,
                    OnPathToLocationReady);

                EventManager.Instance.Subscribe<NoPathToLocationAvailableEventPayload>(
                    Events.NoPathToLocationAvailable,
                    OnNoPathToLocationAvailable);

                pathRequestPending = true;
                EventManager.Instance.Enqueue(
                    Events.PathToLocationRequest,
                    new PathToLocationRequestEventPayload(ThinkingAgent, Destination));
            }
            else
            {
                Debug.LogWarning("Event manager missing. Unable to subscribe to pathfinding events.");;
            }

            // May have to wait a few update cycles before a path is
            // calculated so for appearances sake simply Seek toward the
            // destination until a path has been found.
            // QuickPath
            PushSubgoal(SeekToLocation.CreateInstance(ThinkingAgent, Destination));
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();
            
            Status = ProcessSubgoals();

            // if any of the subgoals have failed then this goal re-plans
            ReactivateIfFailed();
            
            // If QuickPath finishes before we get a response from the PathPlanner, keep Active.
            if (Status == StatusTypes.Completed && pathRequestPending)
            {
                PushSubgoal(WaitForATime.CreateInstance(ThinkingAgent, float.MaxValue));
                Status = StatusTypes.Active;
            }
           
            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            if (EventManager.Instance != null)
            {
                EventManager.Instance.Unsubscribe<PathToLocationReadyEventPayload>(
                    Events.PathToLocationReady,
                    OnPathToLocationReady);

                EventManager.Instance.Unsubscribe<NoPathToLocationAvailableEventPayload>(
                    Events.NoPathToLocationAvailable,
                    OnNoPathToLocationAvailable);
            }
            
            if (VerbosityDebug)
            {
                Log.Debug($"{GetType().Name} terminated with status {Status}.");
            }
        }

        #endregion Terminate

        #region Methods

        public override string ToString()
        {
            return $"{base.ToString()} destination={Destination}";
        }

        #endregion Methods

        #region Event Handlers

        bool OnPathToLocationReady(Event<PathToLocationReadyEventPayload> eventArg)
        {
            PathToLocationReadyEventPayload payload = eventArg.EventData;

            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }
            
            if (VerbosityDebug)
            {
                Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} found path to {payload.path.Destination} using path\n{payload.path}</color>");
            }

            pathRequestPending = false;
            RemoveAllSubgoals();

            Path splicePath = Path.SmoothPath(ThinkingAgent, payload.path);

            var edgesToFollow = splicePath.PathEdges;
            if (!edgesToFollow.IsEmpty() &&
                !ThinkingAgent.Data.CanMoveTo(MapData.SurfacePoint(edgesToFollow[0].fromLocation)))
            {
                // QuickPath lead us astray!
                Status = StatusTypes.Failed;
                
                Path.CancelPath(ref splicePath); // clean up

                if (VerbosityDebug)
                {
                    Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} splice path failed.</color>");
                }
            }
            else if (edgesToFollow.IsEmpty())
            {
                // QuickPath got us to the destination. Wow!
                Status = StatusTypes.Completed;
                
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} completed.</color>");
                }
            }
            else
            {
                if (VerbosityDebug)
                {
                    Log.Debug($"<color=green>{ThinkingAgent.name} {GetType().Name} adding path subgoal FollowPath\n{splicePath}</color>");
                }

                PushSubgoal(FollowPath.CreateInstance(ThinkingAgent, splicePath, CloseEnoughDistance));
            }

            return true;
        }

        bool OnNoPathToLocationAvailable(Event<NoPathToLocationAvailableEventPayload> eventArg)
        {
            NoPathToLocationAvailableEventPayload payload = eventArg.EventData;
            
            // Check if event is for us.
            if (payload.pathfindingAgent != ThinkingAgent) { return false; }

            pathRequestPending = false;
            Status = StatusTypes.Failed;
            
            if (VerbosityDebug)
            {
                Log.Debug($"<color=red>{ThinkingAgent.name} {GetType().Name} got NO path with status {Status}.</color>");
            }

            return true;
        }

        #endregion Event Handlers
    }
}