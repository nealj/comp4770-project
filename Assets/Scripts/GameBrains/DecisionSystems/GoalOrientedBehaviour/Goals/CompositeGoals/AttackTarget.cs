using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.Entities;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class AttackTarget : CompositeGoal
    {
        #region Creators

        public static AttackTarget CreateInstance(
            ThinkingAgent thinkingAgent,
            float closeEnoughDistance = 0.5f)
        {
            var attackTarget = CreateInstance<AttackTarget>();
            Initialize(attackTarget, thinkingAgent, closeEnoughDistance);
            return attackTarget;
        }

        protected static void Initialize(
            AttackTarget attackTarget, 
            ThinkingAgent thinkingAgent,
            float closeEnoughDistance)
        {
            attackTarget.ThinkingAgent = thinkingAgent;
            attackTarget.CloseEnoughDistance = closeEnoughDistance;
        }

        #endregion Creators

        #region Members and Properties
        
        public float CloseEnoughDistance { get; set; }

        #endregion Members and properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            // If this goal is reactivated then there may be some existing
            // subgoals that must be removed.
            RemoveAllSubgoals();

            // It is possible for a agent's target to die while this goal is active
            // so we must test to make sure the agent always has an active target.
            if (ThinkingData.TargetingSystem.IsTargetPresent)
            {
                // If the agent is able to shoot the target (there is LOS between agent
                // and target), then select a tactic to follow while shooting.
                if (ThinkingData.TargetingSystem.IsTargetShootable)
                {
                    // If the agent has space to strafe then do so.
                    if (ThinkingData.CanStepLeft() || ThinkingData.CanStepRight())
                    {
                        PushSubgoal(
                            Strafe.CreateInstance(
                                ThinkingAgent,
                                ThinkingData.TargetingSystem.Target.Data));
                    }

                    // If not able to strafe, head directly at or away from the target's position
                    // to adjust range to the ideal for the weapon.
                    else
                    {
                        PushSubgoal(
                            AdjustRange.CreateInstance(
                                ThinkingAgent,
                                ThinkingData.TargetingSystem.Target.Data,
                                CloseEnoughDistance));
                    }
                }

                // If the target is not visible, go hunt it.
                else { PushSubgoal(HuntTarget.CreateInstance(ThinkingAgent, CloseEnoughDistance)); }
            }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            Status = ProcessSubgoals();

            // As long as the target is alive, keep replanning attacks.
            ReactivateIf(HasFailed ||
                         (Status == StatusTypes.Completed &&
                          ThinkingData.TargetingSystem.IsTargetPresent));

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate() { base.Terminate(); }

        #endregion Terminate

        #region Methods

        public override string ToString()
        {
            var target = ThinkingData.TargetingSystem.Target;
            var targetName = target != null
                ? target.ShortName ?? target.name
                : "Forgotten";
            return $"{base.ToString()} target={targetName}";
        }

        #endregion Methods
    }
}