using System.Collections.Generic;
using System.Text;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Evaluators;
using GameBrains.Entities;
using GameBrains.Entities.Types;
using GameBrains.Extensions.Lists;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class Brain : CompositeGoal
    {
        #region Creators

        public static Brain CreateInstance(ThinkingAgent thinkingAgent)
        {
            var think = CreateInstance<Brain>();
            Initialize(think, thinkingAgent);
            return think;
        }

        protected static void Initialize(Brain brain, ThinkingAgent thinkingAgent)
        {
            brain.ThinkingAgent = thinkingAgent;
            
            // These biases could be loaded in from a script on a per agent basis
            // but for now we'll just give them some random values.
            const float lowRangeOfBias = 0.5f;
            const float highRangeOfBias = 1.5f;

            float healthBias = Random.Range(lowRangeOfBias, highRangeOfBias);
            float exploreBias = Random.Range(lowRangeOfBias, highRangeOfBias);
            float attackBias = Random.Range(lowRangeOfBias, highRangeOfBias);
            float shotgunBias = Random.Range(lowRangeOfBias, highRangeOfBias);
            float rocketLauncherBias = Random.Range(lowRangeOfBias, highRangeOfBias);
            float railgunBias = Random.Range(lowRangeOfBias, highRangeOfBias);
            //ADDED Bomb laucher bias and flag bias
            float bombLauncherBias = Random.Range(lowRangeOfBias, highRangeOfBias);
            float flagBias = Random.Range(highRangeOfBias, highRangeOfBias);

            // Create the evaluator objects.
            brain.evaluators.Add(new EvaluatorGetHealth(thinkingAgent, healthBias));
            brain.evaluators.Add(new EvaluatorExplore(thinkingAgent, exploreBias));
            brain.evaluators.Add(new EvaluatorAttackTarget(thinkingAgent, attackBias));
            brain.evaluators.Add(new EvaluatorGetWeapon(thinkingAgent, shotgunBias, Parameters.ShotgunWeaponType));
            brain.evaluators.Add(new EvaluatorGetWeapon(thinkingAgent, railgunBias, Parameters.RailgunWeaponType));
            brain.evaluators.Add(new EvaluatorGetWeapon(thinkingAgent, rocketLauncherBias, Parameters.RocketLauncherWeaponType));
            //ADDED Bomb laucher evaluator and flag evaluator
            brain.evaluators.Add(new EvaluatorGetWeapon(thinkingAgent, bombLauncherBias,Parameters.BombLauncherWeaponType));
            brain.evaluators.Add(new EvaluatorGetFlag(thinkingAgent, flagBias));

        }

        #endregion Creators
        
        #region Members and Properties

        [SerializeField] List<Evaluator> evaluators = new List<Evaluator>();
        float bestDesirability;
        Evaluator mostDesirable;
        bool arbitrateTickTock;

        #endregion Members and Properties

        #region Activate

        public override void Activate()
        {
            base.Activate();
            
            if (!ThinkingAgent.IsPlayerControlled) { Arbitrate(); }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
           base.Process();
           
           ShowThots();

            StatusTypes subgoalStatus = ProcessSubgoals();

            if ((subgoalStatus == StatusTypes.Completed ||
                 subgoalStatus == StatusTypes.Failed) &&
                !ThinkingAgent.IsPlayerControlled)
            {
                while (!SubgoalStack.IsEmpty()) { CleanUpGoal(SubgoalStack.Pop()); }
                Status = StatusTypes.Inactive;
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate()
        {
            base.Terminate();

            while (!SubgoalStack.IsEmpty()) { CleanUpGoal(SubgoalStack.Pop()); }
        }

        #endregion Terminate

        #region Arbitrate

        void Arbitrate()
        {
            if (VerbosityDebug)
            {
                Log.Debug($"{ThinkingAgent.ShortName} {GetType().Name} Arbitrating.");
            }
            
            arbitrateTickTock = !arbitrateTickTock;

            bestDesirability = 0.0f;
            mostDesirable = null;

            // iterate through all the evaluators to find the highest scoring one
            foreach (Evaluator evaluator in evaluators)
            {
                float desirability = evaluator.CalculateDesirability();

                if (bestDesirability < desirability)
                {
                    bestDesirability = desirability;
                    mostDesirable = evaluator;
                }
            }

            if (mostDesirable == null)
            {
                throw new System.Exception("Brain.Arbitrate: no evaluator selected.");
            }

            if (VerbosityDebug)
            {
                Log.Debug(EvaluatorsToString());
            }

            mostDesirable.SetGoal();
        }
        
        #endregion Arbitrate

        #region Add Goals
        
        public void AddGoalExplore()
        {
            if (!NotPresent(Parameters.ExploreGoalType)) { return; }

            RemoveAllSubgoals();
            PushSubgoal(Explore.CreateInstance(ThinkingAgent));
        }

        public void AddGoalGetItemWithTypes(EntityTypes entityTypes)
        {
            //TODO: Does this check have to be item specific?
            if (!NotPresent(Parameters.GetEntityWithTypesGoalType)) { return; }

            RemoveAllSubgoals();
            var goal = GetEntityWithTypes.CreateInstance(ThinkingAgent, entityTypes);
            goal.verbosity = verbosity;
            PushSubgoal(goal);
        }
        
        public void AddGoalMoveToLocation(VectorXZ destination)
        {
            // TODO: Why is this different?
            PushSubgoal(MoveToLocation.CreateInstance(ThinkingAgent, destination));
        }
        
        public void AddGoalAttackTarget()
        {
            if (!NotPresent(Parameters.AttackTargetGoalType)) { return; }

            RemoveAllSubgoals();
            PushSubgoal(AttackTarget.CreateInstance(ThinkingAgent));
        }

        public void QueueGoalMoveToLocation(VectorXZ destination)
        {
            SubgoalStack.Enqueue(MoveToLocation.CreateInstance(ThinkingAgent, destination));
        }
        
        bool NotPresent(GoalType goalType)
        {
            return SubgoalStack.IsEmpty() || SubgoalStack.PeekInStack().GoalType != goalType;
        }
        
        #endregion Add Goals

        #region Strings

        public string EvaluatorsToString()
        {
            var message = new StringBuilder();
            foreach (Evaluator evaluator in evaluators) { message.Append($"[{evaluator}] "); }
            return message.ToString();
        }
        
        public override string ToString() { return $"{base.ToString()}"; }
        
        #endregion Strings
    }
}