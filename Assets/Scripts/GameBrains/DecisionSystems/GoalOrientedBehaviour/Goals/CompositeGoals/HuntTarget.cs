using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.AtomicGoals;
using GameBrains.Entities;
using GameBrains.Extensions.Vectors;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals.CompositeGoals
{
    public class HuntTarget : CompositeGoal
    {
        #region Creators

        public static HuntTarget CreateInstance( ThinkingAgent thinkingAgent, float closeEnoughDistance = 0.5f )
        {
            var huntTarget = CreateInstance<HuntTarget>();
            Initialize(huntTarget, thinkingAgent, closeEnoughDistance);
            return huntTarget;
        }

        protected static void Initialize( HuntTarget huntTarget, ThinkingAgent thinkingAgent, float closeEnoughDistance)
        {
            huntTarget.ThinkingAgent = thinkingAgent;
            huntTarget.CloseEnoughDistance = closeEnoughDistance;
        }

        #endregion Creators

        #region Members and Properties
        
        public float CloseEnoughDistance { get; set; }

        #endregion Members and properties

        #region Activate

        public override void Activate()
        {
            base.Activate();

            // If this goal is reactivated then there may be some existing subgoals
            // that must be removed.
            RemoveAllSubgoals();

            // It is possible for the target to die while this goal is active so we
            // must test to make sure the agent always has an active target.
            if (ThinkingData.TargetingSystem.IsTargetPresent)
            {
                // Grab a local copy of the last recorded position (LRP) of the target.
                VectorXZ? lrp = ThinkingData.TargetingSystem.LastRecordedPosition;

                // If the agent has reached the LRP and it still hasn't found the target
                // it starts to search by using the explore goal to move to random
                // map locations.
                if (lrp == null || ThinkingData.IsAtPosition((VectorXYZ)lrp))
                {
                    PushSubgoal(Explore.CreateInstance(ThinkingAgent, CloseEnoughDistance));
                }

                // Else move to the LRP.
                else
                {
                    PushSubgoal(MoveToLocation.CreateInstance(ThinkingAgent, lrp.Value, CloseEnoughDistance));
                }
            }

            // If there is no active target then this goal can be removed from the stack.
            else { Status = StatusTypes.Completed; }
        }

        #endregion Activate

        #region Process

        public override StatusTypes Process()
        {
            base.Process();

            Status = ProcessSubgoals();

            // If target is in view this goal is satisfied.
            if (ThinkingData.TargetingSystem.IsTargetWithinFieldOfView)
            {
                Status = StatusTypes.Completed;
                //TODO: Yes, brain surgery. This is an abuse.
                ThinkingData.Brain.InsertUnderTopSubgoal(
                    LinearAndAngularStopImmediately.CreateInstance(ThinkingAgent));
            }

            return Status;
        }

        #endregion Process

        #region Terminate

        public override void Terminate() { base.Terminate(); }

        #endregion Terminate

        #region Methods

        public override string ToString()
        {
            var target = ThinkingData.TargetingSystem.Target;
            var targetName = target != null
                ? target.ShortName ?? target.name
                : "Forgotten";
            return $"{base.ToString()} target={targetName}";
        }

        #endregion Methods
    }
}