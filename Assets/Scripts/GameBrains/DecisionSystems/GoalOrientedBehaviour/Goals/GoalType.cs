using GameBrains.Extensions.ScriptableObjects;
using UnityEngine;

namespace GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals
{
    [CreateAssetMenu(fileName = "GoalType", menuName = "AI/Enums/GoalType", order = 0)]
    public class GoalType : EnumScriptableObject { }
}