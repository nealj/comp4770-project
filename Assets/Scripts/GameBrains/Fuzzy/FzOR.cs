using System.Collections.Generic;

namespace GameBrains.Fuzzy
{
    public class FzOr : FuzzyTerm
    {
        public FzOr(FzOr fzOr)
        {
            Terms = new List<FuzzyTerm>();
            foreach (FuzzyTerm term in fzOr.Terms)
            {
                Terms.Add(term.Clone());
            }
        }

        public FzOr(FuzzyTerm op1, FuzzyTerm op2)
        {
            Terms = new List<FuzzyTerm> { op1.Clone(), op2.Clone() };
        }

        public FzOr(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3)
        {
            Terms = new List<FuzzyTerm> { op1.Clone(), op2.Clone(), op3.Clone() };
        }

        public FzOr(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3, FuzzyTerm op4)
        {
            Terms = new List<FuzzyTerm> { op1.Clone(), op2.Clone(), op3.Clone(), op4.Clone() };
        }

        public List<FuzzyTerm> Terms { get; private set; }

        public override FuzzyTerm Clone()
        {
            return new FzOr(this);
        }

        // The OR operator returns the maximum DOM of the sets it is operating on.
        public override float GetDom()
        {
            float largest = float.MinValue;

            foreach (FuzzyTerm term in Terms)
            {
                if (term.GetDom() > largest)
                {
                    largest = term.GetDom();
                }
            }

            return largest;
        }

        public override void ClearDom()
        {
            throw new System.Exception("FzOR.ClearDOM: invalid context.");
        }

        //  Method for updating the DOM of a consequent when a rule fires.
        public override void OrWithDom(float givenValue)
        {
            throw new System.Exception("FzOR.OrWithDom: invalid context.");
        }
    }
}