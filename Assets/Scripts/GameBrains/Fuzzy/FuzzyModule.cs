using System.Collections.Generic;

namespace GameBrains.Fuzzy
{
    // This class describes a fuzzy module: a collection of fuzzy variables and the rules that
    // operate on them.
    public class FuzzyModule
    {
        // When calculating the centroid of the fuzzy manifold this value is used to determine how
        // many cross-sections should be sampled.
        public const int CrossSectionSampleCount = 15;

        public FuzzyModule()
        {
            Rules = new List<FuzzyRule>();
            Variables = new Dictionary<string, FuzzyVariable>();
        }

        // You must pass one of these values to the DeFuzzify method. This module only
        // supports the MaxAv and Centroid methods.
        public enum DefuzzifyMethod
        {
            MaxAv,
            Centroid
        }

        // Gets a map of all the fuzzy variables this module uses.
        public Dictionary<string, FuzzyVariable> Variables { get; private set; }

        public List<FuzzyRule> Rules { get; private set; }

        // This method calls the Fuzzify method of the variable with the same name as the key.
        public void Fuzzify(string nameOfFlv, float val)
        {
            // first make sure the key exists
            if (!Variables.ContainsKey(nameOfFlv))
            {
                throw new System.Exception("FuzzyModule.Fuzzify>: key not found.");
            }                

            Variables[nameOfFlv].Fuzzify(val);
        }

        // Given a fuzzy variable and a defuzzification method this returns a crisp value.
        public float DeFuzzify(string nameOfFlv, DefuzzifyMethod method)
        {
            // first make sure the key exists
            if (!Variables.ContainsKey(nameOfFlv))
            {
				throw new System.Exception("FuzzyModule.DeFuzzify: key not found.");  
            }

            // clear the DOMs of all the consequents of all the rules
            SetConfidencesOfConsequentsToZero();

            // process the rules
            foreach (FuzzyRule rule in Rules)
            {
                rule.Calculate();
            }

            // now defuzzify the resultant conclusion using the specified method
            switch (method)
            {
                case DefuzzifyMethod.Centroid:
                    return Variables[nameOfFlv].DeFuzzifyCentroid(CrossSectionSampleCount);

                case DefuzzifyMethod.MaxAv:
                    return Variables[nameOfFlv].DeFuzzifyMaxAv();
            }

            return 0;
        }

        public void AddRule(FuzzyTerm antecedent, FuzzyTerm consequent)
        {
            Rules.Add(new FuzzyRule(antecedent, consequent));
        }

        public FuzzyVariable CreateFlv(string fuzzyVariableName)
        {
            Variables[fuzzyVariableName] = new FuzzyVariable();

            return Variables[fuzzyVariableName];
        }


        // Zeros the DOMs of the consequents of each rule.
        void SetConfidencesOfConsequentsToZero()
        {
            foreach (FuzzyRule rule in Rules)
            {
                rule.SetConfidenceOfConsequentToZero();
            }
        }
    }
}