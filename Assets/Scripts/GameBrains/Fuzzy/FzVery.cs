namespace GameBrains.Fuzzy
{
    public class FzVery : FuzzyTerm
    {
        public FzVery(FzSet fzSet)
        {
            Set = fzSet.Set;
        }

        public FzVery(FzVery fzVery)
        {
            Set = fzVery.Set;
        }

        public FuzzySet Set { get; private set; }

        public override FuzzyTerm Clone()
        {
            return new FzVery(this);
        }

        public override float GetDom()
        {
            return Set.Dom * Set.Dom;
        }

        public override void ClearDom()
        {
            Set.ClearDom();
        }

        // Method for updating the DOM of a consequent when a rule fires.
        public override void OrWithDom(float givenValue)
        {
            Set.OrWithDom(givenValue * givenValue);
        }
    }
}