using System.Collections.Generic;

namespace GameBrains.Fuzzy
{
    public class FzAnd : FuzzyTerm
    {
        public FzAnd(FzAnd fzAnd)
        {
            Terms = new List<FuzzyTerm>();
            foreach (FuzzyTerm term in fzAnd.Terms)
            {
                Terms.Add(term.Clone());
            }
        }

        public FzAnd(FuzzyTerm op1, FuzzyTerm op2)
        {
            Terms = new List<FuzzyTerm> { op1.Clone(), op2.Clone() };
        }

        public FzAnd(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3)
        {
            Terms = new List<FuzzyTerm> { op1.Clone(), op2.Clone(), op3.Clone() };
        }

        public FzAnd(FuzzyTerm op1, FuzzyTerm op2, FuzzyTerm op3, FuzzyTerm op4)
        {
            Terms = new List<FuzzyTerm> { op1.Clone(), op2.Clone(), op3.Clone(), op4.Clone() };
        }

        public List<FuzzyTerm> Terms { get; private set; }

        public override FuzzyTerm Clone()
        {
            return new FzAnd(this);
        }

        public override float GetDom()
        {
            float smallest = float.MaxValue;

            foreach (FuzzyTerm term in Terms)
            {
                if (term.GetDom() < smallest)
                {
                    smallest = term.GetDom();
                }
            }

            return smallest;
        }


        // Method for updating the DOM of a consequent when a rule fires.
        public override void OrWithDom(float givenValue)
        {
            foreach (FuzzyTerm term in Terms)
            {
                term.OrWithDom(givenValue);
            }
        }

        public override void ClearDom()
        {
            foreach (FuzzyTerm term in Terms)
            {
                term.ClearDom();
            }
        }
    }
}