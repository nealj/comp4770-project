namespace GameBrains.Fuzzy
{
    public class FuzzyRule
    {
        public FuzzyRule(FuzzyTerm antecedent, FuzzyTerm consequent)
        {
            Antecedent = antecedent.Clone();
            Consequent = consequent.Clone();
        }

        // Gets the antecedent (usually a composite of several fuzzy sets and operators).
        public FuzzyTerm Antecedent { get; private set; }

        // Gets the consequent (usually a single fuzzy set, but can be several ANDed together).
        public FuzzyTerm Consequent { get; private set; }

        // Clear the degree of membership of the consequent.
        public void SetConfidenceOfConsequentToZero()
        {
            Consequent.ClearDom();
        }

        // This method updates the DOM (the confidence) of the consequent term with the DOM of the
        // antecedent term.
        public void Calculate()
        {
            Consequent.OrWithDom(Antecedent.GetDom());
        }
    }
}