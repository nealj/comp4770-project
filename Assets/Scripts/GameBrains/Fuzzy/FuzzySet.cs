namespace GameBrains.Fuzzy
{
    // Class to define an interface for a fuzzy set.
    public abstract class FuzzySet
    {
        float dom;

        protected FuzzySet(float representativeValue)
        {
            dom = 0.0f;
            RepresentativeValue = representativeValue;
        }

        // Gets the maximum of the set's membership function. For instance, if the set is
        // triangular then this will be the peak point of the  triangular. If the set has a plateau
        // then this value will be the mid-point of the plateau. This value is set in the
        // constructor to avoid run-time calculation of mid-point values.
        public float RepresentativeValue { get; private set; }

        public float Dom
        {
            get => dom;

            set
            {
                if (value < 0 || value > 1)
                {
                	throw new System.Exception("FuzzySet.SetDOM: invalid value.");
                }

                dom = value;
            }
        }

        // Returns the degree of membership in this set of the given value. This does not set
        // dom to the degree of membership of the value passed as the parameter.
        // This is because the centroid defuzzification method also uses this method to determine
        // the DOMs of the values it uses as its sample points.
        public abstract float CalculateDom(float givenValue);

        // If this fuzzy set is part of a consequent FLV, and it is fired by a rule then this
        // method sets the DOM (in this context, the DOM represents a confidence level) to the
        // maximum of the parameter value or the set's existing dom value.
        public void OrWithDom(float givenValue)
        {
            if (givenValue > dom)
            {
                dom = givenValue;
            }
        }

        public void ClearDom()
        {
            dom = 0.0f;
        }
    }
}