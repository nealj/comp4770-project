using UnityEngine;

namespace GameBrains.Fuzzy
{
    public class FuzzySetTriangle : FuzzySet
    {
        // Peak is point with max value.
        // LeftOffset is distance to point with value 0 on the left.
        // RightOffset is distance to point with value 0 on the right.
        // E.g.,
        // Dumb = FuzzySetTriangle(80, 80, 20)
        // Average = FuzzySetTriangle(100, 20, 20)
        // Clever = FuzzySetTriangle(120, 20, 20)
        // DOM_Dumb(90) = 0.5
        // DOM_Average(90) = 0.5
        // DOM_Average(115) = 0.25
        // DOM_Clever(115) = 0.75
        public FuzzySetTriangle(float peak, float left, float right)
            : base(peak)
        {
            PeakPoint = peak;
            LeftOffset = left;
            RightOffset = right;
        }

        public float PeakPoint { get; set; }

        public float LeftOffset { get; set; }

        public float RightOffset { get; set; }

        // Returns the degree of membership in this set of the given value. This does not set
        // FuzzySet.dom to the degree of membership of the value passed as the
        // parameter. This is because the centroid defuzzification method also uses this method to
        // determine the DOMs of the values it uses as its sample points.
        public override float CalculateDom(float givenValue)
        {
            // test for the case where the triangle's left or right offsets are
            // zero (to prevent divide by zero errors below)
            if ((Mathf.Approximately(RightOffset, 0.0f) && Mathf.Approximately(PeakPoint, givenValue)) ||
               (Mathf.Approximately(LeftOffset, 0.0f) && Mathf.Approximately(PeakPoint, givenValue)))
            {
                return 1.0f;
            }

            // find DOM if left of center
            if (givenValue <= PeakPoint && givenValue >= (PeakPoint - LeftOffset))
            {
                float grad = 1.0f / LeftOffset;
                return grad * (givenValue - (PeakPoint - LeftOffset));
            }

            // find DOM if right of center
            if (givenValue > PeakPoint && givenValue < (PeakPoint + RightOffset))
            {
                float grad = 1.0f / -RightOffset;
                return grad * (givenValue - PeakPoint) + 1.0f;
            }

            // out of range of this FLV, return zero
            return 0.0f;
        }
    }
}