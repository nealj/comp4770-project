namespace GameBrains.Fuzzy
{
    public class FuzzySetSingleton : FuzzySet
    {
        public FuzzySetSingleton(float mid, float left, float right)
            : base(mid)
        {
            MidPoint = mid;
            LeftOffset = left;
            RightOffset = right;
        }

        public float MidPoint { get; private set; }
        public float LeftOffset { get; private set; }
        public float RightOffset { get; private set; }

        // Returns the degree of membership in this set of the given value. This does not set
        // FuzzySet.dom to the degree of membership of the value passed as the
        // parameter. This is because the centroid defuzzification method also uses this method to
        // determine the DOMs of the values it uses as its sample points.
        public override float CalculateDom(float givenValue)
        {
            if (givenValue >= (MidPoint - LeftOffset) && givenValue <= (MidPoint + RightOffset))
            {
                return 1.0f;
            }

            // out of range of this FLV, return zero
            return 0.0f;
        }
    }
}