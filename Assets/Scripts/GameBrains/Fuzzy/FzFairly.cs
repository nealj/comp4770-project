using UnityEngine;

namespace GameBrains.Fuzzy
{
    public class FzFairly : FuzzyTerm
    {
        public FzFairly(FzSet fuzzyTerm)
        {
            Set = fuzzyTerm.Set;
        }

        public FzFairly(FzFairly fzFairly)
        {
            Set = fzFairly.Set;
        }

        public FuzzySet Set { get; private set; }

        public override float GetDom()
        {
            return Mathf.Sqrt(Set.Dom);
        }

        public override FuzzyTerm Clone()
        {
            return new FzFairly(this);
        }

        public override void ClearDom()
        {
            Set.ClearDom();
        }

        // Method for updating the DOM of a consequent when a rule fires.
        public override void OrWithDom(float givenValue)
        {
            Set.OrWithDom(Mathf.Sqrt(givenValue));
        }
    }
}