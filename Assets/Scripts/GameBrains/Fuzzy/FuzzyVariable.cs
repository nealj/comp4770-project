using System.Collections.Generic;
using UnityEngine;

namespace GameBrains.Fuzzy
{
    public class FuzzyVariable
    {
        public FuzzyVariable()
        {
            MemberSets = new Dictionary<string, FuzzySet>();
            MinRange = 0.0f;
            MaxRange = 0.0f;
        }

        public Dictionary<string, FuzzySet> MemberSets { get; private set; }

        public float MinRange { get; set; }

        public float MaxRange { get; set; }

        // Takes a crisp value and calculates its degree of membership for each set in the variable.
        public void Fuzzify(float crispValue)
        {
            // make sure the value is within the bounds of this variable
            if (crispValue < MinRange || crispValue > MaxRange)
            {
                System.Diagnostics.Debug.WriteLine("FuzzyVariable.Fuzzify>: value out of range.");
                return;
            }

            // for each set in the flv calculate the DOM for the given value
            foreach (KeyValuePair<string, FuzzySet> kvp in MemberSets)
            {
                kvp.Value.Dom = kvp.Value.CalculateDom(crispValue);
            }
        }

        // Defuzzifies the value by averaging the maxima of the sets that have fired.
        // Returns: Sum (maxima * DOM) / sum (DOMs).
        public float DeFuzzifyMaxAv()
        {
            float bottom = 0.0f;
            float top = 0.0f;

            foreach (KeyValuePair<string, FuzzySet> kvp in MemberSets)
            {
                bottom += kvp.Value.Dom;

                top += kvp.Value.RepresentativeValue * kvp.Value.Dom;
            }

            // make sure bottom is not equal to zero
            if (Mathf.Approximately(0, bottom))
            {
                return 0.0f;
            }

            return top / bottom;
        }

        // Defuzzify the variable using the centroid method.
        public float DeFuzzifyCentroid(int sampleCount)
        {
            // calculate the step size
            float stepSize = (MaxRange - MinRange) / sampleCount;

            float totalArea = 0.0f;
            float sumOfMoments = 0.0f;

            // step through the range of this variable in increments equal to
            // stepSize adding up the contribution (lower of CalculateDOM or
            // the actual DOM of this variable's fuzzified value) for each
            // subset. This gives an approximation of the total area of the
            // fuzzy manifold. (This is similar to how the area under a curve
            // is calculated using calculus... the heights of lots of 'slices'
            // are summed to give the total area.)
            //
            // In addition the moment of each slice is calculated and summed.
            // Dividing the total area by the sum of the moments gives the
            // centroid. (Just like calculating the center of mass of an object)
            for (int sample = 1; sample <= sampleCount; ++sample)
            {
                // for each set get the contribution to the area. This is the
                // lower of the value returned from CalculateDOM or the actual
                // DOM of the fuzzified value itself   
                foreach (KeyValuePair<string, FuzzySet> kvp in MemberSets)
                {
                    float contribution =
                        Mathf.Min(
                            kvp.Value.CalculateDom(MinRange + sample * stepSize),
                            kvp.Value.Dom);

                    totalArea += contribution;

                    sumOfMoments += (MinRange + sample * stepSize) * contribution;
                }
            }

            // make sure total area is not equal to zero
            if (Mathf.Approximately(0, totalArea))
            {
                return 0.0f;
            }

            return sumOfMoments / totalArea;
        }

        public FzSet AddTriangularSet(
            string name,
            float minimumBound,
            float peak,
            float maximumBound)
        {
            MemberSets[name] = 
                new FuzzySetTriangle(peak, peak - minimumBound, maximumBound - peak);

            // adjust range if necessary
            AdjustRangeToFit(minimumBound, maximumBound);

            return new FzSet(MemberSets[name]);
        }

        public FzSet AddLeftShoulderSet(
            string name,
            float minimumBound,
            float peak,
            float maximumBound)
        {
            MemberSets[name] = 
                new FuzzySetLeftShoulder(peak, peak - minimumBound, maximumBound - peak);

            // adjust range if necessary
            AdjustRangeToFit(minimumBound, maximumBound);

            return new FzSet(MemberSets[name]);
        }

        public FzSet AddRightShoulderSet(
            string name,
            float minimumBound,
            float peak,
            float maximumBound)
        {
            MemberSets[name] =
                new FuzzySetRightShoulder(
                    peak,
                    peak - minimumBound,
                    maximumBound - peak);

            // adjust range if necessary
            AdjustRangeToFit(minimumBound, maximumBound);

            return new FzSet(MemberSets[name]);
        }

        public FzSet AddSingletonSet(
            string name,
            float minimumBound,
            float peak,
            float maximumBound)
        {
            MemberSets[name] =
                new FuzzySetSingleton(
                    peak,
                    peak - minimumBound,
                    maximumBound - peak);

            AdjustRangeToFit(minimumBound, maximumBound);

            return new FzSet(MemberSets[name]);
        }

        // This method is called with the upper and lower bound of a set each time a new set is
        // added to adjust the upper and lower range values accordingly.
        void AdjustRangeToFit(float minimumBound, float maximumBound)
        {
            if (minimumBound < MinRange)
            {
                MinRange = minimumBound;
            }

            if (maximumBound > MaxRange)
            {
                MaxRange = maximumBound;
            }
        }
    }
}