using UnityEngine;

namespace GameBrains.Fuzzy
{
    public class FuzzySetRightShoulder : FuzzySet
    {
        public FuzzySetRightShoulder(float peak, float left, float right)
            : base(((peak + right) + peak) / 2)
        {
            PeakPoint = peak;
            LeftOffset = left;
            RightOffset = right;
        }

        public float PeakPoint { get; private set; }
        public float LeftOffset { get; private set; }
        public float RightOffset { get; private set; }

        // Returns the degree of membership in this set of the given value. This does not set
        // FuzzySet.dom to the degree of membership of the value passed as the
        // parameter. This is because the centroid defuzzification method also uses this method to
        // determine the DOMs of the values it uses as its sample points.
        public override float CalculateDom(float givenValue)
        {
            // test for the case where the left or right offsets are zero
            // (to prevent divide by zero errors below)
            if (Mathf.Approximately(RightOffset, 0.0f) && Mathf.Approximately(PeakPoint, givenValue) ||
               Mathf.Approximately(LeftOffset, 0.0f) && Mathf.Approximately(PeakPoint, givenValue))
            {
                return 1.0f;
            }

            // find DOM if left of center
            if (givenValue <= PeakPoint && givenValue > (PeakPoint - LeftOffset))
            {
                float grad = 1.0f / LeftOffset;
                return grad * (givenValue - (PeakPoint - LeftOffset));
            }

            // find DOM if right of center and less than center + right offset
            if (givenValue > PeakPoint && givenValue <= PeakPoint + RightOffset)
            {
                return 1.0f;
            }

            // out of range of this FLV, return zero
            return 0f;
        }
    }
}