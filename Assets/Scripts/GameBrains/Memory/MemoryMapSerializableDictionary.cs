using GameBrains.Entities;
using GameBrains.Extensions.DictionaryExtensions;

namespace GameBrains.Memory
{
    [System.Serializable]
    public class MemoryMapSerializableDictionary : SerializableDictionary<ThinkingAgent,SensoryMemoryRecord> { }
}