using System.Collections.Generic;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace GameBrains.Memory
{
    // Sensory memory keeps track of opponents detected by sound or sight.
    /// TODO: how about add smell or tracking if crossing recent path.
    public class SensoryMemory : ExtendedScriptableObject
    {
        #region Creators

        public static SensoryMemory CreateInstance(ThinkingAgent thinkingAgent, float memorySpan)
        {
            var sensoryMemory = CreateInstance<SensoryMemory>();
            Initialize(sensoryMemory, thinkingAgent, memorySpan);
            return sensoryMemory;
        }

        protected static void Initialize(
            SensoryMemory sensoryMemory,
            ThinkingAgent thinkingAgent,
            float memorySpan)
        {
            sensoryMemory.ThinkingAgent = thinkingAgent;
            sensoryMemory.MemorySpan = memorySpan;
            sensoryMemory.MemoryMap = new MemoryMapSerializableDictionary();
        }

        #endregion Creators

        public ThinkingAgent ThinkingAgent { get; private set; }

        public ThinkingData ThinkingData => ThinkingAgent.Data;

        // Gets the container is used to simulate memory of sensory events. A record is created
        // for each opponent in the environment. Each record is updated whenever the opponent is
        // encountered (i.e., whenever it is seen or heard).
        public MemoryMapSerializableDictionary MemoryMap { get; private set; }

        // Gets the agent's memory span. When an agent requests a list of all recently sensed
        // opponents this value is used to determine if the agent is remembers an opponent or not.
        public float MemorySpan { get; private set; }

        public void RemoveAgentFromMemory(ThinkingAgent movingAgent)
        {
            MemoryMap.Remove(movingAgent);
        }

        // Update the record for an individual opponent.
        // Note, there is no need to test if the opponent is within the FOV because that test will
        // be done when the UpdateVision method is called.
        public void UpdateWithSoundSource(ThinkingAgent noiseMaker)
        {
            // make sure the agent being examined is not this agent
            if (ThinkingAgent == noiseMaker || noiseMaker.Data.SameTeam(ThinkingAgent))
            {
                return;
            }

            // if the agent is already part of the memory then update its data,
            // else create a new memory record and add it to the memory
            MakeNewRecordIfNotAlreadyPresent(noiseMaker);

            SensoryMemoryRecord info = MemoryMap[noiseMaker];

            // test if there is LOS between agents
            if (ThinkingAgent.Data.HasLineOfSight(noiseMaker.Data.Position))
            {
                info.IsShootable = true;

                // record the position of the agent
                info.LastSensedPosition = noiseMaker.Data.Location;
            }
            else
            {
                info.IsShootable = false;
            }

            // record the time it was sensed
            info.TimeLastSensed = Time.time;
        }

        /// <summary>
        /// This method iterates through all the agents in the game world to test if they are in the
        /// field of view. Each agent's memory record is updated accordingly.
        /// </summary>
        public void UpdateVision()
        {
            // for each agent in the world test to see if it is visible to the
            // agent of this class
            var thinkingAgents = EntityManager.FindAll<ThinkingAgent>();
            foreach (ThinkingAgent thinkingAgent in thinkingAgents)
            {
                // make sure the agent being examined is not this agents
                if (ThinkingAgent == thinkingAgent || thinkingAgent.Data.SameTeam(ThinkingAgent))
                {
                    continue;
                }

                // make sure it is part of the memory map
                MakeNewRecordIfNotAlreadyPresent(thinkingAgent);

                // get a reference to this agent's data
                SensoryMemoryRecord info = MemoryMap[thinkingAgent];

                if (ThinkingAgent.Data.HasLineOfSight(thinkingAgent.Data.Position))
                {
                    info.IsShootable = true;

                    if (ThinkingData.IsTargetInFieldOfView(thinkingAgent.Data.Position))
                    {
                        info.TimeLastSensed = Time.time;
                        info.LastSensedPosition = thinkingAgent.Data.Location;
                        info.TimeLastVisible = Time.time;

                        if (info.IsWithinFieldOfView == false)
                        {
                            info.IsWithinFieldOfView = true;
                            info.TimeBecameVisible = info.TimeLastSensed;
                        }
                    }
                    else
                    {
                        info.IsWithinFieldOfView = false;
                    }
                }
                else
                {
                    info.IsShootable = false;
                    info.IsWithinFieldOfView = false;
                }
            }
        }

        /// <summary>
        /// Gets the list of recently sensed opponents.
        /// </summary>
        /// <returns>A list of the agents that have been sensed recently.</returns>
        public List<ThinkingAgent> GetListOfRecentlySensedOpponents()
        {
            // this will store all the opponents the agent can remember
            var opponents = new List<ThinkingAgent>();

            float currentTime = Time.time;
            foreach (KeyValuePair<ThinkingAgent, SensoryMemoryRecord> kvp in MemoryMap)
            {
                // if this agent has been updated in the memory recently, add to list
                if ((currentTime - kvp.Value.TimeLastSensed) <= MemorySpan)
                {
                    opponents.Add(kvp.Key);
                }
            }

            return opponents;
        }

        // True if opponent can be shot (i.e. its not obscured by walls).
        public bool IsOpponentShootable(ThinkingAgent opponent)
        {
            if (opponent != null && MemoryMap.ContainsKey(opponent))
            {
                return MemoryMap[opponent].IsShootable;
            }

            return false;
        }

        public bool IsOpponentWithinFieldOfView(ThinkingAgent opponent)
        {
            if (opponent != null && MemoryMap.ContainsKey(opponent))
            {
                return MemoryMap[opponent].IsWithinFieldOfView;
            }

            return false;
        }

        public VectorXZ? GetLastRecordedPosition(ThinkingAgent opponent)
        {
            if (opponent != null && MemoryMap.ContainsKey(opponent))
            {
                return MemoryMap[opponent].LastSensedPosition;
            }

            return null;
        }

        // Gets the amount of time opponent has been visible.
        public float GetTimeVisible(ThinkingAgent opponent)
        {
            if (opponent != null && MemoryMap.ContainsKey(opponent) &&
                MemoryMap[opponent].IsWithinFieldOfView)
            {
                return Time.time - MemoryMap[opponent].TimeBecameVisible;
            }

            return 0;
        }

        // Gets the amount of time the given opponent has remained out of view (or a high value if
        // opponent has never been seen or is not present).
        public float GetTimeOutOfView(ThinkingAgent opponent)
        {
            if (opponent != null && MemoryMap.ContainsKey(opponent))
            {
                return Time.time - MemoryMap[opponent].TimeLastVisible;
            }

            return float.MaxValue;
        }

        // Get the time since opponent was last sensed.
        public float GetTimeSinceLastSensed(ThinkingAgent opponent)
        {
            if (opponent != null && MemoryMap.ContainsKey(opponent) &&
                MemoryMap[opponent].IsWithinFieldOfView)
            {
                return Time.time - MemoryMap[opponent].TimeLastSensed;
            }

            return 0;
        }

        // Check to see if there is an existing record for the opponent. If not a new record is
        // made and added to the memory map.
        void MakeNewRecordIfNotAlreadyPresent(ThinkingAgent opponent)
        {
            // check to see if this Opponent already exists in the memory. If it doesn't,
            // create a new record
            if (!MemoryMap.ContainsKey(opponent))
            {
                MemoryMap[opponent] = SensoryMemoryRecord.CreateInstance(ThinkingAgent,MemorySpan);
            }
        }
    }
}