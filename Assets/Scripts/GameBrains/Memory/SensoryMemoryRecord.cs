using GameBrains.Entities;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;

namespace GameBrains.Memory
{
    // Class to implement short term memory of sensory info.
    public class SensoryMemoryRecord : ExtendedScriptableObject
    {
        #region Creators

        public static SensoryMemoryRecord CreateInstance(ThinkingAgent thinkingAgent, float memorySpan)
        {
            var sensoryMemoryRecord = CreateInstance<SensoryMemoryRecord>();
            Initialize(sensoryMemoryRecord, thinkingAgent, memorySpan);
            return sensoryMemoryRecord;
        }

        protected static void Initialize(
            SensoryMemoryRecord sensoryMemoryRecord,
            ThinkingAgent thinkingAgent,
            float memorySpan)
        {
            sensoryMemoryRecord.ThinkingAgent = thinkingAgent;
            sensoryMemoryRecord.MemorySpan = memorySpan;
            sensoryMemoryRecord.TimeLastSensed = -999;
            sensoryMemoryRecord.TimeBecameVisible = -999;
            sensoryMemoryRecord.TimeLastVisible = 0;
            sensoryMemoryRecord.IsWithinFieldOfView = false;
            sensoryMemoryRecord.IsShootable = false;
        }

        #endregion Creators
        
        public ThinkingAgent ThinkingAgent { get; set; } // TODO: Is this needed here?
        
        public float MemorySpan { get; set; } // TODO: Is this needed here?

        // Gets or sets the time the opponent was last sensed (seen or heard). This is used to
        // determine if an agent can 'remember' this record or not.  (if Time.TimeNow -
        // TimeLastSensed is greater than the agent's memory span, the data in this record is made
        // unavailable to clients).
        public float TimeLastSensed { get; set; }

        // Gets or sets the current time when an opponent first becomes visible. It can be useful
        // to know how long an opponent has been visible. It's then a simple matter to calculate
        // how long the opponent has been in view: Time.TimeNow - TimeBecameVisible.
        public float TimeBecameVisible { get; set; }

        // Gets or sets the last time an opponent was seen.
        public float TimeLastVisible { get; set; }

        // Gets or sets a vector marking the position where the opponent was last sensed. This can
        // be used to help hunt down an opponent if it goes out of view.
        public VectorXZ LastSensedPosition { get; set; }

        public bool IsWithinFieldOfView { get; set; }

        // Gets or sets a value indicating whether there is no obstruction between the opponent and the
        // owner, permitting a shot.
        public bool IsShootable { get; set; }
    }
}