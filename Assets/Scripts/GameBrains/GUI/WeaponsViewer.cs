using GameBrains.Armory;
using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using UnityEngine;

namespace GameBrains.GUI
{
    [AddComponentMenu("Scripts/GameBrains/GUI/Weapons Viewer")]
    public class WeaponsViewer : FpsViewer
    {
        public override void Start()
        {
            base.Start(); // Initializes the window id.
            windowTitle = "Weapon & Flag Viewer";
        }

        // This creates the GUI inside the window.
        // It requires the id of the window it's currently making GUI for.
        protected override void WindowFunction(int windowID)
        {
            // Purposely not calling base.WindowFunction here.
        
            // Draw any Controls inside the window here.
            
            Color savedColor = UnityEngine.GUI.color;
            UnityEngine.GUI.color = defaultContentColor;

            GUILayout.BeginHorizontal();
        
             #region Column One
            
             GUILayout.BeginVertical(GUILayout.MinWidth(minimumColumnWidth));
            
             if (showFps) { GUILayout.Label($"FPS: {fps:f1}"); }

             var thinkingAgents = FindObjectsOfType<ThinkingAgent>();
             foreach (var thinkingAgent in thinkingAgents)
             {
                 var weaponsSystem = thinkingAgent.Data.WeaponSystem;
                 var currentWeaponString = 
                     weaponsSystem.CurrentWeapon == null 
                         ? "NONE" 
                         : weaponsSystem.CurrentWeapon.WeaponType.shortDescription;
                var entityColor = ColorUtility.ToHtmlStringRGBA(thinkingAgent.Color);
                //ADDED player flags captured and the teams total of captured flags, to display the weapon, thier flag count and team flag count
                GUILayout.Label($"{thinkingAgent.ShortName}: {currentWeaponString}\t<color=#{entityColor}>Player:{thinkingAgent.TeamFlagScore}</color>\t<color=#{entityColor}>Teams:{thinkingAgent.GetFlagCount()}</color>");
             }
            
             GUILayout.EndVertical();
            
             #endregion Column One
            
             #region Column Two
            
             // GUILayout.BeginVertical(GUILayout.MinWidth(minimumColumnWidth));
             // // Add second column here
             //
             // GUILayout.EndVertical();
            
             #endregion Column Two
        
            GUILayout.EndHorizontal();
            
            UnityEngine.GUI.color = savedColor;
        
            // Make the windows be draggable.
            UnityEngine.GUI.DragWindow();
        }
    }
}