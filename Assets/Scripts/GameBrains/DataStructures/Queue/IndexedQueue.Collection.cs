﻿using System;
using System.Collections.Generic;

namespace GameBrains.DataStructures.Queue
{
	partial class IndexedQueue<T> : ICollection<T>
	{
		public int Count => count;
		bool ICollection<T>.IsReadOnly => false;

		void ICollection<T>.Add(T item)  =>Enqueue(item);

		public void Clear()
		{
			head = 0;
			count = 0;
			unchecked { ++version; }
		}

		public bool Contains(T item)
		{
			// TODO: Reimplement this using IndexOf()
			for(var i = 0; i < count; ++i)
			{
				if (Equals(array[(head + i) % array.Length], item)) { return true; }
			}
			return false;
		}

		public void CopyTo(T[] arrayDestination, int arrayIndex)
		{
			if (null == arrayDestination) { throw new ArgumentNullException(nameof(arrayDestination)); }

			if (0 > arrayIndex || arrayDestination.Length < arrayIndex + count)
			{
				throw new ArgumentOutOfRangeException(nameof(arrayIndex));
			}
			
			// TODO: Reimplement this using Array.Copy
			for (var i = 0; i < count; ++i)
			{
				arrayDestination[arrayIndex + i] = array[(head + i) % array.Length];
			}
		}

		bool ICollection<T>.Remove(T item)
		{
			if (Equals(array[head],item))
			{
				Dequeue();
				return true;
			}

			// TODO: Reimplement using RemoveAt()/IndexOf()
			for(var i = 0; i < count; ++i)
			{
				var idx = (head + i) % array.Length;
				if (Equals(array[idx],item))
				{
					if (head + count < array.Length)
					{
						Array.Copy(
							array, 
							idx + 1, 
							array, 
							idx, 
							count - idx - 1);
					}
					else if (idx == array.Length - 1)
					{
						array[idx] = array[0];
						if( count + head != array.Length)
						{
							Array.Copy(
								array, 
								1, 
								array, 
								0, 
								(count + head) % array.Length - 1);
						}
					}
					else if (idx < head)
					{
						Array.Copy(
							array, 
							idx + 1, 
							array, 
							idx, 
							(count + head) % array.Length - 1);
					}
					--count;
					unchecked { ++version; }
					return true;
				}
			}
			
			return false;
		}
	}
}