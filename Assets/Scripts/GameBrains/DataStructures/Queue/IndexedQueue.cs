﻿using System;
using System.Collections.Generic;

namespace GameBrains.DataStructures.Queue
{
	// Represents a queue with indexed access to the items
	public partial class IndexedQueue<T>
	{
		const int DefaultCapacity = 16;
		const float GrowthFactor = 2f;
		const float TrimThreshold = .9f;
		T[] array;
		int head;
		int count;

		public IndexedQueue(int capacity)
		{
			if (0 >= capacity)
			{
				throw new ArgumentOutOfRangeException(
					$"The capacity must be greater than zero.", nameof(capacity));
			}
			
			array = new T[capacity];
			head = 0;
			count = 0;
			version = 0;
		}

		public T this[int index]
		{
			get
			{
				if (0 > index || index >= count) { throw new IndexOutOfRangeException(); }
				return array[(index + head) % array.Length];
			}
			set
			{
				if (0 > index || index >= count) { throw new IndexOutOfRangeException(); }
				array[(index + head) % array.Length] = value;
				++version;
			}
		}

		public IndexedQueue() : this(DefaultCapacity) { }

		public IndexedQueue(IEnumerable<T> collection)
		{
			if (null == collection) { throw new ArgumentNullException(nameof(collection)); }
			foreach (var item in collection) { Enqueue(item); }
		}

		public T Peek()
		{
			if (0 == count) { throw new InvalidOperationException("The queue is empty."); }
			return array[head];
		}

		public T[] ToArray()
		{
			var result = new T[count];
			CopyTo(result, 0);
			return result;
		}

		public void Enqueue(T item)
		{
			if (count == array.Length)
			{
				var arr = new T[(int) (array.Length * GrowthFactor)];
				if (head + count <= array.Length)
				{
					Array.Copy(array, arr, count);
					head = 0;
					arr[count] = item;
					++count;
					unchecked { ++version; }
					array = arr;
				}
				else // if(_head+_count<=arr.Length)
				{
					Array.Copy(
						array,
						head,
						arr,
						0,
						array.Length - head);
					Array.Copy(
						array,
						0,
						arr,
						array.Length - head,
						head);
					head = 0;
					arr[count] = item;
					++count;
					unchecked { ++version; }
					array = arr;
				}
			}
			else
			{
				array[(head + count) % array.Length] = item;
				++count;
				unchecked { ++version; }
			}
		}

		public T Dequeue()
		{
			if (0 == count) { throw new InvalidOperationException("The queue is empty"); }
			var result = array[head];
			++head;
			head = head % array.Length;
			--count;
			unchecked { ++version; }

			return result;
		}

		public void TrimExcess()
		{
			if (0 == count) { array = new T[DefaultCapacity]; }

			if (array.Length * TrimThreshold >= count)
			{
				var arr = new T[count];
				CopyTo(arr, 0);
				head = 0;
				array = arr;
				unchecked { ++version; }
			}
		}

		public bool TryPeek(out T item)
		{
			if (0 != count)
			{
				item = array[head];
				return true;
			}

			item = default;
			return false;
		}

		public bool TryDequeue(out T item)
		{
			if (0 < count)
			{
				item = array[head];
				++head;
				head = head % array.Length;
				--count;
				unchecked { ++version; }

				return true;
			}

			item = default;
			return false;
		}
	}
}