﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace GameBrains.DataStructures.Queue
{
	partial class IndexedQueue<T>
	{
		int version;
		public IEnumerator<T> GetEnumerator() => new Enumerator(this);

		// legacy enumerable support (required)
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		struct Enumerator : IEnumerator<T>
		{
			const int AfterEnd = -1;
			const int BeforeStart = -2;
			const int Disposed = -3;

			readonly IndexedQueue<T> outer;
			int index;
			readonly int version;

			public Enumerator(IndexedQueue<T> outer)
			{
				this.outer = outer;
				version = outer.version;
				index = BeforeStart;
			}

			public T Current
			{
				get
				{
					switch (index)
					{
						case Disposed:
							throw new ObjectDisposedException(GetType().Name);
						case BeforeStart:
							throw new InvalidOperationException(
								"The cursor is before the start of the enumeration.");
						case AfterEnd:
							throw new InvalidOperationException(
								"The cursor is after the end of the enumeration.");
					}

					CheckVersion();
					return outer.array[index % outer.array.Length];
				}
			}

			// legacy enumerator support (required)
			object IEnumerator.Current => Current;

			public bool MoveNext()
			{
				switch (index)
				{
					case Disposed:
						throw new ObjectDisposedException(GetType().Name);
					case AfterEnd:
						return false;
					case BeforeStart:
						CheckVersion();
						if (0 == outer.count)
						{
							index = AfterEnd;
							return false;
						}

						index = outer.head;
						return true;
				}

				CheckVersion();
				if (++index >= outer.count + outer.head)
				{
					index = AfterEnd;
					return false;
				}

				return true;
			}

			public void Reset()
			{
				if (-3 == index) { throw new ObjectDisposedException(GetType().Name); }
				CheckVersion();
				index = BeforeStart;
			}

			public void Dispose() { index = Disposed; }

			void CheckVersion()
			{
				if (version != outer.version)
				{
					throw new InvalidOperationException(
						"The enumeration has changed and may not execute.");
					
				}
			}
		}
	}
}