using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Entities.Triggers;
using GameBrains.Extensions.Vectors;
using GameBrains.Fuzzy;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
    public abstract class Weapon
    {
        protected int activeProjectileCount;

		protected Weapon(
            WeaponType weaponType,
            int roundsRemaining,
            int maximumRoundsCarried,
            float rateOfFire,
            float idealRange,
            float projectileSpeed,
            ThinkingAgent thinkingAgent)
        {
            FuzzyModule = new FuzzyModule();
            WeaponType = weaponType;
            RoundsRemaining = roundsRemaining;
            ThinkingAgent = thinkingAgent;
            RateOfFire = rateOfFire;
            MaximumRoundsCarried = maximumRoundsCarried;
            LastDesirabilityScore = 0;
            IdealRange = idealRange;
            MaximumProjectileSpeed = projectileSpeed;
            TimeNextAvailable = Time.time;
        }

		public ThinkingAgent ThinkingAgent { get; private set; }
		public ThinkingData ThinkingData => ThinkingAgent.Data;

		public WeaponType WeaponType { get; private set; }

		public int RoundsRemaining { get; private set; }

		public float RateOfFire { get; private set; }

		public int MaximumRoundsCarried { get; private set; }

		public float MaximumProjectileSpeed { get; private set; }

		public float IdealRange { get; private set; }

		public float LastDesirabilityScore { get; set; }

		public float TimeNextAvailable { get; private set; }

		public FuzzyModule FuzzyModule { get; set; }

		public bool AimAt(VectorXYZ targetPosition)
        {
            return RotateAimTowardPosition(targetPosition);
        }

		public abstract void ShootAt(VectorXYZ targetPosition);

		public abstract float GetDesirability(float distanceToTarget);

		public virtual void OnProjectileAdded()
		{
			activeProjectileCount++;
		}

		public virtual void OnProjectileRemoved()
		{
			activeProjectileCount--;

			if (activeProjectileCount < 0)
			{
				activeProjectileCount = 0;
			}
		}

		public void DecrementRounds()
        {
            if (RoundsRemaining > 0)
            {
                RoundsRemaining -= 1;
            }
        }

		public void IncrementRounds(int amount)
        {
            RoundsRemaining
	            = Mathf.Clamp(RoundsRemaining + amount, 0, MaximumRoundsCarried);
        }

		protected bool IsReadyForNextShot()
        {
            return Time.time > TimeNextAvailable;
        }

		protected void UpdateTimeNextAvailable()
        {
            TimeNextAvailable = Time.time + (1.0f / RateOfFire);
        }

		protected abstract void InitializeFuzzyModule();

		protected void InitializeDistanceToTarget(
			out FzSet targetClose,
			out FzSet targetMedium,
			out FzSet targetFar)
		{
			FuzzyVariable distanceToTarget
				= FuzzyModule.CreateFlv("distanceToTarget");

            targetClose
	            = distanceToTarget.AddLeftShoulderSet(
		            "targetClose",
		            0f,
		            10f,
		            20f);
            targetMedium
	            = distanceToTarget.AddTriangularSet(
		            "targetMedium",
		            10f, 30f, 50f);
            targetFar
	            = distanceToTarget.AddRightShoulderSet(
		            "targetFar",
		            30f,
		            60f,
		            100f);
		}

		protected void InitializeDesirability(
			out FzSet undesirable,
			out FzSet desirable,
			out FzSet veryDesirable)
		{
			FuzzyVariable desirability = FuzzyModule.CreateFlv("desirability");

            veryDesirable
	            = desirability.AddRightShoulderSet(
		            "veryDesirable",
		            50f,
		            75f,
		            100f);
            desirable
	            = desirability.AddTriangularSet(
		            "Desirable",
		            25f,
		            50f,
		            75f);
            undesirable
	            = desirability.AddLeftShoulderSet(
		            "undesirable",
		            0f,
		            25f,
		            50f);
		}

		protected void AddSoundTrigger(ThinkingAgent soundSource, float range, Color color)
        {
	        GameObject triggerObjectPrefab = Resources.Load<GameObject>("Prefabs/WoW/SoundNotifyTrigger");
	        GameObject triggerObject = Object.Instantiate(triggerObjectPrefab);
	        TriggerSoundNotify trigger = triggerObject.GetComponent<TriggerSoundNotify>();
	        trigger.enabled = true;
	        var meshRender = trigger.GetComponent<MeshRenderer>();
            meshRender.enabled = Parameters.Instance.SoundTriggerVisible;
            meshRender.material.color = color; // visual cue to who made the sound
			trigger.NoiseMakingAgent = soundSource;
			triggerObject.transform.position = soundSource.Data.Position;

			// TODO: Is y limited to 5 because overhead camera is at 10???
			triggerObject.transform.localScale = new Vector3(range, 5, range);
        }
		
		public bool RotateAimTowardPosition(VectorXYZ target)
		{
			float angle = VectorXYZ.Angle(ThinkingData.Position, target);
			// TODO: Should be a Parameter
			const float weaponAimTolerance = 5; // degrees

			if (angle < weaponAimTolerance)
			{
				ThinkingData.Rotation = Quaternion.LookRotation(target);
				return true;
			}

			var lookRotation = Quaternion.LookRotation(target);
			ThinkingData.Rotation = Quaternion.RotateTowards(ThinkingData.Rotation, lookRotation, ThinkingData.MaximumAngularSpeed);

			return false;
		}
    }
}