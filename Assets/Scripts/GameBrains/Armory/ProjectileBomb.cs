//ADDED Class ProjectileBomb
using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class ProjectileBomb : Projectile
	{
		public void Spawn(Weapon weapon, ThinkingAgent shooter, VectorXYZ targetPosition)
		{
			name = "BOMB_" + ID;
			EntityTypes.Add(Parameters.Instance.BombEntityType);
			Data.MaximumSpeed = Parameters.Instance.BombMaximumSpeed;

			Spawn(weapon, shooter, shooter.Data.Position, targetPosition, Parameters.Instance.BombDamage);

			transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		}

		protected override void ProcessAgentHit(VectorXYZ hitPoint, ThinkingAgent hitThinkingAgent)
		{
			// Skip impact handling because high speed slugs penetrate multiple targets
			//ProcessImpact(hitPoint);
			InflictDamage(hitThinkingAgent, hitPoint);
		}
	}
}