using System.ComponentModel;
using GameBrains.Entities;
using GameBrains.Extensions.Vectors;

// ReSharper disable once CheckNamespace
namespace GameBrains.EventSystem // NOTE: Don't change this namespace
{
    public static partial class Events
    {
        [Description("Damage Inflicted")]
        public static readonly EventType DamageInflicted = (EventType)Count++;

        [Description("Entity Destroyed")]
        public static readonly EventType EntityDestroyed = (EventType)Count++;

        [Description("Weapon Sound")]
        public static readonly EventType WeaponSound = (EventType)Count++;
    }

    public struct DamageInflicted
    {
        public ThinkingAgent shooter;
        public Entity victim;
        public VectorXYZ hitPoint;
        public float damageInflicted;

        public DamageInflicted(
            ThinkingAgent shooter,
            Entity victim,
            VectorXYZ hitPoint,
            float damageInflicted)
        {
            this.shooter = shooter;
            this.victim = victim;
            this.hitPoint = hitPoint;
            this.damageInflicted = damageInflicted;
        }
    }

    public struct EntityDestroyed
    {
        public ThinkingAgent shooter;
        public Entity victim;

        public EntityDestroyed(
            ThinkingAgent shooter,
            Entity victim)
        {
            this.shooter = shooter;
            this.victim = victim;
        }
    }

    public struct WeaponSound
    {
        public ThinkingAgent noiseHearer;
        public ThinkingAgent noiseMaker;

        public WeaponSound(
            ThinkingAgent noiseHearer,
            ThinkingAgent noiseMaker)
        {
            this.noiseHearer = noiseHearer;
            this.noiseMaker = noiseMaker;
        }
    }
}