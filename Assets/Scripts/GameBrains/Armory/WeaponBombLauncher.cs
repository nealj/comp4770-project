//ADDED Class ProjectileBomb
using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Fuzzy;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class WeaponBombLauncher : Weapon
	{
		public WeaponBombLauncher(ThinkingAgent thinkingAgent)
			: base(
				Parameters.Instance.BombLauncherWeaponType,
				Parameters.Instance.BombLauncherDefaultRounds,
				Parameters.Instance.BombLauncherMaximumRoundsCarried,
				Parameters.Instance.BombLauncherFiringFrequency,
				Parameters.Instance.BombLauncherIdealRange,
				Parameters.Instance.BombMaximumSpeed,
				thinkingAgent)
		{
			// setup the fuzzy module
			InitializeFuzzyModule();
		}

		public override void ShootAt(VectorXYZ targetPosition)
		{
			if (RoundsRemaining <= 0 ||
				!IsReadyForNextShot() ||
				activeProjectileCount >= Parameters.Instance.MaximumActiveBombs)
			{
				return;
			}

			AddBomb(ThinkingAgent, targetPosition/*, Parameters.Instance.BombColor*/);
			DecrementRounds();
			UpdateTimeNextAvailable();
			AddSoundTrigger(ThinkingAgent, Parameters.Instance.BombLauncherSoundRange, Parameters.Instance.BombColor);
		}

		public override float GetDesirability(float distanceToTarget)
		{
			if (RoundsRemaining == 0)
			{
				LastDesirabilityScore = 0;
			}
			else
			{
				// fuzzify distance and amount of ammo
				FuzzyModule.Fuzzify("distanceToTarget", distanceToTarget);
				FuzzyModule.Fuzzify("ammoStatus", RoundsRemaining);
				LastDesirabilityScore =
					FuzzyModule.DeFuzzify("desirability", FuzzyModule.DefuzzifyMethod.MaxAv);
			}

			return LastDesirabilityScore;
		}

		protected override void InitializeFuzzyModule()
		{
			InitializeDistanceToTarget(
				out FzSet targetClose,
				out FzSet targetMedium,
				out FzSet targetFar);

			InitializeDesirability(
				out FzSet undesirable,
				out FzSet desirable,
				out FzSet veryDesirable);

			FuzzyVariable ammoStatus = FuzzyModule.CreateFlv("ammoStatus");
			FzSet ammoLoads = ammoStatus.AddRightShoulderSet("ammoLoads", 10, 12, 15);
			FzSet ammoOkay = ammoStatus.AddTriangularSet("ammoOkay", 5, 10, 12);
			FzSet ammoLow = ammoStatus.AddTriangularSet("ammoLow", 0, 2, 6);

			FuzzyModule.AddRule(new FzAnd(targetClose, ammoLoads), undesirable);
			FuzzyModule.AddRule(new FzAnd(targetClose, ammoOkay), undesirable);
			FuzzyModule.AddRule(new FzAnd(targetClose, ammoLow), undesirable);

			FuzzyModule.AddRule(new FzAnd(targetMedium, ammoLoads), veryDesirable);
			FuzzyModule.AddRule(new FzAnd(targetMedium, ammoOkay), veryDesirable);
			FuzzyModule.AddRule(new FzAnd(targetMedium, ammoLow), desirable);
			FuzzyModule.AddRule(new FzAnd(targetFar, ammoLoads), desirable);
			FuzzyModule.AddRule(new FzAnd(targetFar, ammoOkay), undesirable);
			FuzzyModule.AddRule(new FzAnd(targetFar, ammoLow), undesirable);
		}

		// This is public for testing but should be private.
		public void AddBomb(ThinkingAgent shooter, VectorXYZ target, Color? projectileColor = null)
		{
			if (activeProjectileCount < Parameters.Instance.MaximumActiveBombs)
			{
				OnProjectileAdded();

				GameObject bombObjectPrefab = Resources.Load<GameObject>("Prefabs/WoW/Bomb");
				GameObject bombObject = Object.Instantiate(bombObjectPrefab);
				bombObject.GetComponent<Rigidbody>().mass = Parameters.Instance.BombMass;
				if (projectileColor.HasValue)
				{
					bombObject.GetComponent<MeshRenderer>().material.color = projectileColor.Value;
				}
				var bomb = bombObject.GetComponent<ProjectileBomb>();
				bomb.Spawn(this, shooter, target);
			}
		}
	}
}