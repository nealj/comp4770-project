using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Extensions.ScriptableObjects;
using GameBrains.Extensions.Vectors;

namespace GameBrains.Armory
{
	public class TargetingSystem : ExtendedScriptableObject
    {
	    #region Creators

	    public static TargetingSystem CreateInstance(ThinkingAgent thinkingAgent)
	    {
		    var targetingSystem = CreateInstance<TargetingSystem>();
		    Initialize(targetingSystem, thinkingAgent);
		    return targetingSystem;
	    }

	    protected static void Initialize(
		    TargetingSystem targetingSystem,
		    ThinkingAgent thinkingAgent)
	    {
		    targetingSystem.ThinkingAgent = thinkingAgent;
		    targetingSystem.Target = null;
	    }

	    #endregion Creators
	    public ThinkingAgent ThinkingAgent { get; private set; }
	    public ThinkingData ThinkingData => ThinkingAgent.Data;

		public ThinkingAgent Target { get; set; }
		public ThinkingData TargetData => Target.Data;

		public bool IsTargetPresent => Target != null;

		public bool IsTargetWithinFieldOfView
			=> ThinkingData.SensoryMemory.IsOpponentWithinFieldOfView(Target);

		public bool IsTargetShootable => ThinkingData.SensoryMemory.IsOpponentShootable(Target);

		public VectorXZ? LastRecordedPosition => ThinkingData.SensoryMemory.GetLastRecordedPosition(Target);

		public float TimeTargetVisible => ThinkingData.SensoryMemory.GetTimeVisible(Target);

		public float TimeTargetOutOfView => ThinkingData.SensoryMemory.GetTimeOutOfView(Target);

		public void ClearTarget() { Target = null; }

		public void UpdateTarget()
        {
			if (ThinkingAgent == null) { return; }

			ClearTarget();

			float closestDistanceSoFar = float.MaxValue;
			var sensedOpponents = ThinkingData.SensoryMemory.GetListOfRecentlySensedOpponents();

			foreach (ThinkingAgent sensedOpponent in sensedOpponents)
            {
                // make sure the opponent is alive and that it is not the owner
                if (!sensedOpponent.IsActive || sensedOpponent == ThinkingAgent) { continue; }

                float distance = (sensedOpponent.Data.Position - ThinkingData.Position).magnitude;

                if (distance >= closestDistanceSoFar) { continue; }

                closestDistanceSoFar = distance;
                Target = sensedOpponent;
            }
		}
	}
}