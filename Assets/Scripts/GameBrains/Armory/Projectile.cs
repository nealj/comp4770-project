using GameBrains.Entities;
using GameBrains.EventSystem;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public abstract class Projectile : SteerableAgent
    {
	    // Gets or sets a value indicating whether the projectile has impacted but is not yet dead
        // (because it may be exploding outwards from the point of impact for example).
        public bool HasImpacted { get; protected set; }

        // Gets or sets the agent that owns this projectile.
        public ThinkingAgent Shooter { get; protected set; }

		public Weapon Weapon { get; protected set; }

        // Gets or sets the target position this projectile is heading for.
        public VectorXYZ TargetPosition { get; protected set; }

        // Gets or sets where the shot was fired from.
        public VectorXYZ OriginPosition { get; protected set; }

        // Gets or sets the amount of damage a projectile of this type does.
        public float DamageInflicted { get; protected set; }

        // Gets or sets the time when projectile was fired. This enables the shot to be rendered
        // for a specific length of time.
        public float TimeOfCreation { get; set; }

        // Gets or sets the position where this projectile impacts an object.
        public VectorXYZ ImpactPoint
        {
            get => impactPoint;
            set => impactPoint = value;
        }
        // The position where this projectile impacts an object.
        // Since we cannot pass the ImpactPoint property as a ref parameter in some
        // intersection tests, we make this field protected. This breaks encapsulation.
        // TODO (SRC-18): Might be useful to rethink the design of the intersection tests.
        protected VectorXYZ impactPoint;

        public virtual void Spawn(
	        Weapon weapon,
	        ThinkingAgent shooter,
	        VectorXYZ originPosition,
	        VectorXYZ targetPosition,
	        float damageInflicted)
		{
			VectorXYZ heightOffset = VectorXYZ.up * 1.8f; // raise up to shoot over low walls

			Weapon = weapon;
			Shooter = shooter;
			OriginPosition = originPosition + heightOffset;
			TargetPosition = targetPosition;
			DamageInflicted = damageInflicted;
			TimeOfCreation = Time.time;
			IsPlayerControlled = false;

			Spawn(OriginPosition);

			VectorXYZ directionToTarget
				= (targetPosition - (shooter.Data.Position + heightOffset)).normalized;

			transform.position
				= shooter.Data.Position +
				  heightOffset +
				  directionToTarget * shooter.Data.Radius;

			Data.Velocity = (VectorXZ) directionToTarget * Data.MaximumSpeed;
		}

        protected override void Act(float deltaTime)
        {
	        base.Act(deltaTime);

	        // This will eventually kill off projectiles that leave the play area.
	        if (TimeOfCreation + Parameters.Instance.ProjectileTimeToLive < Time.time)
	        {
		        Destroy(gameObject);
		        Weapon.OnProjectileRemoved();
	        }
        }

        public void OnTriggerEnter(Collider hitCollider)
        {
	        var hitEntity = hitCollider.GetComponent<Entity>();

	        if (hitEntity == null) { return; }

	        VectorXYZ hitPoint = hitCollider.ClosestPointOnBounds(transform.position);

	        // TODO: What about the ground?
	        if (hitEntity.EntityTypes.Contains(Parameters.Instance.ObstacleEntityType))
	        {
		        ProcessImpact(hitPoint);
	        }
	        else if (hitEntity.EntityTypes.Contains(Parameters.Instance.AgentEntityType))
	        {
		        var hitThinkingAgent = hitEntity as ThinkingAgent;

		        if (hitThinkingAgent != null &&
		            hitThinkingAgent != Shooter &&
		            (Parameters.Instance.FriendlyFire || Shooter.Color != hitThinkingAgent.Color))
		        {
			        ProcessAgentHit(hitPoint, hitThinkingAgent);
		        }
	        }
        }

        protected virtual void ProcessAgentHit(VectorXYZ hitPoint, ThinkingAgent hitThinkingAgent)
        {
	        ProcessImpact(hitPoint);
	        InflictDamage(hitThinkingAgent, hitPoint);
        }

        protected void ProcessImpact(Vector3 hitPoint)
        {
	        ImpactPoint = hitPoint;
	        HasImpacted = true;
	        IsActive = false;
	        Destroy(gameObject);
	        Weapon.OnProjectileRemoved();
        }

        protected void InflictDamage(ThinkingAgent hitThinkingAgent, VectorXYZ hitPoint)
        {
	        InflictDamage(hitThinkingAgent, hitPoint, DamageInflicted);
        }

        protected void InflictDamage(ThinkingAgent hitThinkingAgent, VectorXYZ hitPoint, float damageInflicted)
        {
	        var damagePayload = new DamageInflicted(Shooter, hitThinkingAgent, hitPoint, damageInflicted);
	        EventManager.Instance.Enqueue(Events.DamageInflicted, damagePayload);
        }
	}
}