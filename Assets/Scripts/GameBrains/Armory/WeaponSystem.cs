using System.Collections.Generic;
using GameBrains.Entities;
using GameBrains.Entities.EntityData;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
    public class WeaponSystem
    {
        readonly Dictionary<WeaponType, Weapon> weaponMap =
            new Dictionary<WeaponType, Weapon>();

        protected Parameters Parameters { get; private set; }

        // A flag that is toggle each time weapon selection is called.
        bool weaponSelectionTickTock;

		float bestDesirability;
        //WeaponType mostDesirable = WeaponTypes.Blaster;
        //WeaponType previousMostDesirable = WeaponTypes.Blaster;

		public WeaponSystem(
			ThinkingAgent thinkingAgent,
		    float reactionTime,
		    float aimAccuracy,
		    float aimPersistenceTime)
        {
	        Parameters = Parameters.Instance;
            ThinkingAgent = thinkingAgent;
			ReactionTime = reactionTime;
			AimAccuracy = aimAccuracy;
            AimPersistenceTime = aimPersistenceTime;
			Initialize();
        }

		public ThinkingAgent ThinkingAgent { get; private set; }
		public ThinkingData ThinkingData => ThinkingAgent.Data;
		
		public TargetingSystem TargetingSystem => ThinkingData.TargetingSystem;
		
		public KinematicData TargetData => TargetingSystem.Target.Data;
		
		public VectorXYZ TargetPosition => TargetData.Position;
		public VectorXYZ TargetVelocityXYZ => TargetData.VelocityXYZ;

		public Weapon CurrentWeapon { get; set; }

		public VectorXYZ AimingPosition { get; private set; }

		public float ReactionTime { get; private set; }

		public float AimAccuracy { get; private set; }

		public float AimPersistenceTime { get; private set; }

		public void Initialize()
		{
			CurrentWeapon = Parameters.UseBlaster ? new WeaponBlaster(ThinkingAgent) : null;

			weaponMap.Clear();
			weaponMap[Parameters.BlasterWeaponType] = CurrentWeapon;
            weaponMap[Parameters.Instance.ShotgunWeaponType] = null;
            weaponMap[Parameters.Instance.RailgunWeaponType] = null;
            weaponMap[Parameters.Instance.RocketLauncherWeaponType] = null;
            weaponMap[Parameters.Instance.BombLauncherWeaponType] = null;
        }

		public void SelectWeapon()
        {
			// If a target is present use fuzzy logic to determine the most desirable weapon.
            if (TargetingSystem.IsTargetPresent)
            {
                weaponSelectionTickTock = !weaponSelectionTickTock;
                //previousMostDesirable = mostDesirable;

				// Calculate the distance to the target.
                float distanceToTarget
	                = Vector3.Distance(
		                ThinkingData.Position,
		                TargetData.Position);

				// For each weapon in the inventory calculate its desirability
                // given the current situation. The most desirable weapon is selected.
                bestDesirability = float.MinValue;

				foreach (KeyValuePair<WeaponType, Weapon> kvp in weaponMap)
                {
                    // Grab the desirability of this weapon (desirability is
                    // based upon distance to target and ammo remaining).
                    if (kvp.Value == null) { continue; }

                    float score = kvp.Value.GetDesirability(distanceToTarget);

                    // If it is the most desirable so far select it.
                    if (score <= bestDesirability) { continue; }

                    bestDesirability = score;

                    // Place the weapon in the agent's hand.
                    CurrentWeapon = kvp.Value;
                    //mostDesirable = kvp.Key;
                }
			}
			else
            {
                CurrentWeapon = weaponMap[Parameters.BlasterWeaponType];
            }
		}

		public void AddWeapon(WeaponType weaponType)
        {
			 // create an instance of this weapon
            Weapon weaponToAdd = null;

            if (weaponType == Parameters.Instance.RailgunWeaponType)
            { weaponToAdd = new WeaponRailgun(ThinkingAgent); }
            else if (weaponType == Parameters.Instance.ShotgunWeaponType)
            { weaponToAdd = new WeaponShotgun(ThinkingAgent); }
            else if (weaponType == Parameters.Instance.RocketLauncherWeaponType)
            { weaponToAdd = new WeaponRocketLauncher(ThinkingAgent); }
            else if (weaponType == Parameters.Instance.BombLauncherWeaponType)
            { weaponToAdd = new WeaponBombLauncher(ThinkingAgent); }
            if (weaponToAdd == null) { return; }

            // If the agent already holds a weapon of this type, just add its ammo.
            Weapon weaponInInventory = GetWeaponFromInventory(weaponType);

            if (weaponInInventory != null)
            {
                weaponInInventory.IncrementRounds(weaponToAdd.RoundsRemaining);
            }

            // If not already holding, add to inventory.
            else
            {
                weaponMap[weaponType] = weaponToAdd;
            }
		}

		public Weapon GetWeaponFromInventory(WeaponType weaponType)
        {
            return weaponMap[weaponType];
        }

		public void ChangeWeapon(WeaponType weaponType)
        {
            Weapon weaponInInventory = GetWeaponFromInventory(weaponType);

            if (weaponInInventory != null)
            {
                CurrentWeapon = weaponInInventory;
            }
        }

		public void TakeAimAndShoot(float deltaTime)
		{
			if (CurrentWeapon == null) { return; }
			
			// Aim the weapon only if the current target is shootable or if it
            // has only very recently gone out of view (this latter condition is
            // to ensure the weapon is aimed at the target even if it temporarily
            // dodges behind a wall or other cover).
            if (TargetingSystem.IsTargetShootable ||
                (TargetingSystem.TimeTargetOutOfView < AimPersistenceTime))
            {
                // the position the weapon will be aimed at
                AimingPosition = TargetData.Position;

				// If the current weapon is not an instant hit type gun the
                // target position must be adjusted to take into account the
                // predicted movement of the target.
                if ((CurrentWeapon.WeaponType == Parameters.RocketLauncherWeaponType && Parameters.UseTargetPredictionForRocketLauncher) ||
                    (CurrentWeapon.WeaponType == Parameters.BlasterWeaponType && Parameters.UseTargetPredictionForBlaster) ||
                    (CurrentWeapon.WeaponType == Parameters.ShotgunWeaponType && Parameters.UseTargetPredictionForShotgun) ||
                    (CurrentWeapon.WeaponType == Parameters.RailgunWeaponType && Parameters.UseTargetPredictionForRailgun) ||
                    (CurrentWeapon.WeaponType == Parameters.BombLauncherWeaponType && Parameters.UseTargetPredictionForBombLauncher))
                {
                    AimingPosition = PredictFuturePositionOfTarget(deltaTime);
    
					// If the weapon is aimed correctly, there is line of sight between
					// the agent and the aiming position and it has been in view for a
					// period longer than the agent's reaction time, shoot the weapon.
                    if (//ThinkingAgent.RotateAimTowardPosition(AimingPosition) &&
                        TargetingSystem.TimeTargetVisible > ReactionTime &&
                        ThinkingData.HasLineOfSight(AimingPosition))
                    {
                        AddNoiseToAim();
                        ShootAt(AimingPosition);
                    }
				}
				// No need to predict movement, aim directly at target.
                else
                {
                    // If the weapon is aimed correctly and it has been in view for
					// a period longer than the agent's reaction time, shoot the weapon.
                    if (//ThinkingAgent.RotateAimTowardPosition(AimingPosition) &&
                        (TargetingSystem.TimeTargetVisible > ReactionTime))
                    {
                        AddNoiseToAim();
                        ShootAt(AimingPosition);
                    }
                }
			}
			// No target to shoot at so rotate aim to be parallel
			// with the agent's heading direction.
            else
            {
                //ThinkingAgent.RotateAimTowardPosition(ThinkingData.Position + ThinkingData.HeadingVector);
            }
		}

		public void AddNoiseToAim()
        {
			VectorXYZ direction = AimingPosition - ThinkingData.Position;
			direction = Quaternion.Euler(0, Random.Range(-AimAccuracy, AimAccuracy), 0) * direction;
			AimingPosition = ThinkingData.Position + direction;
		}

		public VectorXYZ PredictFuturePositionOfTarget(float deltaTime)
        {
			float maximumSpeed = CurrentWeapon.MaximumProjectileSpeed;

			// If the target is ahead and facing the agent shoot at its current position.
            VectorXYZ vectorToEnemy = TargetData.Position - ThinkingData.Position;

			// The lookahead time is proportional to the distance between the
            // enemy and the pursuer; and is inversely proportional to the sum
            // of the agents' velocities.

			float lookAheadTime = vectorToEnemy.magnitude / (maximumSpeed + TargetData.Speed);

			// Return the predicted future position of the enemy.
            return TargetData.Position + TargetData.VelocityXYZ * (lookAheadTime * deltaTime);
		}

		public int GetRoundsRemaining(WeaponType weaponType)
        {
			 return weaponMap[weaponType] != null ?
                weaponMap[weaponType].RoundsRemaining : 0;
		}

		public void ShootAt(VectorXYZ targetPosition)
        {
            CurrentWeapon.ShootAt(targetPosition);
        }
    }
}