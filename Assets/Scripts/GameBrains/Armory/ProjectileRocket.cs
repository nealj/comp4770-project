using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using GameBrains.Motion.Steering.VelocityBased;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class ProjectileRocket : Projectile
	{
		int seekID;

		public void Spawn(Weapon weapon, ThinkingAgent shooter, VectorXYZ targetPosition)
		{
			name = "ROCKET_" + ID;
			EntityTypes.Add(Parameters.Instance.RocketEntityType);
			Data.MaximumSpeed = Parameters.Instance.RocketMaximumSpeed;
			var shooterData = shooter.Data;

			Spawn(weapon, shooter,shooterData.Position, targetPosition,	Parameters.Instance.RocketDamage);

			transform.localScale = new Vector3(0.25f, 0.5f, 0.25f);
			transform.LookAt(targetPosition);
			transform.Rotate(Vector3.right, 90);

			if (Parameters.Instance.RocketIsHeatSeeking)
			{
				var targetingSystem = shooterData.TargetingSystem;
				// TODO: What if target dies before rocket hits. How does seek deal with the TargetKinematic?
				seekID = Data.AddSteeringBehaviour( Seek.CreateInstance(Data, targetingSystem.TargetData));
			}
		}

		protected override void Act(float deltaTime)
		{
			base.Act(deltaTime);
			
			if (!Data.IsAtPosition(TargetPosition, 0.1f)) { return; }

			if (Parameters.Instance.RocketIsHeatSeeking)
			{
				Data.RemoveSteeringBehaviour(seekID);
			}

			ProcessImpact(TargetPosition); // TODO: handle multiple impacts??
			InflictDamageOnAgentsWithinBlastRadius();
		}

		void InflictDamageOnAgentsWithinBlastRadius()
		{
			foreach (ThinkingAgent thinkingAgent in EntityManager.FindAll<ThinkingAgent>())
			{
				var distanceFromBlast = VectorXYZ.Distance(Data.Position, thinkingAgent.Data.Position) - thinkingAgent.Data.Radius;
				if (distanceFromBlast <= Parameters.Instance.RocketBlastRadius)
				{
					var fallOffFactor
						= (Parameters.Instance.RocketBlastRadius -
						   distanceFromBlast) /
						  Parameters.Instance.RocketBlastRadius;
					//TODO (SRC-1): Cache Entity's collider.
					VectorXYZ hitPoint =
						thinkingAgent.Collider.ClosestPointOnBounds(Data.Position);
					InflictDamage(thinkingAgent, hitPoint, DamageInflicted * fallOffFactor);
				}
			}
		}
	}
}