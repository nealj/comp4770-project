using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Fuzzy;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class WeaponBlaster : Weapon
    {
		public WeaponBlaster(ThinkingAgent thinkingAgent)
            : base(
                Parameters.Instance.BlasterWeaponType,
                Parameters.Instance.BlasterDefaultRounds,
                Parameters.Instance.BlasterMaxRoundsCarried,
                Parameters.Instance.BlasterFiringFrequency,
                Parameters.Instance.BlasterIdealRange,
                Parameters.Instance.BlasterMaximumSpeed,
                thinkingAgent)
        {
            // setup the fuzzy module
            InitializeFuzzyModule();
        }
		
		public override void ShootAt(VectorXYZ targetPosition)
		{
			if (!Parameters.Instance.UseBlaster || !IsReadyForNextShot()) { return; }
			
			AddBolt(ThinkingAgent, targetPosition/*, Parameters.Instance.BoltColor*/);
			UpdateTimeNextAvailable();
			AddSoundTrigger(ThinkingAgent, Parameters.Instance.BlasterSoundRange, Parameters.Instance.BoltColor);
		}
		
		public override float GetDesirability(float distanceToTarget)
        {
            // fuzzify distance and amount of ammo
            FuzzyModule.Fuzzify("distanceToTarget", distanceToTarget);

            LastDesirabilityScore =
                FuzzyModule.DeFuzzify("desirability", FuzzyModule.DefuzzifyMethod.MaxAv);

            return LastDesirabilityScore;
        }
		
		protected override void InitializeFuzzyModule()
        {
	        InitializeDistanceToTarget(
		        out FzSet targetClose,
		        out FzSet targetMedium,
		        out FzSet targetFar);

	        InitializeDesirability(
		        out FzSet undesirable,
		        out FzSet desirable,
		        out FzSet _);
					
            FuzzyModule.AddRule(targetClose, desirable);
            FuzzyModule.AddRule(targetMedium, new FzVery(undesirable));
            FuzzyModule.AddRule(targetFar, new FzVery(undesirable));
		}

		// This is public for testing but should be private.
		public void AddBolt(ThinkingAgent shooter, Vector3 target, Color? projectileColor = null)
		{
			if (activeProjectileCount >= Parameters.Instance.MaximumActiveBolts) { return; }

			OnProjectileAdded();
			
			GameObject boltObjectPrefab = Resources.Load<GameObject>("Prefabs/WoW/Bolt");
			GameObject boltObject = Object.Instantiate(boltObjectPrefab);
			boltObject.GetComponent<Rigidbody>().mass = Parameters.Instance.BoltMass;
			if (projectileColor.HasValue)
			{
				boltObject.GetComponent<MeshRenderer>().material.color = projectileColor.Value;
			}
			var bolt = boltObject.GetComponent<ProjectileBolt>();
			bolt.Spawn(this, shooter, target);
		}
	}
}