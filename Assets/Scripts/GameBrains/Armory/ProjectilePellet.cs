using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
    public sealed class ProjectilePellet : Projectile
    {
        public void Spawn(Weapon weapon, ThinkingAgent shooter, VectorXYZ targetPosition)
        {
            name = "PELLET_" + ID;
            EntityTypes.Add(Parameters.Instance.PelletEntityType);
            Data.MaximumSpeed = Parameters.Instance.PelletMaximumSpeed;

            Spawn(weapon, shooter, shooter.Data.Position, targetPosition, Parameters.Instance.PelletDamage);

            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
    }
}