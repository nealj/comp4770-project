using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Fuzzy;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class WeaponRailgun : Weapon
    {
		public WeaponRailgun(ThinkingAgent thinkingAgent)
            : base(
	            Parameters.Instance.RailgunWeaponType,
                Parameters.Instance.RailgunDefaultRounds,
	            Parameters.Instance.RailgunMaximumRoundsCarried,
	            Parameters.Instance.RailgunFiringFrequency,
	            Parameters.Instance.RailgunIdealRange,
	            Parameters.Instance.SlugMaximumSpeed,
                thinkingAgent)
        {
            // setup the fuzzy module
            InitializeFuzzyModule();
        }

		public override void ShootAt(VectorXYZ targetPosition)
		{
			if (RoundsRemaining <= 0 || !IsReadyForNextShot()) { return; }

			AddSlug(ThinkingAgent, targetPosition/*, Parameters.Instance.SlugColor*/);
			DecrementRounds();
			UpdateTimeNextAvailable();
			AddSoundTrigger(ThinkingAgent, Parameters.Instance.RailgunSoundRange, Parameters.Instance.SlugColor);
		}

		public override float GetDesirability(float distanceToTarget)
        {
			if (RoundsRemaining == 0) { LastDesirabilityScore = 0; }
            else
            {
				// fuzzify distance and amount of ammo
                FuzzyModule.Fuzzify("distanceToTarget", distanceToTarget);
                FuzzyModule.Fuzzify("ammoStatus", RoundsRemaining);
                LastDesirabilityScore =
                    FuzzyModule.DeFuzzify(
	                    "desirability",
	                    FuzzyModule.DefuzzifyMethod.MaxAv);
			}

            return LastDesirabilityScore;
        }

		protected override void InitializeFuzzyModule()
        {
	        InitializeDistanceToTarget(
		        out FzSet targetClose,
		        out FzSet targetMedium,
		        out FzSet targetFar);

	        InitializeDesirability(
		        out FzSet undesirable,
		        out FzSet desirable,
		        out FzSet veryDesirable);

            FuzzyVariable ammoStatus = FuzzyModule.CreateFlv("ammoStatus");
            FzSet ammoLoads
	            = ammoStatus.AddRightShoulderSet(
		            "ammoLoads",
		            8,
		            12,
		            15);
            FzSet ammoOkay
	            = ammoStatus.AddTriangularSet(
		            "ammoOkay",
		            0,
		            5,
		            10);
            FzSet ammoLow
	            = ammoStatus.AddTriangularSet("ammoLow", 0, 0, 4);

            FuzzyModule.AddRule(
	            new FzAnd(targetClose, ammoLoads),
	            new FzFairly(desirable));
            FuzzyModule.AddRule(
	            new FzAnd(targetClose, ammoOkay),
	            new FzFairly(desirable));
            FuzzyModule.AddRule(new FzAnd(targetClose, ammoLow), undesirable);

            FuzzyModule.AddRule(
	            new FzAnd(targetMedium, ammoLoads),
	            veryDesirable);
            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoOkay), desirable);
            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoLow), desirable);
            FuzzyModule.AddRule(
	            new FzAnd(targetFar, ammoLoads),
	            new FzVery(veryDesirable));
            FuzzyModule.AddRule(
	            new FzAnd(targetFar, ammoOkay),
	            new FzVery(veryDesirable));
            FuzzyModule.AddRule(
	            new FzAnd(targetFar, new FzFairly(ammoLow)),
	            veryDesirable);
		}

		// This is public for testing but should be private.
		public void AddSlug(ThinkingAgent shooter, VectorXYZ target, Color? projectileColor = null)
        {
			if (activeProjectileCount < Parameters.Instance.MaximumActiveSlugs)
			{
				OnProjectileAdded();
				
				GameObject slugObjectPrefab = Resources.Load<GameObject>("Prefabs/WoW/Slug");
				GameObject slugObject = Object.Instantiate(slugObjectPrefab);
				slugObject.GetComponent<Rigidbody>().mass = Parameters.Instance.SlugMass;
				if (projectileColor.HasValue)
				{
					slugObject.GetComponent<MeshRenderer>().material.color = projectileColor.Value;
				}
				var slug = slugObject.GetComponent<ProjectileSlug>();
				slug.Spawn(this, shooter, target);
			}
        }
	}
}