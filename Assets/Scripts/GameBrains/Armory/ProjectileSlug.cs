using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class ProjectileSlug : Projectile
	{
		public void Spawn(Weapon weapon, ThinkingAgent shooter, VectorXYZ targetPosition)
		{
			name = "SLUG_" + ID;
			EntityTypes.Add(Parameters.Instance.SlugEntityType);
			Data.MaximumSpeed = Parameters.Instance.SlugMaximumSpeed;

			Spawn(weapon,
				shooter,
				shooter.Data.Position,
				targetPosition,
				Parameters.Instance.SlugDamage);

			transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		}

		protected override void ProcessAgentHit(VectorXYZ hitPoint, ThinkingAgent hitThinkingAgent)
		{
			// Skip impact handling because high speed slugs penetrate multiple targets
			//ProcessImpact(hitPoint);
			InflictDamage(hitThinkingAgent, hitPoint);
		}
	}
}