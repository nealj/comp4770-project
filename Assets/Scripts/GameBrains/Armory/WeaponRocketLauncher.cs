using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Fuzzy;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class WeaponRocketLauncher : Weapon
    {
		public WeaponRocketLauncher(ThinkingAgent thinkingAgent)
            : base(
	            Parameters.Instance.RocketLauncherWeaponType,
	            Parameters.Instance.RocketLauncherDefaultRounds,
	            Parameters.Instance.RocketLauncherMaximumRoundsCarried,
	            Parameters.Instance.RocketLauncherFiringFrequency,
	            Parameters.Instance.RocketLauncherIdealRange,
	            Parameters.Instance.RocketMaximumSpeed,
                thinkingAgent)
        {
            // setup the fuzzy module
            InitializeFuzzyModule();
        }

		public override void ShootAt(VectorXYZ targetPosition)
		{
			if (RoundsRemaining <= 0 ||
			    !IsReadyForNextShot() ||
			    activeProjectileCount >= Parameters.Instance.MaximumActiveRockets)
			{
				return;
			}

			AddRocket(ThinkingAgent, targetPosition/*, Parameters.Instance.RocketColor*/);
			DecrementRounds();
			UpdateTimeNextAvailable();
			AddSoundTrigger(ThinkingAgent, Parameters.Instance.RocketLauncherSoundRange, Parameters.Instance.RocketColor);
		}

		public override float GetDesirability(float distanceToTarget)
        {
			if (RoundsRemaining == 0)
            {
                LastDesirabilityScore = 0;
            }
            else
            {
				// fuzzify distance and amount of ammo
                FuzzyModule.Fuzzify("distanceToTarget", distanceToTarget);
                FuzzyModule.Fuzzify("ammoStatus", RoundsRemaining);
                LastDesirabilityScore =
                    FuzzyModule.DeFuzzify("desirability", FuzzyModule.DefuzzifyMethod.MaxAv);
			}

            return LastDesirabilityScore;
        }

		protected override void InitializeFuzzyModule()
        {
	        InitializeDistanceToTarget(
		        out FzSet targetClose,
		        out FzSet targetMedium,
		        out FzSet targetFar);

	        InitializeDesirability(
		        out FzSet undesirable,
		        out FzSet desirable,
		        out FzSet veryDesirable);

            FuzzyVariable ammoStatus = FuzzyModule.CreateFlv("ammoStatus");
            FzSet ammoLoads = ammoStatus.AddRightShoulderSet("ammoLoads", 10, 12, 15);
            FzSet ammoOkay = ammoStatus.AddTriangularSet("ammoOkay", 5, 10, 12);
            FzSet ammoLow = ammoStatus.AddTriangularSet("ammoLow", 0, 2, 6);

            FuzzyModule.AddRule(new FzAnd(targetClose, ammoLoads), undesirable);
            FuzzyModule.AddRule(new FzAnd(targetClose, ammoOkay), undesirable);
            FuzzyModule.AddRule(new FzAnd(targetClose, ammoLow), undesirable);

            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoLoads), veryDesirable);
            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoOkay), veryDesirable);
            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoLow), desirable);
            FuzzyModule.AddRule(new FzAnd(targetFar, ammoLoads), desirable);
            FuzzyModule.AddRule(new FzAnd(targetFar, ammoOkay), undesirable);
            FuzzyModule.AddRule(new FzAnd(targetFar, ammoLow), undesirable);
		}

		// This is public for testing but should be private.
		public void AddRocket(ThinkingAgent shooter, VectorXYZ target, Color? projectileColor = null)
        {
			if (activeProjectileCount < Parameters.Instance.MaximumActiveRockets)
			{
				OnProjectileAdded();

				GameObject rocketObjectPrefab = Resources.Load<GameObject>("Prefabs/WoW/Rocket");
				GameObject rocketObject = Object.Instantiate(rocketObjectPrefab);
				rocketObject.GetComponent<Rigidbody>().mass = Parameters.Instance.RocketMass;
				if (projectileColor.HasValue)
				{
					rocketObject.GetComponent<MeshRenderer>().material.color = projectileColor.Value;
				}
				var rocket = rocketObject.GetComponent<ProjectileRocket>();
				rocket.Spawn(this, shooter, target);
			}
        }
	}
}