using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.Fuzzy;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class WeaponShotgun : Weapon
    {
		public WeaponShotgun(ThinkingAgent thinkingAgent)
            : base(
	            Parameters.Instance.ShotgunWeaponType,
	            Parameters.Instance.ShotgunDefaultRounds,
	            Parameters.Instance.ShotgunMaximumRoundsCarried,
	            Parameters.Instance.ShotgunFiringFrequency,
	            Parameters.Instance.ShotgunIdealRange,
	            Parameters.Instance.PelletMaximumSpeed,
                thinkingAgent)
        {
	        // temp debugging code
            // Targets = new List<VectorXYZ>();
            BallsInShell = Parameters.Instance.ShotgunBallsInShell;
            Spread = Parameters.Instance.ShotgunSpread;

            // setup the fuzzy module
            InitializeFuzzyModule();
        }

		public int BallsInShell { get; private set; }

		public float Spread { get; private set; }

		// temp debugging code
		// public List<VectorXYZ> Targets { get; private set; }

		public override void ShootAt(VectorXYZ targetPosition)
		{
			if (RoundsRemaining <= 0 || !IsReadyForNextShot()) { return; }

			// temp debugging code
			// Targets.Clear();

			// a shotgun cartridge contains lots of tiny metal balls called
            // pellets. Therefore, every time the shotgun is discharged we
            // have to calculate the spread of the pellets and add one for
            // each trajectory
            for (int b = 0; b < BallsInShell; ++b)
            {
                // determine deviation from target using a bell curve type distribution
                float deviation =
                    Random.Range(0, Spread) +
                    Random.Range(0, Spread) - Spread;

                Vector3 adjustedTarget = targetPosition;

                // rotate the target vector by the deviation
                Vector3Extensions.RotateAroundPivot(
                    ThinkingAgent.Data.Position,
                    Quaternion.Euler(0, deviation, 0),
					ref adjustedTarget);

                // add a pellet to the game world
                AddPellet(ThinkingAgent, adjustedTarget/*, Parameters.Instance.PelletColor*/);

                // temp debugging code
                // Targets.Add(adjustedTarget);
            }

			DecrementRounds();
			UpdateTimeNextAvailable();
			// Color parameter gives visual cue to who made the noise
			AddSoundTrigger(ThinkingAgent, Parameters.Instance.ShotgunSoundRange, Parameters.Instance.PelletColor);
		}

		public override float GetDesirability(float distanceToTarget)
        {
			if (RoundsRemaining == 0)
            {
                LastDesirabilityScore = 0;
            }
            else
            {
				// fuzzify distance and amount of ammo
                FuzzyModule.Fuzzify("distanceToTarget", distanceToTarget);
                FuzzyModule.Fuzzify("ammoStatus", RoundsRemaining);
                LastDesirabilityScore = FuzzyModule.DeFuzzify("desirability", FuzzyModule.DefuzzifyMethod.MaxAv);
			}

            return LastDesirabilityScore;
        }

		protected override void InitializeFuzzyModule()
        {
	        InitializeDistanceToTarget(
		        out FzSet targetClose,
		        out FzSet targetMedium,
		        out FzSet targetFar);

	        InitializeDesirability(
		        out FzSet undesirable,
		        out FzSet desirable,
		        out FzSet veryDesirable);

            FuzzyVariable ammoStatus = FuzzyModule.CreateFlv("ammoStatus");
            FzSet ammoLoads = ammoStatus.AddRightShoulderSet("ammoLoads", 15, 20, 30);
            FzSet ammoOkay = ammoStatus.AddTriangularSet("ammoOkay", 0, 10, 20);
            FzSet ammoLow = ammoStatus.AddTriangularSet("ammoLow", 0, 0, 10);

            FuzzyModule.AddRule(new FzAnd(targetClose, ammoLoads), veryDesirable);
            FuzzyModule.AddRule(new FzAnd(targetClose, ammoOkay), veryDesirable);
            FuzzyModule.AddRule(new FzAnd(targetClose, ammoLow), veryDesirable);

            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoLoads), veryDesirable);
            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoOkay), desirable);
            FuzzyModule.AddRule(new FzAnd(targetMedium, ammoLow), undesirable);

            FuzzyModule.AddRule(new FzAnd(targetFar, ammoLoads), desirable);
            FuzzyModule.AddRule(new FzAnd(targetFar, ammoOkay), undesirable);
            FuzzyModule.AddRule(new FzAnd(targetFar, ammoLow), undesirable);
		}

		// This is public for testing but should be private.
		public void AddPellet(ThinkingAgent shooter, VectorXYZ target, Color? projectileColor = null)
        {
			if (activeProjectileCount < Parameters.Instance.MaximumActivePellets)
			{
				OnProjectileAdded();

				GameObject pelletObjectPrefab = Resources.Load<GameObject>("Prefabs/WoW/Pellet");
				GameObject pelletObject = Object.Instantiate(pelletObjectPrefab);
				pelletObject.GetComponent<Rigidbody>().mass = Parameters.Instance.PelletMass;
				if (projectileColor.HasValue)
				{
					pelletObject.GetComponent<MeshRenderer>().material.color = projectileColor.Value;
				}
				var pellet = pelletObject.GetComponent<ProjectilePellet>();
				pellet.Spawn(this, shooter, target);
			}
        }
    }
}