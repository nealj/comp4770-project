using GameBrains.Extensions.ScriptableObjects;
using UnityEngine;

namespace GameBrains.Armory
{
    [CreateAssetMenu(fileName = "WeaponType", menuName = "AI/Enums/WeaponType", order = 0)]
    public class WeaponType : EnumScriptableObject { }
}