using GameBrains.Entities;
using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Armory
{
	public sealed class ProjectileBolt : Projectile
    {
		public void Spawn(Weapon weapon, ThinkingAgent shooter, VectorXYZ targetPosition)
		{
			name = "BOLT_" + ID;
			EntityTypes.Add(Parameters.Instance.BoltEntityType);
			Data.MaximumSpeed = Parameters.Instance.BoltMaximumSpeed;

			Spawn(weapon,
				shooter,
				shooter.Data.Position,
				targetPosition,
				Parameters.Instance.BoltDamage);

			transform.localScale = new Vector3(0.25f, 1, 0.25f);
			transform.LookAt(targetPosition);
			transform.Rotate(Vector3.right, 90);
		}
    }
}