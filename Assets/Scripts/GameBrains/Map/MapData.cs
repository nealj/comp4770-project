using GameBrains.Extensions.Vectors;
using GameBrains.GameManagement;
using UnityEngine;

namespace GameBrains.Map
{
    public static class MapData
    {
        // TODO: Should these be automatically initialize using map analysis?
        public static float sizeX = 100f;
        public static float sizeZ = 75f;
        public static float lowestY = 0f;
        public static float highestY = 3f;

        public static VectorXYZ SurfacePoint(VectorXZ point)
        {
            Vector3 dropFromPosition = (Vector3)point;
            // TODO: NodeDropFromHeightOffset and highestY should be connected.
            dropFromPosition.y += Parameters.Instance.NodeDropFromHeightOffset;

            if (Physics.SphereCast(
                    dropFromPosition,
                    Parameters.Instance.NodeRadius, // TODO: Should be a parameter of the method?
                    Vector3.down,
                    out RaycastHit hitInfo,
                    float.MaxValue, // TODO: Should be connected to drop from height.
                    Parameters.Instance.ObstacleLayerMask))
            {
                // TODO: Offset should be a parameter of the method call?
                dropFromPosition = hitInfo.point + Vector3.up * Parameters.Instance.NodeSurfaceOffset;
            }
            
            return dropFromPosition;
        }
    }
}