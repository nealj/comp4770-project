using System.ComponentModel;
using GameBrains.Armory;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals;
using GameBrains.Entities.Types;
using GameBrains.Extensions.Attributes;
using GameBrains.Extensions.MonoBehaviours;
using UnityEngine;

namespace GameBrains.GameManagement
{
	public sealed class Parameters : ExtendedMonoBehaviour
	{
		#region Parameters Singleton

		static Parameters instance;

		public static Parameters Instance
		{
			get
			{
				if (!instance)
				{
					instance = FindObjectOfType<Parameters>();
				}

				return instance;
			}
		}

		#endregion Parameters Singleton
		
		#region Resource Path Settings

		#region Entity Types Path

		[Foldout("Resource Path Settings")] [SerializeField]
		string entityTypesPath = "ScriptableObjects/EntityTypes/";
		
		// Gets or sets the path to EntityType scriptable objects.
		[Category("Resource Path Settings")]
		[Description("The path to EntityTypes scriptable objects.")]
		public string EntityTypesPath
		{
			get => entityTypesPath;
			set => entityTypesPath = value;
		}

		#endregion Entity Types Path

		#region Entity Types Framework Path

		[Foldout("Resource Path Settings")] [SerializeField]
		string entityTypesFrameworkPath = "ScriptableObjects/EntityTypes/Framework/";
		
		// Gets or sets the path to EntityTypes Framework scriptable objects.
		[Category("Resource Path Settings")]
		[Description("The path to EntityTypes Framework scriptable objects.")]
		public string EntityTypesFrameworkPath
		{
			get => entityTypesFrameworkPath;
			set => entityTypesFrameworkPath = value;
		}

		#endregion Entity Types Framework Path

		#region Default Entity Path

		[Foldout("Resource Path Settings")] [SerializeField]
		string defaultEntityTypePath = "ScriptableObjects/EntityTypes/Framework/Default";
		
		// Gets or sets the path to the default EntityType scriptable objects.
		[Category("Resource Path Settings")]
		[Description("The path to the default EntityType scriptable objects.")]
		public string DefaultEntityTypePath
		{
			get => defaultEntityTypePath;
			set => defaultEntityTypePath = value;
		}

		#endregion Default Entity Path
		
		#region Goal Types Path

		[Foldout("Resource Path Settings")] [SerializeField]
		string goalTypesPath = "ScriptableObjects/GoalTypes/";

		[Category("Resource Path Settings")]
		[Description("The path to GoalType scriptable objects.")]
		public string GoalTypesPath { get => goalTypesPath; set => goalTypesPath = value; }

		#endregion Goal Types Path

		#region Weapon Types Path

		[Foldout("Resource Path Settings")] [SerializeField]
		string weaponTypesPath = "ScriptableObjects/WeaponTypes/";

		[Category("Resource Path Settings")]
		[Description("The path to WeaponType scriptable objects.")]
		public string WeaponTypesPath { get => weaponTypesPath; set => weaponTypesPath = value; }

		#endregion Weapon Types Path

		#region Sound Notify Trigger Path

		[Foldout("Resource Path Settings")] [SerializeField]
		string soundNotifyTriggerPrefab = "Prefabs/WoW/SoundNotifyTrigger";

		[Category("Resource Path Settings")]
		[Description("The path to SoundNotifyTrigger prefab.")]
		public string SoundNotifyTriggerPrefab
		{
			get => soundNotifyTriggerPrefab; set => soundNotifyTriggerPrefab = value;
		}

		#endregion Sound Notify Trigger Path

		#endregion Resource Path Settings

		#region Visualizer Settings

		[Foldout("Visualizer Settings")] [SerializeField]
		float visualizeThreshold = 0.1f;
		
		// Gets or sets the smallest ray or capsule to visualize.
		[Category("Visualizer Settings")]
		[Description("Hide ray or capsule visualizer smaller than this size.")]
		public float VisualizeThreshold
		{
			get => visualizeThreshold;
			set => visualizeThreshold = value;
		}
		
		[Foldout("Visualizer Settings")] [SerializeField]
		float visualizerHideAfter = 1f;
		
		// Gets or sets the time in seconds after which to hide the visualizer.
		[Category("Visualizer Settings")]
		[Description("Hide the visualizer after these seconds.")]
		public float VisualizerHideAfter
		{
			get => visualizerHideAfter;
			set => visualizerHideAfter = value;
		}

		#region Point Marker Settings

		[Foldout("Visualizer Settings")] [SerializeField]
		float pointMarkerDropFromHeightOffset = 10f; // should be higher than the highest point.
		
		// Gets or sets the height offset to drop a point marker from.
		[Category("Visualizer Settings")]
		[Description("The height offset to drop a point marker from.")]
		public float PointMarkerDropFromHeightOffset
		{
			get => pointMarkerDropFromHeightOffset;
			set => pointMarkerDropFromHeightOffset = value;
		}

		[Foldout("Visualizer Settings")] [SerializeField]
		float pointMarkerRadius = 0.5f;
		
		// Gets or sets the point marker radius.
		[Category("Visualizer Settings")]
		[Description("The point marker radius.")]
		public float PointMarkerRadius
		{
			get => pointMarkerRadius;
			set => pointMarkerRadius = value;
		}

		[Foldout("Visualizer Settings")] [SerializeField]
		float pointMarkerSurfaceOffset = 0f;
		
		// Gets or sets the offset from the surface to drop the point marker.
		[Category("Visualizer Settings")]
		[Description("The offset from the surface to drop the point marker.")]
		public float PointMarkerSurfaceOffset
		{
			get => pointMarkerSurfaceOffset;
			set => pointMarkerSurfaceOffset = value;
		}
		
		#endregion Point Marker Settings
		
		#region Point Beacon Settings

		[Foldout("Visualizer Settings")] [SerializeField]
		float pointBeaconDropFromHeightOffset = 10f; // should be higher than the highest point.
		
		// Gets or sets the height offset to drop a point beacon from.
		[Category("Visualizer Settings")]
		[Description("The height offset to drop a point beacon from.")]
		public float PointBeaconDropFromHeightOffset
		{
			get => pointBeaconDropFromHeightOffset;
			set => pointBeaconDropFromHeightOffset = value;
		}

		[Foldout("Visualizer Settings")] [SerializeField]
		float pointBeaconRadius = 0.5f;
		
		// Gets or sets the point beacon radius.
		[Category("Visualizer Settings")]
		[Description("The point beacon radius.")]
		public float PointBeaconRadius
		{
			get => pointBeaconRadius;
			set => pointBeaconRadius = value;
		}
		
		[Foldout("Visualizer Settings")] [SerializeField]
		float pointBeaconHeight = 5f;
		
		// Gets or sets the point beacon height.
		[Category("Visualizer Settings")]
		[Description("The point beacon height.")]
		public float PointBeaconHeight
		{
			get => pointBeaconHeight;
			set => pointBeaconHeight = value;
		}

		[Foldout("Visualizer Settings")] [SerializeField]
		float pointBeaconSurfaceOffset = 0f;
		
		// Gets or sets the offset from the surface to drop the point beacon.
		[Category("Visualizer Settings")]
		[Description("The offset from the surface to drop the point beacon.")]
		public float PointBeaconSurfaceOffset
		{
			get => pointBeaconSurfaceOffset;
			set => pointBeaconSurfaceOffset = value;
		}
		
		#endregion Point Beacon Settings
		
		#region Edge Beacon Settings

		[Foldout("Visualizer Settings")] [SerializeField]
		float edgeBeaconDropFromHeightOffset = 10f; // should be higher than the highest point.
		
		// Gets or sets the height offset to drop an edge beacon from.
		[Category("Visualizer Settings")]
		[Description("The height offset to drop an edge beacon from.")]
		public float EdgeBeaconDropFromHeightOffset
		{
			get => edgeBeaconDropFromHeightOffset;
			set => edgeBeaconDropFromHeightOffset = value;
		}

		[Foldout("Visualizer Settings")] [SerializeField]
		float edgeBeaconRadius = 0.5f;
		
		// Gets or sets the edge beacon radius.
		[Category("Visualizer Settings")]
		[Description("The edge beacon radius.")]
		public float EdgeBeaconRadius
		{
			get => edgeBeaconRadius;
			set => edgeBeaconRadius = value;
		}

		[Foldout("Visualizer Settings")] [SerializeField]
		float edgeBeaconSurfaceOffset = 0f;
		
		// Gets or sets the offset from the surface to drop the edge beacon.
		[Category("Visualizer Settings")]
		[Description("The offset from the surface to drop the edge beacon.")]
		public float EdgeBeaconSurfaceOffset
		{
			get => edgeBeaconSurfaceOffset;
			set => edgeBeaconSurfaceOffset = value;
		}
		
		#endregion Edge Beacon Settings

		#endregion Visualizer Settings

		#region Obstacle Settings

		[Foldout("Obstacle Settings")] [SerializeField]
		LayerMask obstacleLayerMask;
		
		// Gets the obstacle layer mask. There is currently no setter. We are assuming for now
		// that all obstacles are on one layer. This could be changed in the future.
		[Category("Obstacle Settings")]
		[Description("The obstacle layer.")]
		public LayerMask ObstacleLayerMask
		{
			get => obstacleLayerMask;
			set => obstacleLayerMask = value;
		}
		
		[Foldout("Ground Settings")] [SerializeField]
		LayerMask groundLayerMask;
		
		// Gets the ground layer mask. There is currently no setter. We are assuming for now
		// that all ground is on one layer. This could be changed in the future.
		[Category("Ground Settings")]
		[Description("The ground layer.")]
		public LayerMask GroundLayerMask
		{
			get => groundLayerMask;
			set => groundLayerMask = value;
		}

		#region Teams

		[Foldout("Obstacle Settings")] [SerializeField]
		LayerMask blueTeamLayerMask;

		[Category("Obstacle Settings")]
		[Description("The blue layer.")]
		public LayerMask BlueTeamLayerMask
		{
			get => blueTeamLayerMask;
			set => blueTeamLayerMask = value;
		}

		[Foldout("Obstacle Settings")] [SerializeField]
		LayerMask redTeamLayerMask;

		[Category("Obstacle Settings")]
		[Description("The red team layer.")]
		public LayerMask RedTeamLayerMask
		{
			get => redTeamLayerMask;
			set => redTeamLayerMask = value;
		}

		[Category("Obstacle Settings")]
		[Description("The obstacle plus teams layer.")]
		public LayerMask ObstaclePlusTeamsLayerMask
			=> ObstacleLayerMask | BlueTeamLayerMask | RedTeamLayerMask;

		[Category("Obstacle Settings")]
		[Description("The teams layer.")]
		public LayerMask TeamsLayerMask => BlueTeamLayerMask | RedTeamLayerMask;

		#endregion Teams

		[Foldout("Obstacle Settings")] [SerializeField]
		EntityType obstacleEntityType;

		[Category("Obstacle Settings")]
		[Description("Obstacle Entity Type.")]
		public EntityType ObstacleEntityType
		{
			get
			{
				if (!obstacleEntityType)
					obstacleEntityType = Resources.Load<EntityType>($"{EntityTypesPath}Obstacle");
				return obstacleEntityType;
			}
		}

		#endregion Obstacle Settings

		#region Navigation Settings
		
		#region Path Management Settings

		[Foldout("Path Management Settings")] [SerializeField]
		public int maximumSearchCyclesPerUpdateStep = 1000;
		
		// Gets or sets the maximum number of search cycles allocated to each path planning search per update.
		[Category("Path Management Settings")]
		[Description("The maximum number of search cycles allocated to each path planning search per update.")]
		public int MaximumSearchCyclesPerUpdateStep
		{
			get => maximumSearchCyclesPerUpdateStep;
			set => maximumSearchCyclesPerUpdateStep = value;
		}

		#endregion Path Management Settings

		#region Search Graph Settings

		#region Node Settings

		[Foldout("Node Settings")] [SerializeField]
		float nodeDropFromHeightOffset = 10f; // should be higher than the highest point.
		
		// Gets or sets the height offset to drop a node from.
		[Category("Node Settings")]
		[Description("The height offset to drop a node from.")]
		public float NodeDropFromHeightOffset
		{
			get => nodeDropFromHeightOffset;
			set => nodeDropFromHeightOffset = value;
		}

		[Foldout("Node Settings")] [SerializeField]
		float nodeRadius = 0.5f;
		
		// Gets or sets the node radius.
		[Category("Node Settings")]
		[Description("The node radius.")]
		public float NodeRadius
		{
			get => nodeRadius;
			set => nodeRadius = value;
		}

		[Foldout("Node Settings")] [SerializeField]
		bool nodeUseCapsuleCast;
		
		// Gets or sets whether to use capsule cast for checking node line of sight.
		[Category("Node Settings")]
		[Description("Use capsule cast for connecting node line of sight.")]
		public bool NodeUseCapsuleCast
		{
			get => nodeUseCapsuleCast;
			set => nodeUseCapsuleCast = value;
		}

		[Foldout("Node Settings")] [SerializeField]
		float nodeCastMaximumDistance = 20f;
		
		// Gets or sets maximum distance for checking node line of sight.
		[Category("Node Settings")]
		[Description("Maximum distance for checking node line of sight.")]
		public float NodeCastMaximumDistance
		{
			get => nodeCastMaximumDistance;
			set => nodeCastMaximumDistance = value;
		}

		[Foldout("Node Settings")] [SerializeField]
		float nodeCastPathRadius = 0.5f;
		
		// Gets or sets the radius for checking node line of sight.
		[Category("Node Settings")]
		[Description("Radius for checking node line of sight.")]
		public float NodeCastPathRadius
		{
			get => nodeCastPathRadius;
			set => nodeCastPathRadius = value;
		}

		[Foldout("Node Settings")] [SerializeField]
		float nodeSurfaceOffset = 1.1f;
		
		// Gets or sets the offset from the surface to drop the node.
		[Category("Node Settings")]
		[Description("The offset from the surface to drop the node.")]
		public float NodeSurfaceOffset
		{
			get => nodeSurfaceOffset;
			set => nodeSurfaceOffset = value;
		}

		#endregion Node Settings

		#region Node Collection Settings

		[Foldout("Node Collection Settings")] [SerializeField]
		Color nodeDefaultColor = Color.yellow;
		
		// Gets or sets the default node color.
		[Category("Node Collection Settings")]
		[Description("Default node color.")]
		public Color NodeDefaultColor
		{
			get
			{
				nodeDefaultColor.a = NodeDefaultAlpha;
				return nodeDefaultColor;
			}
			set => nodeDefaultColor = value;
		}

		[Foldout("Node Collection Settings")] [SerializeField]
		float nodeDefaultAlpha = 0.25f;
		
		// Gets or sets the default alpha for node color.
		[Category("Node Collection Settings")]
		[Description("Default alpha for node color.")]
		public float NodeDefaultAlpha
		{
			get => nodeDefaultAlpha;
			set => nodeDefaultAlpha = value;
		}

		[Foldout("Node Collection Settings")] [SerializeField]
		bool nodeCollectionIsVisible = true;
		
		// Gets or sets the whether the nodes in the node collection are visible.
		[Category("Node Collection Settings")]
		[Description("Whether the nodes in the node collection are visible.")]
		public bool NodeCollectionIsVisible
		{
			get => nodeCollectionIsVisible;
			set => nodeCollectionIsVisible = value;
		}

		#endregion Node Collection Settings

		#region Edge Settings

		#endregion Edge Settings

		#region Edge Collection Settings

		[Foldout("Edge Collection Settings")] [SerializeField]
		Color edgeDefaultColor = Color.blue;
		
		// Gets or sets the default edge color.
		[Category("Edge Collection Settings")]
		[Description("Default edge color.")]
		public Color EdgeDefaultColor
		{
			get
			{
				edgeDefaultColor.a = EdgeDefaultAlpha;
				return edgeDefaultColor;
			}
			set => edgeDefaultColor = value;
		}

		[Foldout("Edge Collection Settings")] [SerializeField]
		float edgeDefaultAlpha = 0.25f;
		
		// Gets or sets the default alpha for edge color.
		[Category("Edge Collection Settings")]
		[Description("Default alpha for edge color.")]
		public float EdgeDefaultAlpha
		{
			get => edgeDefaultAlpha;
			set => edgeDefaultAlpha = value;
		}

		[Foldout("Edge Collection Settings")] [SerializeField]
		bool edgeCollectionIsVisible = true;
		
		// Gets or sets the whether the edges in the edge collection are visible.
		[Category("Edge Collection Settings")]
		[Description("Whether the edges in the edge collection are visible.")]
		public bool EdgeCollectionIsVisible
		{
			get => edgeCollectionIsVisible;
			set => edgeCollectionIsVisible = value;
		}

		#endregion Edge Collection Settings

		#region Graph Settings

		[Foldout("Graph Settings")] [SerializeField]
		string nodePrefabPath = "Prefabs/Navigation/SearchGraph/Node";
		
		// Gets or sets the path to the prefab to use for creating new nodes.
		[Category("Graph Settings")]
		[Description("The path to the prefab to use for creating new nodes.")]
		public string NodePrefabPath
		{
			get => nodePrefabPath;
			set => nodePrefabPath = value;
		}
		
		[Foldout("Graph Settings")] [SerializeField]
		string nodeMaterialPath = "Materials/NavigationMaterials/NodeMaterial";
		
		// Gets or sets the path to the material to use for creating new nodes.
		[Category("Graph Settings")]
		[Description("The path to the material to use for creating new nodes.")]
		public string NodeMaterialPath
		{
			get => nodeMaterialPath;
			set => nodeMaterialPath = value;
		}

		[Foldout("Graph Settings")] [SerializeField]
		string edgePrefabPath = "Prefabs/Navigation/SearchGraph/Edge";
		
		// Gets or sets the path to the prefab to use for creating new edges.
		[Category("Graph Settings")]
		[Description("The path to the prefab to use for creating new edges.")]
		public string EdgePrefabPath
		{
			get => edgePrefabPath;
			set => edgePrefabPath = value;
		}
		
		[Foldout("Graph Settings")] [SerializeField]
		string edgeMaterialPath = "Materials/NavigationMaterials/EdgeMaterial";
		
		// Gets or sets the path to the material to use for creating new edges.
		[Category("Graph Settings")]
		[Description("The path to the material to use for creating new edges.")]
		public string EdgeMaterialPath
		{
			get => edgeMaterialPath;
			set => edgeMaterialPath = value;
		}

		#endregion Graph Settings

		#endregion Search Graph Settings

		#endregion Navigation Settings
		
		#region Agent Settings
		
		[Foldout("Agent Settings")] [SerializeField]
		EntityType agentEntityType;

		[Category("Agent Settings")]
		[Description("Agent Entity Type.")]
		public EntityType AgentEntityType
		{
			get
			{
				if (!agentEntityType)
					agentEntityType = Resources.Load<EntityType>($"{EntityTypesPath}Agent");
				return agentEntityType;
			}
		}

		[Foldout("Agent Settings")] [SerializeField]
		bool friendlyFire;

		[Category("Agent Settings")]
		[Description("Is friendly fire enabled?")]
		public bool FriendlyFire { get => friendlyFire; set => friendlyFire = value; }

		[Foldout("Agent Settings")] [SerializeField]
		float hitFlashTime = 0.2f;

		[Category("Agent Settings")]
		[Description("How long a flash is displayed when the agent is hit.")]
		public float HitFlashTime { get => hitFlashTime; set => hitFlashTime = value; }

		[Foldout("Agent Settings")] [SerializeField]
		float agentGoalAppraisalUpdateFrequency = 0.5f;

		// A frequency of -1 will disable the feature and a frequency of zero will ensure the
		// feature is updated every agent update.
		[Category("Agent Settings")]
		[Description("The number of times a second an agent 'thinks' about changing strategy.")]
		public float AgentGoalAppraisalUpdateFrequency
		{
			get => agentGoalAppraisalUpdateFrequency;
			set => agentGoalAppraisalUpdateFrequency = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentVisionUpdateFrequency = 4f;

		// A frequency of -1 will disable the feature and a frequency of zero will ensure the
		// feature is updated every agent update.
		[Category("Agent Settings")]
		[Description("The number of times a second an agent updates its vision.")]
		public float AgentVisionUpdateFrequency
		{
			get => agentVisionUpdateFrequency;
			set => agentVisionUpdateFrequency = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentTargetingUpdateFrequency = 1f;

		// A frequency of -1 will disable the feature and a frequency of zero will ensure the
		// feature is updated every update cycle.
		[Category("Agent Settings")]
		[Description("The number of times a second an agent updates its target info.")]
		public float AgentTargetingUpdateFrequency
		{
			get => agentTargetingUpdateFrequency;
			set => agentTargetingUpdateFrequency = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentWeaponSelectionFrequency = 0.5f;

		// A frequency of -1 will disable the feature and a frequency of zero will ensure the
		// feature is updated every update cycle.
		[Category("Agent Settings")]
		[Description("The number of times a second an agent 'thinks' about weapon selection.")]
		public float AgentWeaponSelectionFrequency
		{
			get => agentWeaponSelectionFrequency;
			set => agentWeaponSelectionFrequency = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentMemorySpan = 5f;

		[Category("Agent Settings")]
		[Description("How long (in seconds) an agent's sensory memory persists.")]
		public float AgentMemorySpan { get => agentMemorySpan; set => agentMemorySpan = value; }

		[Foldout("Agent Settings")] [SerializeField]
		float agentFieldOfView = 135f;

		[Category("Agent Settings")]
		[Description("The agent's field of view (in degrees).")]
		public float AgentFieldOfView { get => agentFieldOfView; set => agentFieldOfView = value; }

		[Foldout("Agent Settings")] [SerializeField]
		float agentReactionTime = 0.2f;

		[Category("Agent Settings")]
		[Description("The agent's reaction time (in seconds).")]
		public float AgentReactionTime
		{
			get => agentReactionTime;
			set => agentReactionTime = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentAimPersistenceTime = 1f;

		[Category("Agent Settings")]
		[Description(
			"How long (in seconds) the agent will keep pointing its weapon at its target after the target goes out of view.")]
		public float AgentAimPersistenceTime
		{
			get => agentAimPersistenceTime;
			set => agentAimPersistenceTime = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentAimAccuracy;

		[Category("Agent Settings")]
		[Description(
			"How accurate the agents are at aiming. 0 is very accurate, (the value represents the max deviation in range (in degrees)).")]
		public float AgentAimAccuracy { get => agentAimAccuracy; set => agentAimAccuracy = value; }

		[Foldout("Agent Settings")] [SerializeField]
		int agentMaximumHealth = 100;

		[Category("Agent Settings")]
		[Description("The maximum health of an agent.")]
		public int AgentMaximumHealth
		{
			get => agentMaximumHealth;
			set => agentMaximumHealth = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentHealthGoalTweaker = 0.1f;

		[Category("Agent Settings")]
		[Description("The value used to tweak desirability of the get health goal.")]
		public float AgentHealthGoalTweaker
		{
			get => agentHealthGoalTweaker;
			set => agentHealthGoalTweaker = value;
		}

		//ADDED Flag Goal
		[Foldout("Agent Settings")]
		[SerializeField]
		float agentFlagGoalTweaker = 0.1f;

		[Category("Agent Settings")]
		[Description("The value used to tweak desirability of the get flag goal.")]
		public float AgentFlagGoalTweaker
		{
			get => agentFlagGoalTweaker;
			set => agentFlagGoalTweaker = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentShotgunGoalTweaker = 0.2f;

		[Category("Agent Settings")]
		[Description("The value used to tweak desirability of the get shotgun goal.")]
		public float AgentShotgunGoalTweaker
		{
			get => agentShotgunGoalTweaker;
			set => agentShotgunGoalTweaker = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentRailgunGoalTweaker = 0.05f;

		[Category("Agent Settings")]
		[Description("The value used to tweak desirability of the get railgun goal.")]
		public float AgentRailgunGoalTweaker
		{
			get => agentRailgunGoalTweaker;
			set => agentRailgunGoalTweaker = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentRocketLauncherGoalTweaker = 0.1f;

		[Category("Agent Settings")]
		[Description("The value used to tweak desirability of the get rocket launcher goal.")]
		public float AgentRocketLauncherGoalTweaker
		{
			get => agentRocketLauncherGoalTweaker;
			set => agentRocketLauncherGoalTweaker = value;
		}

		//ADDED Bomb Launcher Goal Tweaker
		[Foldout("Agent Settings")]
		[SerializeField]
		float agentBombLauncherGoalTweaker = 0.1f;

		[Category("Agent Settings")]
		[Description("The value used to tweak desirability of the get bomb launcher goal.")]
		public float AgentBombLauncherGoalTweaker
		{
			get => agentBombLauncherGoalTweaker;
			set => agentBombLauncherGoalTweaker = value;
		}

		[Foldout("Agent Settings")] [SerializeField]
		float agentAggroGoalTweaker = 1.0f;

		[Category("Agent Settings")]
		[Description("The value used to tweak desirability of the attack target goal.")]
		public float AgentAggroGoalTweaker
		{
			get => agentAggroGoalTweaker;
			set => agentAggroGoalTweaker = value;
		}
		
		[Foldout("Agent Settings")] [SerializeField]
		float agentExploreDesirability = 0.1f;

		[Category("Agent Settings")]
		[Description("The fixed value of the agent's preference for exploring.")]
		public float AgentExploreDesirability
		{
			get => agentExploreDesirability;
			set => agentExploreDesirability = value;
		}

		#endregion Agent Settings

		#region Trigger Settings

		#region Health Trigger Settings

		[Foldout("Health Trigger Settings")] [SerializeField]
		EntityType healthEntityType;

		[Category("Health Trigger Settings")]
		[Description("Health Entity Type.")]
		public EntityType HealthEntityType
		{
			get
			{
				if (!healthEntityType)
				{
					//ADDED renamed WoW/Heath to WoW/Health also renamed the Resource in Unity
					healthEntityType = Resources.Load<EntityType>($"{EntityTypesPath}WoW/Health");
				}
				return healthEntityType;
			}
		}

		[Foldout("Health Trigger Settings")] [SerializeField]
		int defaultHealthGiven = 50;

		[Category("Health Trigger Settings")]
		[Description("How much health a health giver trigger gives.")]
		public int DefaultHealthGiven
		{
			get => defaultHealthGiven;
			set => defaultHealthGiven = value;
		}

		[Foldout("Health Trigger Settings")] [SerializeField]
		float healthRespawnDelay = 10f;

		[Category("Health Trigger Settings")]
		[Description("How many seconds before a health giver trigger reactivates itself.")]
		public float HealthRespawnDelay
		{
			get => healthRespawnDelay;
			set => healthRespawnDelay = value;
		}

		#endregion Health Trigger Settings

		//ADDED Flag Trigger Settings
		#region Flag Trigger Settings
		[Foldout("Blue Flag Trigger Settings")]
		[SerializeField]
		EntityType blueFlagEntityType;

		[Category("Blue Flag Trigger Settings")]
		[Description("Blue Flag Entity Type.")]
		public EntityType BlueFlagEntityType
		{
			get
			{
				if (!blueFlagEntityType)
				{
					blueFlagEntityType = Resources.Load<EntityType>($"{EntityTypesPath}WoW/Red"); //Red here because Blues goes for Red's flag
				}
				return blueFlagEntityType;
			}
		}

		//ADDED Flag Trigger Settings
		[Foldout("Red Flag Trigger Settings")]
		[SerializeField]
		EntityType redFlagEntityType;

		[Category("Red Flag Trigger Settings")]
		[Description("Red Flag Entity Type.")]
		public EntityType RedFlagEntityType
		{
			get
			{
				if (!redFlagEntityType)
				{
					redFlagEntityType = Resources.Load<EntityType>($"{EntityTypesPath}WoW/Blue"); //Blue here because Red goes for Blues's flag
				}
				return redFlagEntityType;
			}
		}


		/*
		[Foldout("Flag Trigger Settings")]
		[SerializeField]
		EntityType flagEntityType;

		[Category("Flag Trigger Settings")]
		[Description("Flag Entity Type.")]
		public EntityType FlagEntityType
		{
			get
			{
				if (!flagEntityType)
				{
					flagEntityType = Resources.Load<EntityType>($"{EntityTypesPath}WoW/Flag");
				}
				return flagEntityType;
			}
		}

		*/




		[Foldout("Flag Trigger Settings")]
		[SerializeField]
		int defaultFlagGiven = 50;

		[Category("Flag Trigger Settings")]
		[Description("How much flag a flag giver trigger gives.")]
		public int DefaultFlagGiven
		{
			get => defaultFlagGiven;
			set => defaultFlagGiven = value;
		}

		[Foldout("Flag Trigger Settings")]
		[SerializeField]
		float flagRespawnDelay = 10f;

		[Category("Flag Trigger Settings")]
		[Description("How many seconds before a flag giver trigger reactivates itself.")]
		public float FlagRespawnDelay
		{
			get => flagRespawnDelay;
			set => flagRespawnDelay = value;
		}

		#endregion Flag Trigger Settings

		#region Weapon Trigger Settings

		[Foldout("Weapon Trigger Settings")] [SerializeField]
		EntityType shotgunEntityType;

		[Category("Shotgun Trigger Settings")]
		[Description("Shotgun Entity Type.")]
		public EntityType ShotgunEntityType
		{
			get
			{
				if (!shotgunEntityType)
				{
					shotgunEntityType
						= Resources.Load<EntityType>($"{EntityTypesPath}WoW/Shotgun");
				}
				return shotgunEntityType;
			}
		}

		[Foldout("Weapon Trigger Settings")] [SerializeField]
		EntityType rocketLauncherEntityType;

		[Category("Rocket Launcher Trigger Settings")]
		[Description("Rocket Launcher Entity Type.")]
		public EntityType RocketLauncherEntityType
		{
			get
			{
				if (!rocketLauncherEntityType)
				{
					rocketLauncherEntityType
						= Resources.Load<EntityType>($"{EntityTypesPath}WoW/RocketLauncher");
				}
				return rocketLauncherEntityType;
			}
		}

		[Foldout("Weapon Trigger Settings")]
		[SerializeField]
		EntityType bombLauncherEntityType;

		//ADDED for bomb launcher
		[Category("Bomb Launcher Trigger Settings")]
		[Description("Bomb Launcher Entity Type.")]
		public EntityType BombLauncherEntityType
		{
			get
			{
				if (!bombLauncherEntityType)
				{
					bombLauncherEntityType
						= Resources.Load<EntityType>($"{EntityTypesPath}WoW/BombLauncher");
				}
				return bombLauncherEntityType;
			}
		}

		[Foldout("Weapon Trigger Settings")] [SerializeField]
		EntityType railgunEntityType;

		[Category("Railgun Trigger Settings")]
		[Description("Railgun Entity Type.")]
		public EntityType RailgunEntityType
		{
			get
			{
				if (!railgunEntityType)
				{
					railgunEntityType = Resources.Load<EntityType>($"{EntityTypesPath}WoW/Railgun");
				}
				return railgunEntityType;
			}
		}

		[Foldout("Weapon Trigger Settings")] [SerializeField]
		float weaponRespawnDelay = 15f;

		[Category("Weapon Trigger Settings")]
		[Description("How many seconds before a weapon giver trigger reactivates itself.")]
		public float WeaponRespawnDelay
		{
			get => weaponRespawnDelay;
			set => weaponRespawnDelay = value;
		}

		#endregion Weapon Trigger Settings

		#region Sound Trigger Settings

		[Foldout("Sound Trigger Settings")] [SerializeField]
		EntityType soundNotifyEntityType;

		[Category("Sound Trigger Settings")]
		[Description("Sound Notifier Entity Type.")]
		public EntityType SoundNotifierEntityType
		{
			get
			{
				if (!soundNotifyEntityType)
				{
					soundNotifyEntityType = Resources.Load<EntityType>($"{EntityTypesPath}WoW/SoundNotifier");
				}
				return soundNotifyEntityType;
			}
		}

		[Foldout("Sound Trigger Settings")] [SerializeField]
		float soundTriggerLifetime = 2f;

		[Category("Sound Trigger Settings")]
		[Description("The time a sound trigger remains active.")]
		public float SoundTriggerLifetime
		{
			get => soundTriggerLifetime;
			set => soundTriggerLifetime = value;
		}

		[Foldout("Sound Trigger Settings")] [SerializeField]
		bool soundTriggerVisible;

		[Category("Sound Trigger Settings")]
		[Description("Whether a sound trigger is visible (for debugging).")]
		public bool SoundTriggerVisible
		{
			get => soundTriggerVisible;
			set => soundTriggerVisible = value;
		}

		#endregion Sound Trigger Settings

		#endregion Trigger Settings

		#region Weapon System Settings
		
		#region Projectile Settings
		
		[Foldout("Projectile Settings")] [SerializeField]
		float projectileTimeToLive = 4f;

		[Category("Projectile Settings")]
		[Description("The maximum time for a projectile to live (to kill projectiles that escape the play area.")]
		public float ProjectileTimeToLive
		{
			get => projectileTimeToLive;
			set => projectileTimeToLive = value;
		}
		
		#endregion Projectile Settings

		#region Blaster Settings

		[Foldout("Blaster Settings")] [SerializeField]
		float blasterFiringFrequency = 3f;

		[Category("Blaster Settings")]
		[Description("The blaster rate of fire (shots per second).")]
		public float BlasterFiringFrequency
		{
			get => blasterFiringFrequency;
			set => blasterFiringFrequency = value;
		}

		[Foldout("Blaster Settings")] [SerializeField]
		float blasterMaximumSpeed = 25f;

		//TODO: this seems to duplicate <see cref="BoltMaximumSpeed"/>.
		[Category("Blaster Settings")]
		[Description("The maximum speed of blaster (bolt) projectile.")]
		public float BlasterMaximumSpeed
		{
			get => blasterMaximumSpeed;
			set => blasterMaximumSpeed = value;
		}

		[Foldout("Blaster Settings")] [SerializeField]
		int blasterDefaultRounds;

		[Category("Blaster Settings")]
		[Description(
			"The initial number of blaster rounds carried. Not used, a blaster always has ammo.")]
		public int BlasterDefaultRounds
		{
			get => blasterDefaultRounds;
			set => blasterDefaultRounds = value;
		}

		[Foldout("Blaster Settings")] [SerializeField]
		int blasterMaximumRoundsCarried;

		[Category("Blaster Settings")]
		[Description(
			"The maximum number of rounds for the blaster. Not used, a blaster always has ammo.")]
		public int BlasterMaxRoundsCarried
		{
			get => blasterMaximumRoundsCarried;
			set => blasterMaximumRoundsCarried = value;
		}

		[Foldout("Blaster Settings")] [SerializeField]
		float blasterIdealRange = 250f;

		[Category("Blaster Settings")]
		[Description("The ideal range to target for blaster.")]
		public float BlasterIdealRange
		{
			get => blasterIdealRange;
			set => blasterIdealRange = value;
		}

		[Foldout("Blaster Settings")] [SerializeField]
		float blasterSoundRange = 1000f;

		[Category("Blaster Settings")]
		[Description("The distance blaster sound is heard.")]
		public float BlasterSoundRange
		{
			get => blasterSoundRange;
			set => blasterSoundRange = value;
		}
		
		[Foldout("Blaster Settings")] [SerializeField]
		bool useTargetPredictionForBlaster = true;

		[Category("Blaster Settings")]
		[Description("Whether to use target position prediction for the blaster.")]
		public bool UseTargetPredictionForBlaster
		{
			get => useTargetPredictionForBlaster; 
			set => useTargetPredictionForBlaster = value;
		}

		[Foldout("Blaster Settings")] [SerializeField]
		bool useBlaster = true;

		[Category("Blaster Settings")]
		[Description("Whether to allow use of blaster.")]
		public bool UseBlaster { get => useBlaster; set => useBlaster = value; }

		#region Bolt Settings

		[Foldout("Bolt Settings")] [SerializeField]
		int maximumActiveBolts = 5;

		[Category("Bolt Settings")]
		[Description("Maximum number of active bolts allowed at one time.")]
		public int MaximumActiveBolts
		{
			get => maximumActiveBolts;
			set => maximumActiveBolts = value;
		}

		[Foldout("Bolt Settings")] [SerializeField]
		float boltMaximumSpeed = 25f;

		// TODO: this seems to duplicate <see cref="BlasterMaximumSpeed"/>.
		[Category("Bolt Settings")]
		[Description("The maximum speed of blaster (bolt) projectile.")]
		public float BoltMaximumSpeed { get => boltMaximumSpeed; set => boltMaximumSpeed = value; }

		[Foldout("Bolt Settings")] [SerializeField]
		float boltMass = 1f;

		[Category("Bolt Settings")]
		[Description("The mass of bolt projectile.")]
		public float BoltMass { get => boltMass; set => boltMass = value; }

		[Foldout("Bolt Settings")] [SerializeField]
		float boltMaximumForce = 6000.0f;

		[Category("Bolt Settings")]
		[Description("The maximum steering force for bolt projectile.")]
		public float BoltMaximumForce { get => boltMaximumForce; set => boltMaximumForce = value; }

		[Foldout("Bolt Settings")] [SerializeField]
		float boltScale = 2f;

		[Category("Bolt Settings")]
		[Description("The scale of bolt projectile.")]
		public float BoltScale { get => boltScale; set => boltScale = value; }

		[Foldout("Bolt Settings")] [SerializeField]
		float boltDamage = 1f;

		[Category("Bolt Settings")]
		[Description("The damage inflicted by a bolt.")]
		public float BoltDamage { get => boltDamage; set => boltDamage = value; }
		
		[Foldout("Bolt Settings")] [SerializeField]
		Color boltColor = Color.cyan;
		
		// Gets or sets the default bolt color.
		[Category("Bolt Settings")]
		[Description("Default bolt color.")]
		public Color BoltColor
		{
			get
			{
				boltColor.a = BoltAlpha;
				return boltColor;
			}
			set => boltColor = value;
		}

		[Foldout("Bolt Settings")] [SerializeField]
		float boltAlpha = 0.25f;
		
		// Gets or sets the default alpha for bolt color.
		[Category("Bolt Settings")]
		[Description("Default alpha for bolt color.")]
		public float BoltAlpha
		{
			get => boltAlpha;
			set => boltAlpha = value;
		}

		#endregion Bolt Settings

		#endregion Blaster Settings

		#region Rocket Launcher Settings

		[Foldout("Rocket Launcher Settings")] [SerializeField]
		float rocketLauncherFiringFrequency = 1.5f;

		[Category("Rocket Launcher Settings")]
		[Description("The rocket launcher rate of fire (shots per second).")]
		public float RocketLauncherFiringFrequency
		{
			get => rocketLauncherFiringFrequency;
			set => rocketLauncherFiringFrequency = value;
		}

		[Foldout("Rocket Launcher Settings")] [SerializeField]
		int rocketLauncherDefaultRounds = 5;

		[Category("Rocket Launcher Settings")]
		[Description("The initial number of rounds carried.")]
		public int RocketLauncherDefaultRounds
		{
			get => rocketLauncherDefaultRounds;
			set => rocketLauncherDefaultRounds = value;
		}

		[Foldout("Rocket Launcher Settings")] [SerializeField]
		int rocketLauncherMaximumRoundsCarried = 15;

		[Category("Rocket Launcher Settings")]
		[Description("The maximum number of rounds carriable.")]
		public int RocketLauncherMaximumRoundsCarried
		{
			get => rocketLauncherMaximumRoundsCarried;
			set => rocketLauncherMaximumRoundsCarried = value;
		}

		[Foldout("Rocket Launcher Settings")] [SerializeField]
		float rocketLauncherIdealRange = 750f;

		[Category("Rocket Launcher Settings")]
		[Description("The ideal range to target for rocket launcher.")]
		public float RocketLauncherIdealRange
		{
			get => rocketLauncherIdealRange;
			set => rocketLauncherIdealRange = value;
		}

		[Foldout("Rocket Launcher Settings")] [SerializeField]
		float rocketLauncherSoundRange = 2000f;

		[Category("Rocket Launcher Settings")]
		[Description("The distance rocket launcher sound is heard.")]
		public float RocketLauncherSoundRange
		{
			get => rocketLauncherSoundRange;
			set => rocketLauncherSoundRange = value;
		}
		
		[Foldout("Rocket Launcher Settings")] [SerializeField]
		bool useTargetPredictionForRocketLauncher = true;

		[Category("Rocket Launcher Settings")]
		[Description("Whether to use target position prediction for the rocket launcher.")]
		public bool UseTargetPredictionForRocketLauncher
		{
			get => useTargetPredictionForRocketLauncher; 
			set => useTargetPredictionForRocketLauncher = value;
		}

		#region Rocket Settings

		[Foldout("Rocket Settings")] [SerializeField]
		int maximumActiveRockets = 2;

		[Category("Rocket Settings")]
		[Description("Maximum number of active rockets allowed at one time.")]
		public int MaximumActiveRockets
		{
			get => maximumActiveRockets;
			set => maximumActiveRockets = value;
		}

		[Foldout("Rocket Settings")] [SerializeField]
		bool rocketIsHeatSeeking;

		[Category("Rocket Settings")]
		[Description("Whether rockets are heat seeking.")]
		public bool RocketIsHeatSeeking
		{
			get => rocketIsHeatSeeking;
			set => rocketIsHeatSeeking = value;
		}

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketBlastRadius = 20f;

		[Category("Rocket Settings")]
		[Description("The blast radius of exploding rocket projectile.")]
		public float RocketBlastRadius
		{
			get => rocketBlastRadius;
			set => rocketBlastRadius = value;
		}

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketMaximumSpeed = 10f;

		[Category("Rocket Settings")]
		[Description("The maximum speed of rocket projectile.")]
		public float RocketMaximumSpeed
		{
			get => rocketMaximumSpeed;
			set => rocketMaximumSpeed = value;
		}

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketMass = 1f;

		[Category("Rocket Settings")]
		[Description("The mass of rocket projectile.")]
		public float RocketMass { get => rocketMass; set => rocketMass = value; }

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketMaximumForce = 600.0f;

		[Category("Rocket Settings")]
		[Description("The maximum steering force for rocket projectile.")]
		public float RocketMaxForce
		{
			get => rocketMaximumForce;
			set => rocketMaximumForce = value;
		}

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketScale = 1f;

		[Category("Rocket Settings")]
		[Description("The scale of rocket projectile.")]
		public float RocketScale { get => rocketScale; set => rocketScale = value; }

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketDamage = 10f;

		[Category("Rocket Settings")]
		[Description("The damage inflicted by a rocket projectile.")]
		public float RocketDamage { get => rocketDamage; set => rocketDamage = value; }

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketExplosionDecayRate = 40.0f;

		[Category("Rocket Settings")]
		[Description("How fast the explosion occurs (in radius units per sec).")]
		public float RocketExplosionDecayRate
		{
			get => rocketExplosionDecayRate;
			set => rocketExplosionDecayRate = value;
		}
		
		[Foldout("Rocket Settings")] [SerializeField]
		Color rocketColor = Color.yellow;
		
		// Gets or sets the default rocket color.
		[Category("Rocket Settings")]
		[Description("Default rocket color.")]
		public Color RocketColor
		{
			get
			{
				rocketColor.a = RocketAlpha;
				return rocketColor;
			}
			set => rocketColor = value;
		}

		[Foldout("Rocket Settings")] [SerializeField]
		float rocketAlpha = 0.25f;
		
		// Gets or sets the default alpha for rocket color.
		[Category("Rocket Settings")]
		[Description("Default alpha for rocket color.")]
		public float RocketAlpha
		{
			get => rocketAlpha;
			set => rocketAlpha = value;
		}

		#endregion Rocket Settings

		#endregion Rocket Launcher Settings

		#region Bomb Launcher Settings

		[Foldout("Bomb Launcher Settings")]
		[SerializeField]
		float bombLauncherFiringFrequency = 1.5f;

		[Category("Bomb Launcher Settings")]
		[Description("The bomb launcher rate of fire (shots per second).")]
		public float BombLauncherFiringFrequency
		{
			get => bombLauncherFiringFrequency;
			set => bombLauncherFiringFrequency = value;
		}

		[Foldout("Bomb Launcher Settings")]
		[SerializeField]
		int bombLauncherDefaultRounds = 5;

		[Category("Bomb Launcher Settings")]
		[Description("The initial number of rounds carried.")]
		public int BombLauncherDefaultRounds
		{
			get => bombLauncherDefaultRounds;
			set => bombLauncherDefaultRounds = value;
		}

		[Foldout("Bomb Launcher Settings")]
		[SerializeField]
		int bombLauncherMaximumRoundsCarried = 15;

		[Category("Bomb Launcher Settings")]
		[Description("The maximum number of rounds carriable.")]
		public int BombLauncherMaximumRoundsCarried
		{
			get => bombLauncherMaximumRoundsCarried;
			set => bombLauncherMaximumRoundsCarried = value;
		}

		[Foldout("Bomb Launcher Settings")]
		[SerializeField]
		float bombLauncherIdealRange = 750f;

		[Category("Bomb Launcher Settings")]
		[Description("The ideal range to target for bomb launcher.")]
		public float BombLauncherIdealRange
		{
			get => bombLauncherIdealRange;
			set => bombLauncherIdealRange = value;
		}

		[Foldout("Bomb Launcher Settings")]
		[SerializeField]
		float bombLauncherSoundRange = 2000f;

		[Category("Bomb Launcher Settings")]
		[Description("The distance bomb launcher sound is heard.")]
		public float BombLauncherSoundRange
		{
			get => bombLauncherSoundRange;
			set => bombLauncherSoundRange = value;
		}

		[Foldout("Bomb Launcher Settings")]
		[SerializeField]
		bool useTargetPredictionForBombLauncher = true;

		[Category("Bomb Launcher Settings")]
		[Description("Whether to use target position prediction for the bomb launcher.")]
		public bool UseTargetPredictionForBombLauncher
		{
			get => useTargetPredictionForBombLauncher;
			set => useTargetPredictionForBombLauncher = value;
		}

		#region Bomb Settings

		[Foldout("Bomb Settings")]
		[SerializeField]
		int maximumActiveBombs = 2;

		[Category("Bomb Settings")]
		[Description("Maximum number of active bombs allowed at one time.")]
		public int MaximumActiveBombs
		{
			get => maximumActiveBombs;
			set => maximumActiveBombs = value;
		}

		[Foldout("Bomb Settings")]
		[SerializeField]
		bool bombIsHeatSeeking;

		[Category("Bomb Settings")]
		[Description("Whether bombs are heat seeking.")]
		public bool BombIsHeatSeeking
		{
			get => bombIsHeatSeeking;
			set => bombIsHeatSeeking = value;
		}

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombBlastRadius = 20f;

		[Category("Bomb Settings")]
		[Description("The blast radius of exploding bomb projectile.")]
		public float BombBlastRadius
		{
			get => bombBlastRadius;
			set => bombBlastRadius = value;
		}

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombMaximumSpeed = 10f;

		[Category("Bomb Settings")]
		[Description("The maximum speed of bomb projectile.")]
		public float BombMaximumSpeed
		{
			get => bombMaximumSpeed;
			set => bombMaximumSpeed = value;
		}

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombMass = 1f;

		[Category("Bomb Settings")]
		[Description("The mass of bomb projectile.")]
		public float BombMass { get => bombMass; set => bombMass = value; }

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombMaximumForce = 600.0f;

		[Category("Bomb Settings")]
		[Description("The maximum steering force for bomb projectile.")]
		public float BombMaxForce
		{
			get => bombMaximumForce;
			set => bombMaximumForce = value;
		}

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombScale = 1f;

		[Category("Bomb Settings")]
		[Description("The scale of bomb projectile.")]
		public float BombScale { get => bombScale; set => bombScale = value; }

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombDamage = 10f;

		[Category("Bomb Settings")]
		[Description("The damage inflicted by a bomb projectile.")]
		public float BombDamage { get => bombDamage; set => bombDamage = value; }

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombExplosionDecayRate = 40.0f;

		[Category("Bomb Settings")]
		[Description("How fast the explosion occurs (in radius units per sec).")]
		public float BombExplosionDecayRate
		{
			get => bombExplosionDecayRate;
			set => bombExplosionDecayRate = value;
		}

		[Foldout("Bomb Settings")]
		[SerializeField]
		Color bombColor = Color.yellow;

		// Gets or sets the default bomb color.
		[Category("Bomb Settings")]
		[Description("Default bomb color.")]
		public Color BombColor
		{
			get
			{
				bombColor.a = BombAlpha;
				return bombColor;
			}
			set => bombColor = value;
		}

		[Foldout("Bomb Settings")]
		[SerializeField]
		float bombAlpha = 0.25f;

		// Gets or sets the default alpha for bomb color.
		[Category("Bomb Settings")]
		[Description("Default alpha for bomb color.")]
		public float BombAlpha
		{
			get => bombAlpha;
			set => bombAlpha = value;
		}

		#endregion Bomb Settings

		#endregion Bomb Launcher Settings

		#region Railgun Settings

		[Foldout("Railgun Settings")] [SerializeField]
		float railgunFiringFrequency = 1f;

		[Category("Railgun Settings")]
		[Description("The railgun rate of fire (shots per second).")]
		public float RailgunFiringFrequency
		{
			get => railgunFiringFrequency;
			set => railgunFiringFrequency = value;
		}

		[Foldout("Railgun Settings")] [SerializeField]
		int railgunDefaultRounds = 5;

		[Category("Railgun Settings")]
		[Description("The initial number of rounds carried.")]
		public int RailgunDefaultRounds
		{
			get => railgunDefaultRounds;
			set => railgunDefaultRounds = value;
		}

		[Foldout("Railgun Settings")] [SerializeField]
		int railgunMaximumRoundsCarried = 15;

		[Category("Railgun Settings")]
		[Description("The maximum number of rounds carriable.")]
		public int RailgunMaximumRoundsCarried
		{
			get => railgunMaximumRoundsCarried;
			set => railgunMaximumRoundsCarried = value;
		}

		[Foldout("Railgun Settings")] [SerializeField]
		float railgunIdealRange = 2000f;

		[Category("Railgun Settings")]
		[Description("The ideal range to target for railgun.")]
		public float RailgunIdealRange
		{
			get => railgunIdealRange;
			set => railgunIdealRange = value;
		}

		[Foldout("Railgun Settings")] [SerializeField]
		float railgunSoundRange = 2000f;

		[Category("Railgun Settings")]
		[Description("The distance railgun sound is heard.")]
		public float RailgunSoundRange
		{
			get => railgunSoundRange;
			set => railgunSoundRange = value;
		}
		
		[Foldout("Railgun Settings")] [SerializeField]
		bool useTargetPredictionForRailgun = true;

		[Category("Railgun Settings")]
		[Description("Whether to use target position prediction for the railgun.")]
		public bool UseTargetPredictionForRailgun
		{
			get => useTargetPredictionForRailgun; 
			set => useTargetPredictionForRailgun = value;
		}

		#region Slug Settings

		[Foldout("Slug Settings")] [SerializeField]
		int maximumActiveSlugs = 5;

		[Category("Slug Settings")]
		[Description("Maximum number of active slugs allowed at one time.")]
		public int MaximumActiveSlugs
		{
			get => maximumActiveSlugs;
			set => maximumActiveSlugs = value;
		}

		[Foldout("Slug Settings")] [SerializeField]
		float slugMaximumSpeed = 200f;

		[Category("Slug Settings")]
		[Description("The maximum speed of slug projectile.")]
		public float SlugMaximumSpeed { get => slugMaximumSpeed; set => slugMaximumSpeed = value; }

		[Foldout("Slug Settings")] [SerializeField]
		float slugMass = 0.1f;

		[Category("Slug Settings")]
		[Description("The mass of slug projectile.")]
		public float SlugMass { get => slugMass; set => slugMass = value; }

		[Foldout("Slug Settings")] [SerializeField]
		float slugMaximumForce = 1000000.0f;

		[Category("Slug Settings")]
		[Description("The maximum steering force for slug projectile.")]
		public float SlugMaximumForce { get => slugMaximumForce; set => slugMaximumForce = value; }

		[Foldout("Slug Settings")] [SerializeField]
		float slugScale = 1f;

		[Category("Slug Settings")]
		[Description("The scale of slug projectile.")]
		public float SlugScale { get => slugScale; set => slugScale = value; }

		[Foldout("Slug Settings")] [SerializeField]
		float slugPersistence = 0.2f;

		[Category("Slug Settings")]
		[Description("The time slug (and trajectory) remain visible.")]
		public float SlugPersistence { get => slugPersistence; set => slugPersistence = value; }

		[Foldout("Slug Settings")] [SerializeField]
		float slugDamage = 10f;

		[Category("Slug Settings")]
		[Description("The damage inflicted by a slug projectile.")]
		public float SlugDamage { get => slugDamage; set => slugDamage = value; }
		
		[Foldout("Slug Settings")] [SerializeField]
		Color slugColor = Color.white;
		
		// Gets or sets the default slug color.
		[Category("Slug Settings")]
		[Description("Default slug color.")]
		public Color SlugColor
		{
			get
			{
				slugColor.a = SlugAlpha;
				return slugColor;
			}
			set => slugColor = value;
		}

		[Foldout("Slug Settings")] [SerializeField]
		float slugAlpha = 0.25f;
		
		// Gets or sets the default alpha for slug color.
		[Category("Slug Settings")]
		[Description("Default alpha for rocksluget color.")]
		public float SlugAlpha
		{
			get => slugAlpha;
			set => slugAlpha = value;
		}

		#endregion Slug Settings

		#endregion Railgun Settings

		#region Shotgun Settings

		[Foldout("Shotgun Settings")] [SerializeField]
		float shotgunFiringFrequency = 1f;

		[Category("Shotgun Settings")]
		[Description("The shotgun rate of fire (shots per second).")]
		public float ShotgunFiringFrequency
		{
			get => shotgunFiringFrequency;
			set => shotgunFiringFrequency = value;
		}

		[Foldout("Shotgun Settings")] [SerializeField]
		int shotgunDefaultRounds = 15;

		[Category("Shotgun Settings")]
		[Description("The initial number of rounds carried.")]
		public int ShotgunDefaultRounds
		{
			get => shotgunDefaultRounds;
			set => shotgunDefaultRounds = value;
		}

		[Foldout("Shotgun Settings")] [SerializeField]
		int shotgunMaximumRoundsCarried = 30;

		[Category("Shotgun Settings")]
		[Description("The maximum number of rounds carriable.")]
		public int ShotgunMaximumRoundsCarried
		{
			get => shotgunMaximumRoundsCarried;
			set => shotgunMaximumRoundsCarried = value;
		}

		[Foldout("Shotgun Settings")] [SerializeField]
		int shotgunBallsInShell = 10;

		[Category("Shotgun Settings")]
		[Description("The number of balls in a shotgun shell.")]
		public int ShotgunBallsInShell
		{
			get => shotgunBallsInShell;
			set => shotgunBallsInShell = value;
		}

		[Foldout("Shotgun Settings")] [SerializeField]
		float shotgunSpread = 0.15f;

		[Category("Shotgun Settings")]
		[Description("The spread angle (in radians) for shotgun balls.")]
		public float ShotgunSpread { get => shotgunSpread; set => shotgunSpread = value; }

		[Foldout("Shotgun Settings")] [SerializeField]
		float shotgunIdealRange = 250f;

		[Category("Shotgun Settings")]
		[Description("The ideal range to target for shotgun.")]
		public float ShotgunIdealRange
		{
			get => shotgunIdealRange;
			set => shotgunIdealRange = value;
		}

		[Foldout("Shotgun Settings")] [SerializeField]
		float shotgunSoundRange = 3000f;

		[Category("Shotgun Settings")]
		[Description("The distance shotgun sound is heard.")]
		public float ShotgunSoundRange
		{
			get => shotgunSoundRange;
			set => shotgunSoundRange = value;
		}
		
		[Foldout("Shotgun Settings")] [SerializeField]
		bool useTargetPredictionForShotgun = true;

		[Category("Shotgun Settings")]
		[Description("Whether to use target position prediction for the railgun.")]
		public bool UseTargetPredictionForShotgun
		{
			get => useTargetPredictionForShotgun; 
			set => useTargetPredictionForShotgun = value;
		}

		#region Pellet Settings

		[Foldout("Pellet Settings")] [SerializeField]
		int maximumActivePellets = 10;

		[Category("Pellet Settings")]
		[Description("Maximum number of active pellets allowed at one time.")]
		public int MaximumActivePellets
		{
			get => maximumActivePellets;
			set => maximumActivePellets = value;
		}

		[Foldout("Pellet Settings")] [SerializeField]
		float pelletMaximumSpeed = 50f;

		[Category("Pellet Settings")]
		[Description("The maximum speed of pellet projectile.")]
		public float PelletMaximumSpeed
		{
			get => pelletMaximumSpeed;
			set => pelletMaximumSpeed = value;
		}

		[Foldout("Pellet Settings")] [SerializeField]
		float pelletMass = 0.1f;

		[Category("Pellet Settings")]
		[Description("The mass of pellet projectile.")]
		public float PelletMass { get => pelletMass; set => pelletMass = value; }

		[Foldout("Pellet Settings")] [SerializeField]
		float pelletMaximumForce = 6000.0f;

		[Category("Pellet Settings")]
		[Description("The maximum steering force for pellet projectile.")]
		public float PelletMaximumForce
		{
			get => pelletMaximumForce;
			set => pelletMaximumForce = value;
		}

		[Foldout("Pellet Settings")] [SerializeField]
		float pelletScale = 1f;

		[Category("Pellet Settings")]
		[Description("The scale of pellet projectile.")]
		public float PelletScale { get => pelletScale; set => pelletScale = value; }

		[Foldout("Pellet Settings")] [SerializeField]
		float pelletPersistence = 1f;

		[Category("Pellet Settings")]
		[Description("The time pellets (and trajectory) remain visible.")]
		public float PelletPersistence
		{
			get => pelletPersistence;
			set => pelletPersistence = value;
		}

		[Foldout("Pellet Settings")] [SerializeField]
		float pelletDamage = 1f;

		[Category("Pellet Settings")]
		[Description("The damage inflicted by a pellet projectile.")]
		public float PelletDamage { get => pelletDamage; set => pelletDamage = value; }

		[Foldout("Pellet Settings")] [SerializeField]
		Color pelletColor = Color.black;
		
		// Gets or sets the default pellet color.
		[Category("Pellet Settings")]
		[Description("Default pellet color.")]
		public Color PelletColor
		{
			get
			{
				pelletColor.a = PelletAlpha;
				return pelletColor;
			}
			set => pelletColor = value;
		}

		[Foldout("Pellet Settings")] [SerializeField]
		float pelletAlpha = 0.25f;
		
		// Gets or sets the default alpha for pellet color.
		[Category("Pellet Settings")]
		[Description("Default alpha for pellet color.")]
		public float PelletAlpha
		{
			get => pelletAlpha;
			set => pelletAlpha = value;
		}

		#endregion Pellet Settings

		#endregion Shotgun Settings
		
		#region Projectile Type Setings

        #region Bolt Settings

        [Foldout("Bolt Settings")] [SerializeField]
        EntityType boltEntityType;

        [Category("Bolt Settings")]
        [Description("Bolt Entity Type.")]
        public EntityType BoltEntityType
        {
            get
            {
                if (!boltEntityType)
                    boltEntityType = Resources.Load<EntityType>($"{EntityTypesPath}Bolt");
                return boltEntityType;
            }
        }

        #endregion Bolt Settings

        #region Pellet Settings

        [Foldout("Pellet Settings")] [SerializeField]
        EntityType pelletEntityType;

        [Category("Pellet Settings")]
        [Description("Pellet Entity Type.")]
        public EntityType PelletEntityType
        {
            get
            {
                if (!pelletEntityType)
                    pelletEntityType = Resources.Load<EntityType>($"{EntityTypesPath}Pellet");
                return pelletEntityType;
            }
        }

        #endregion Pellet Settings

        #region Rocket Settings

        [Foldout("Rocket Settings")] [SerializeField]
        EntityType rocketEntityType;

        [Category("Rocket Settings")]
        [Description("Rocket Entity Type.")]
        public EntityType RocketEntityType
        {
            get
            {
                if (!rocketEntityType)
                    rocketEntityType = Resources.Load<EntityType>($"{EntityTypesPath}Rocket");
                return rocketEntityType;
            }
        }

		#endregion Rocket Settings

		#region Bomb Settings

		[Foldout("Bomb Settings")]
		[SerializeField]
		EntityType bombEntityType;

		[Category("Bomb Settings")]
		[Description("Bomb Entity Type.")]
		public EntityType BombEntityType
		{
			get
			{
				if (!bombEntityType)
					bombEntityType = Resources.Load<EntityType>($"{EntityTypesPath}Bomb");
				return bombEntityType;
			}
		}

		#endregion Bomb Settings

		#region Slug Settings

		[Foldout("Slug Settings")] [SerializeField]
        EntityType slugEntityType;

        [Category("Slug Settings")]
        [Description("Slug Entity Type.")]
        public EntityType SlugEntityType
        {
            get
            {
                if (!slugEntityType)
                    slugEntityType = Resources.Load<EntityType>($"{EntityTypesPath}Slug");
                return slugEntityType;
            }
        }

        #endregion Slug Settings

        #endregion Projectile Type Setings
		
		#region Weapon Type Settings

		[Foldout("Weapon Type Settings")] [SerializeField]
		WeaponType blasterWeaponType;

		[Category("Weapon Type Settings")]
		[Description("Blaster Weapon Type.")]
		public WeaponType BlasterWeaponType
		{
			get
			{
				if (!blasterWeaponType)
				{
					blasterWeaponType
						= Resources.Load<WeaponType>($"{WeaponTypesPath}WoW/Blaster");
				}
				return blasterWeaponType;
			}
		}

		[Foldout("Weapon Type Settings")] [SerializeField]
		WeaponType shotgunWeaponType;

		[Category("Weapon Type Settings")]
		[Description("Shotgun Weapon Type.")]
		public WeaponType ShotgunWeaponType
		{
			get
			{
				if (!shotgunWeaponType)
				{
					shotgunWeaponType
						= Resources.Load<WeaponType>($"{WeaponTypesPath}WoW/Shotgun");
				}
				return shotgunWeaponType;
			}
		}

		[Foldout("Weapon Type Settings")] [SerializeField]
		WeaponType rocketLauncherWeaponType;

		[Category("Weapon Type Settings")]
		[Description("Rocket Launcher Weapon Type.")]
		public WeaponType RocketLauncherWeaponType
		{
			get
			{
				if (!rocketLauncherWeaponType)
				{
					rocketLauncherWeaponType
						= Resources.Load<WeaponType>($"{WeaponTypesPath}WoW/RocketLauncher");
				}
				return rocketLauncherWeaponType;
			}
		}

		[Foldout("Weapon Type Settings")]
		[SerializeField]
		WeaponType bombLauncherWeaponType;

		[Category("Weapon Type Settings")]
		[Description("Bomb Launcher Weapon Type.")]
		public WeaponType BombLauncherWeaponType
		{
			get
			{
				if (!bombLauncherWeaponType)
				{
					bombLauncherWeaponType
						= Resources.Load<WeaponType>($"{WeaponTypesPath}WoW/BombLauncher");
				}
				return bombLauncherWeaponType;
			}
		}

		[Foldout("Weapon Type Settings")] [SerializeField]
		WeaponType railgunWeaponType;

		[Category("Weapon Type Settings")]
		[Description("Railgun Weapon Type.")]
		public WeaponType RailgunWeaponType
		{
			get
			{
				if (!railgunWeaponType)
				{
					railgunWeaponType
						= Resources.Load<WeaponType>($"{WeaponTypesPath}WoW/Railgun");
				}
				return railgunWeaponType;
			}
		}

		#endregion Weapon Type Settings

		#endregion Weapon System Settings
		
		#region Decision System Settings

		#region Goal Type Settings

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType adjustRangeGoalType;

		[Category("Goal Type Settings")]
		[Description("Adjust Range Goal Type.")]
		public GoalType AdjustRangeGoalType
		{
			get
			{
				if (!adjustRangeGoalType)
				{
					adjustRangeGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/AdjustRange");
				}
				return adjustRangeGoalType;
			}
		}
		
		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType angularStopImmediatelyGoalType;

		[Category("Goal Type Settings")]
		[Description("Angular Stop Immediately Goal Type.")]
		public GoalType AngularStopImmediatelyGoalType
		{
			get
			{
				if (!angularStopImmediatelyGoalType)
				{
					angularStopImmediatelyGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/AngularStopImmediately");
				}
				return angularStopImmediatelyGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType arriveAtLocationGoalType;

		[Category("Goal Type Settings")]
		[Description("Arrive At Location Goal Type.")]
		public GoalType ArriveAtLocationGoalType
		{
			get
			{
				if (!arriveAtLocationGoalType)
				{
					arriveAtLocationGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/ArriveAtLocation");
				}
				return arriveAtLocationGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType attackTargetGoalType;

		[Category("Goal Type Settings")]
		[Description("Attack Target Goal Type.")]
		public GoalType AttackTargetGoalType
		{
			get
			{
				if (!attackTargetGoalType)
				{
					attackTargetGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/AttackTarget");
				}
				return attackTargetGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType brainGoalType;

		[Category("Goal Type Settings")]
		[Description("Brain Goal Type.")]
		public GoalType BrainGoalType
		{
			get
			{
				if (!brainGoalType)
				{
					brainGoalType = Resources.Load<GoalType>($"{GoalTypesPath}WoW/Brain");
				}
				return brainGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType exploreGoalType;

		[Category("Goal Type Settings")]
		[Description("Explore Goal Type.")]
		public GoalType ExploreGoalType
		{
			get
			{
				if (!exploreGoalType)
				{
					exploreGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/Explore");
				}
				return exploreGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType followPathGoalType;

		[Category("Goal Type Settings")]
		[Description("Follow Path Goal Type.")]
		public GoalType FollowPathGoalType
		{
			get
			{
				if (!followPathGoalType)
				{
					followPathGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/FollowPath");
				}
				return followPathGoalType;
			}
		}
		
		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType getEntityOfTypeGoalType;

		[Category("Goal Type Settings")]
		[Description("Get Entity Of Type Goal Type.")]
		public GoalType GetEntityOfTypeGoalType
		{
			get
			{
				if (!getEntityOfTypeGoalType)
				{
					getEntityOfTypeGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/GetEntityOfType");
				}
				return getEntityOfTypeGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType getEntityWithTypesGoalType;

		[Category("Goal Type Settings")]
		[Description("Get Entity With Types Goal Type.")]
		public GoalType GetEntityWithTypesGoalType
		{
			get
			{
				if (!getEntityWithTypesGoalType)
				{
					getEntityWithTypesGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/GetEntityWithTypes");
				}
				return getEntityWithTypesGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType huntTargetGoalType;

		[Category("Goal Type Settings")]
		[Description("Hunt Target Goal Type.")]
		public GoalType HuntTargetGoalType
		{
			get
			{
				if (!huntTargetGoalType)
				{
					huntTargetGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/HuntTarget");
				}
				return huntTargetGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType moveToLocationGoalType;

		[Category("Goal Type Settings")]
		[Description("Move To Location Goal Type.")]
		public GoalType MoveToLocationGoalType
		{
			get
			{
				if (!moveToLocationGoalType)
				{
					moveToLocationGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/MoveToLocation");
				}
				return moveToLocationGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType seekToLocationGoalType;

		[Category("Goal Type Settings")]
		[Description("Seek To Location Goal Type.")]
		public GoalType SeekToLocationGoalType
		{
			get
			{
				if (!seekToLocationGoalType)
				{
					seekToLocationGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/SeekToLocation");
				}
				return seekToLocationGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType linearAndAngularStopImmediatelyGoalType;

		[Category("Goal Type Settings")]
		[Description("Linear And Angular Stop Immediately Goal Type.")]
		public GoalType LinearAndAngularStopImmediatelyGoalType
		{
			get
			{
				if (!linearAndAngularStopImmediatelyGoalType)
				{
					linearAndAngularStopImmediatelyGoalType
						= Resources.Load<GoalType>(
							$"{GoalTypesPath}WoW/LinearAndAngularStopImmediately");
				}
				return linearAndAngularStopImmediatelyGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType linearStopImmediatelyGoalType;

		[Category("Goal Type Settings")]
		[Description("Linear Immediately Goal Type.")]
		public GoalType LinearStopImmediatelyGoalType
		{
			get
			{
				if (!linearStopImmediatelyGoalType)
				{
					linearStopImmediatelyGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/LinearStopImmediately");
				}
				return linearStopImmediatelyGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType strafeGoalType;

		[Category("Goal Type Settings")]
		[Description("Strafe Goal Type.")]
		public GoalType StrafeGoalType
		{
			get
			{
				if (!strafeGoalType)
				{
					strafeGoalType = Resources.Load<GoalType>($"{GoalTypesPath}WoW/Strafe");
				}
				return strafeGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType traverseEdgeGoalType;

		[Category("Goal Type Settings")]
		[Description("Traverse Edge Goal Type.")]
		public GoalType TraverseEdgeGoalType
		{
			get
			{
				if (!traverseEdgeGoalType)
				{
					traverseEdgeGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/TraverseEdge");
				}
				return traverseEdgeGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType waitForATimeGoalType;

		[Category("Goal Type Settings")]
		[Description("Wait for a time Goal Type.")]
		public GoalType WaitForATimeGoalType
		{
			get
			{
				if (!waitForATimeGoalType)
				{
					waitForATimeGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/WaitForATime");
				}
				return waitForATimeGoalType;
			}
		}

		[Foldout("Goal Type Settings")] [SerializeField]
		GoalType wanderIndefinitelyGoalType;

		[Category("Goal Type Settings")]
		[Description("Wander Indefinitely Goal Type.")]
		public GoalType WanderIndefinitelyGoalType
		{
			get
			{
				if (!wanderIndefinitelyGoalType)
				{
					wanderIndefinitelyGoalType
						= Resources.Load<GoalType>($"{GoalTypesPath}WoW/WanderIndefinitely");
				}
				return wanderIndefinitelyGoalType;
			}
		}

		#endregion Goal Type Settings

		#endregion Decision System Settings
	}
}