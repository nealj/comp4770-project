using System.Collections.Generic;
using GameBrains.Entities;
using GameBrains.Extensions.MonoBehaviours;
using UnityEngine;

namespace GameBrains.GameManagement
{
    public class Respawner : ExtendedMonoBehaviour
    {
        public override void Update()
        {
            base.Update();
            
            bool isSpawnPossible = true;
            
            foreach (ThinkingAgent thinkingAgent in EntityManager.FindAll<ThinkingAgent>())
            {
                // if this agent's status is 'spawning' attempt to resurrect it
                // from an unoccupied spawn point
                if (thinkingAgent.IsSpawning && isSpawnPossible)
                {
                    isSpawnPossible = AttemptToAddAgent(thinkingAgent);
                }
                // if this agent's status is 'inactive' add a grave at its current
                // location then change its status to 'spawning'
                else if (thinkingAgent.IsInactive)
                {
                    // create a grave
                    AddGrave(thinkingAgent.Data.Position);

                    // change its status to spawning
                    thinkingAgent.SetSpawning();
                }

                // if this agent is alive update it.
                else if (thinkingAgent.IsActive)
                {
                          
                }
            }
        }
        
        bool AttemptToAddAgent(ThinkingAgent thinkingAgent)
        {
            List<SpawnPoint> spawnPoints = EntityManager.FindAll<SpawnPoint>();
		
            foreach (SpawnPoint spawnPoint in spawnPoints)
            {
                if (spawnPoint.Color == thinkingAgent.Color && spawnPoint.IsAvailable)
                {
                    Vector3 position = spawnPoint.transform.position;
                    position.y = 1.1f; // could place higher and let drop in with gravity
                    thinkingAgent.Spawn(position);
                    return true;
                }
            }
		
            return false;
        }
        
        void AddGrave(Vector3 position)
        {
        }
    }
}