#if UNITY_EDITOR
using GameBrains.Actuators.Motion.Steering;
using GameBrains.Actuators.Motion.Steering.VelocityBased;
using GameBrains.Armory;
using GameBrains.DecisionSystems.GoalOrientedBehaviour.Goals;
using GameBrains.Editor.Extensions;
using GameBrains.Editor.PropertyDrawers.Utilities;
using GameBrains.Entities.EntityData;
using GameBrains.Memory;
using GameBrains.Motion.Steering.VelocityBased;
using UnityEditor;
using UnityEngine;

namespace GameBrains.Editor.PropertyDrawers
{
    [CustomPropertyDrawer(typeof(StaticData), true)]
    public class StaticDataDrawer : PropertyDrawer
    {
        #region Members and Properties

        StaticData staticData;
        bool showEntityData = true;
        bool showStaticInfo;
        bool showKinematicInfo;
        bool showSteeringInfo;
        bool showSteeringBehaviours = true;
        bool showPathfindingInfo;
        bool showThinkingInfo;
        bool showGoals = true;
        bool showTargetingSystem = true;
        bool showSensoryMemory = true;

        #endregion Members and Properties

        #region OnGUI

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            staticData =
                PropertyDrawerUtilities.GetActualObjectForSerializedProperty<StaticData>(
                    fieldInfo, property);

            if (staticData == null || !staticData.OwnerTransform)
            {
                return;
            }

            showEntityData = EditorGUILayout.Foldout(showEntityData, "Entity Data");

            if (!showEntityData) { return; }

            #region Header Foldouts

            EditorGUI.indentLevel += 1;

            showStaticInfo = EditorGUILayout.Foldout(showStaticInfo, nameof(StaticData));
            if (showStaticInfo) { DrawStaticData(); }

            if (staticData is KinematicData kinematicData)
            {
                showKinematicInfo
                    = EditorGUILayout.Foldout(showKinematicInfo, nameof(KinematicData));
                if (showKinematicInfo) { DrawKinematicData(kinematicData); }
            }
            
            if (staticData is SteeringData steeringData)
            {
                showSteeringInfo
                    = EditorGUILayout.Foldout(showSteeringInfo, nameof(SteeringData));
                if (showSteeringInfo) { DrawSteeringData(steeringData); }
            }

            if (staticData is PathfindingData pathfindingData)
            {
                showPathfindingInfo
                    = EditorGUILayout.Foldout(showPathfindingInfo, nameof(PathfindingData));
                if (showPathfindingInfo) { DrawPathfindingData(pathfindingData); }
            }
            
            if (staticData is ThinkingData thinkingData)
            {
                showThinkingInfo
                    = EditorGUILayout.Foldout(showThinkingInfo, nameof(ThinkingData));
                if (showThinkingInfo) { DrawThinkingData(thinkingData); }
            }

            EditorGUI.indentLevel -= 1;

            #endregion Header Foldouts

            // Constantly updates inspector but grinds
            if (EditorApplication.isPlaying)
            {
                EditorUtility.SetDirty(property.serializedObject.targetObject); 
            }
        }
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return 0;
        }

        #endregion OnGUI

        #region Draw Static Data

        void DrawStaticData()
        {
            EditorGUI.indentLevel += 1;

            staticData.ObstacleLayerMask
                = EditorGUILayoutExtensions.LayerMaskField(
                    "ObstacleLayerMask",
                    staticData.ObstacleLayerMask);

            staticData.Location =
                EditorGUILayoutExtensions.VectorXZField("Location", staticData.Location);

            staticData.Orientation
                = EditorGUILayout.FloatField("Orientation", staticData.Orientation);

            EditorGUILayoutExtensions.VectorXZField("Heading", staticData.HeadingVectorXZ);

            staticData.Radius = EditorGUILayout.FloatField("Radius", staticData.Radius);

            staticData.Height = EditorGUILayout.FloatField("Height", staticData.Height);

            staticData.CenterOffset
                = EditorGUILayoutExtensions.VectorXYZField(
                    "CenterOffset",
                    staticData.CenterOffset);

            EditorGUILayoutExtensions.VectorXYZField(
                "Top",
                staticData.Top);

            EditorGUILayoutExtensions.VectorXYZField(
                "Bottom",
                staticData.Bottom);

            EditorGUILayoutExtensions.VectorXYZField(
                "Center",
                staticData.Center);

            staticData.CloseEnoughDistance
                = EditorGUILayout.FloatField(
                    "CloseEnoughDistance",
                    staticData.CloseEnoughDistance);

            staticData.FarEnoughDistance
                = EditorGUILayout.FloatField(
                    "FarEnoughDistance",
                    staticData.FarEnoughDistance);

            staticData.CloseEnoughAngle
                = EditorGUILayout.FloatField(
                    "CloseEnoughAngle",
                    staticData.CloseEnoughAngle);

            staticData.FarEnoughAngle
                = EditorGUILayout.FloatField(
                    "FarEnoughAngle",
                    staticData.FarEnoughAngle);

            staticData.ClearColor
                = EditorGUILayout.ColorField("ClearColor", staticData.ClearColor);

            staticData.BlockedColor
                = EditorGUILayout.ColorField("BlockedColor", staticData.BlockedColor);

            EditorGUI.indentLevel -= 1;
        }

        #endregion Draw Static Data

        #region Draw Kinematic Data

        void DrawKinematicData(KinematicData kinematicData)
        {
            EditorGUI.indentLevel += 1;

            kinematicData.Velocity =
                EditorGUILayoutExtensions.VectorXZField(new GUIContent("Velocity"),
                    kinematicData.Velocity);

            kinematicData.AngularVelocity
                = EditorGUILayout.FloatField(
                    "AngularVelocity",
                    kinematicData.AngularVelocity);

            kinematicData.Acceleration =
                EditorGUILayoutExtensions.VectorXZField(new GUIContent("Acceleration"),
                    kinematicData.Acceleration);

            kinematicData.AngularAcceleration
                = EditorGUILayout.FloatField(
                    "AngularAcceleration",
                    kinematicData.AngularAcceleration);

            EditorGUILayout.FloatField("Speed", kinematicData.Speed);

            kinematicData.MaximumSpeed
                = EditorGUILayout.FloatField("MaximumSpeed", kinematicData.MaximumSpeed);

            kinematicData.MaximumAngularSpeed
                = EditorGUILayout.FloatField(
                    "MaximumAngularSpeed",
                    kinematicData.MaximumAngularSpeed);

            kinematicData.MaximumAcceleration
                = EditorGUILayout.FloatField(
                    "MaximumAcceleration",
                    kinematicData.MaximumAcceleration);

            kinematicData.MaximumAngularAcceleration
                = EditorGUILayout.FloatField(
                    "MaximumAngularAcceleration",
                    kinematicData.MaximumAngularAcceleration);

            EditorGUI.indentLevel -= 1;
        }

        #endregion Draw Kinematic Data
        
        #region Draw Steering Data

        void DrawSteeringData(SteeringData steeringData)
        {
            EditorGUI.indentLevel += 1;

            steeringData.CombiningMethod
                = (SteeringData.CombiningMethods)EditorGUILayout.EnumPopup(
                    "Combining Method",
                    steeringData.CombiningMethod);

            showSteeringBehaviours
                = EditorGUILayout.Foldout(showSteeringBehaviours, "Steering Behaviours");

            if (showSteeringBehaviours && steeringData.SteeringBehaviours != null)
            {
                foreach (var keyValuePair in steeringData.SteeringBehaviours)
                {
                    GUIStyle windowStyle = new GUIStyle("window")
                    {
                        alignment = TextAnchor.UpperLeft
                    };

                    GUILayout.BeginHorizontal();
                    GUILayout.Space(EditorGUI.indentLevel * 15f);
                    GUILayout.BeginVertical(keyValuePair.Value.GetType().Name, windowStyle);

                    UnityEngine.GUI.enabled = false;

                    EditorGUILayout.TextField("ID", keyValuePair.Key.ToString());
                    
                    DrawSteeringBehaviour(keyValuePair.Value);

                    UnityEngine.GUI.enabled = true;

                    EditorGUILayout.EndVertical();
                    GUILayout.EndHorizontal();
                }
            }

            EditorGUI.indentLevel -= 1;
        }
        
        #region Draw Steering Behaviours

        void DrawSteeringBehaviour<T>(T steeringBehaviour) where T : SteeringBehaviour
        {
            if (steeringBehaviour is LinearStop stop) { DrawLinearStop(stop); }
            if (steeringBehaviour is LinearSlow slow) { DrawLinearSlow(slow); }
            if (steeringBehaviour is Seek seek) { DrawSeek(seek); }
            if (steeringBehaviour is Flee flee) { DrawFlee(flee); }
            if (steeringBehaviour is Arrive arrive) { DrawArrive(arrive); }
            if (steeringBehaviour is Depart depart) { DrawDepart(depart); }
            if (steeringBehaviour is Interpose interpose) { DrawInterpose(interpose); }
            if (steeringBehaviour is Hide hide) { DrawHide(hide); }
            if (steeringBehaviour is Pursue pursue) { DrawPursue(pursue); }
            if (steeringBehaviour is Evade evade) { DrawEvade(evade); }
            if (steeringBehaviour is AvoidObstacles avoidObstacles)
            {
                DrawAvoidObstacles(avoidObstacles);
            }
            if (steeringBehaviour is AvoidWalls avoidWalls) { DrawAvoidWalls(avoidWalls); }

            if (steeringBehaviour is AngularStop stopTurning) { DrawAngularStop(stopTurning); }
            if (steeringBehaviour is AngularSlow slowTurning) { DrawAngularSlow(slowTurning); }
            if (steeringBehaviour is Align align) { DrawAlign(align); }
            if (steeringBehaviour is AngularArrive arriveOrientation)
            {
                DrawArriveOrientation(arriveOrientation);
            }
            if (steeringBehaviour is Face face) { DrawFace(face); }
            if (steeringBehaviour is FaceHeading faceHeading) { DrawFaceHeading(faceHeading); }

            if (steeringBehaviour is Wander wander) { DrawWander(wander); }
        }

        void DrawLinearStop(LinearStop linearStop)
        {
            EditorGUILayout.ToggleLeft("Linear Stop Active", linearStop.LinearStopActive);
        }

        void DrawLinearSlow(LinearSlow linearSlow)
        {
            EditorGUILayout.ToggleLeft("Linear Slow Active", linearSlow.LinearSlowActive);
            EditorGUILayout.FloatField("Slow Enough Linear Speed", linearSlow.SlowEnoughLinearSpeed);
            EditorGUILayout.FloatField("Linear Drag", linearSlow.LinearDrag);
        }

        void DrawSeek(Seek seek)
        {
            EditorGUILayout.ToggleLeft("Seek Active", seek.SeekActive);
            EditorGUILayoutExtensions.VectorXZField("Target Location", seek.TargetLocation);
            EditorGUILayout.FloatField("Close Enough Distance", seek.CloseEnoughDistance);
        }

        void DrawFlee(Flee flee)
        {
            EditorGUILayout.ToggleLeft("Flee Active", flee.FleeActive);
            EditorGUILayout.FloatField("Escape Distance", flee.EscapeDistance);
        }

        void DrawArrive(Arrive arrive)
        {
            EditorGUILayout.ToggleLeft("Arrive Active", arrive.ArriveActive);
            EditorGUILayout.FloatField("Braking Distance", arrive.BrakingDistance);
        }

        void DrawDepart(Depart depart)
        {
            EditorGUILayout.ToggleLeft("Depart Active", depart.DepartActive);
            EditorGUILayout.FloatField("Braking Distance", depart.BrakingDistance);
        }

        void DrawInterpose(Interpose interpose)
        {
            EditorGUILayoutExtensions.VectorXZField("First Location", interpose.FirstLocation);
            EditorGUILayoutExtensions.VectorXZField("Second Location", interpose.SecondLocation);
        }

        void DrawHide(Hide hide)
        {
            EditorGUILayoutExtensions.VectorXZField("Threat Location", hide.OtherTargetLocation);
        }

        void DrawPursue(Pursue pursue)
        {
            EditorGUILayout.ToggleLeft("Pursue Active", pursue.PursueActive);
            EditorGUILayoutExtensions.VectorXZField("Target Location", pursue.OtherTargetLocation);
        }

        void DrawEvade(Evade evade)
        {
            EditorGUILayout.ToggleLeft("Evade Active", evade.EvadeActive);
            EditorGUILayout.FloatField("Escape Distance", evade.EscapeDistance);
        }

        void DrawAvoidObstacles(AvoidObstacles avoidObstacles)
        {
            EditorGUILayout.FloatField("Force Multiplier", avoidObstacles.ForceMultiplier);
            EditorGUILayout.FloatField("Lookahead Multiplier", avoidObstacles.LookAheadMultiplier);
        }

        void DrawAvoidWalls(AvoidWalls avoidWalls)
        {
            EditorGUILayout.IntField("Feeler Count", AvoidWalls.FeelerCount);
            EditorGUILayout.FloatField("Force Multiplier", avoidWalls.ForceMultiplier);
            EditorGUILayout.FloatField("Lookahead Multiplier", avoidWalls.LookAheadMultiplier);
        }

        void DrawAngularStop(AngularStop angularStop)
        {
            EditorGUILayout.ToggleLeft("Angular Stop Active", angularStop.AngularStopActive);
        }

        void DrawAngularSlow(AngularSlow angularSlow)
        {
            EditorGUILayout.ToggleLeft("Angular Slow Active", angularSlow.AngularSlowActive);
            EditorGUILayout.FloatField("Slow Enough Angular Velocity", angularSlow.SlowEnoughAngularVelocity);
            EditorGUILayout.FloatField("Angular Drag", angularSlow.AngularDrag);
        }

        void DrawAlign(Align align)
        {
            EditorGUILayout.ToggleLeft("Align Active", align.AlignActive);
            EditorGUILayout.FloatField("Target Orientation", align.TargetOrientation);
            EditorGUILayout.FloatField("Close Enough Angle", align.CloseEnoughAngle);
        }

        void DrawArriveOrientation(AngularArrive angularArrive)
        {
            EditorGUILayout.ToggleLeft(
                "Arrive Orientation Active",
                angularArrive.AngularArriveActive);
            EditorGUILayout.FloatField("Braking Angle", angularArrive.BrakingAngle);
        }

        void DrawFace(Face face)
        {
            EditorGUILayout.ToggleLeft("Face Active", face.FaceActive);
            EditorGUILayoutExtensions.VectorXZField("Target Location", face.TargetLocation);
        }

        void DrawFaceHeading(FaceHeading faceHeading)
        {
            EditorGUILayout.ToggleLeft("Face Heading Active", faceHeading.FaceHeadingActive);
        }

        void DrawWander(Wander wander)
        {
            DrawSteeringBehaviour(wander.Move);
            DrawSteeringBehaviour(wander.Look);
            EditorGUILayout.FloatField("Wander Circle Radius", wander.WanderCircleRadius);
            EditorGUILayout.FloatField("Wander Circle Offset", wander.WanderCircleOffset);
            EditorGUILayout.FloatField("MaximumSlideDegrees", wander.MaximumSlideDegrees);
            EditorGUILayout.FloatField(
                "Wander Close Enough Distance",
                wander.WanderCloseEnoughDistance);
            if (wander.WanderStopLocation.HasValue)
            {
                EditorGUILayoutExtensions.VectorXZField(
                    "Wander Stop Location",
                    wander.WanderStopLocation.Value);
            }
        }

        #endregion Draw Steering Behaviours

        #endregion Draw Steering Data

        #region Draw Pathfinding Data

        void DrawPathfindingData(PathfindingData pathfindingData)
        {
            EditorGUI.indentLevel += 1;
            
            pathfindingData.ShowPath
                = EditorGUILayout.ToggleLeft(
                    "ShowPath",
                    pathfindingData.ShowPath);
            
            pathfindingData.SmoothPath
                = EditorGUILayout.ToggleLeft(
                    "SmoothPath",
                    pathfindingData.SmoothPath);

            pathfindingData.ShowClosestNodeVisualizer
                = EditorGUILayout.ToggleLeft(
                    "ShowClosestNodeVisualizer",
                    pathfindingData.ShowClosestNodeVisualizer);

            pathfindingData.ShowClosestNodeVisualizerOnlyWhenBlocked
                = EditorGUILayout.ToggleLeft(
                    "ShowClosestNodeVisualizerOnlyWhenBlocked",
                    pathfindingData.ShowClosestNodeVisualizerOnlyWhenBlocked);

            pathfindingData.ShowClosestNodeVisualizerCastRadius
                = EditorGUILayout.FloatField(
                    "ShowClosestNodeVisualizerCastRadius",
                    pathfindingData.ShowClosestNodeVisualizerCastRadius);

            pathfindingData.ShowClosestNodeVisualizerClearColor
                = EditorGUILayout.ColorField(
                    "ShowClosestNodeVisualizerClearColor",
                    pathfindingData.ShowClosestNodeVisualizerClearColor);

            pathfindingData.ShowClosestNodeVisualizerBlockedColor
                = EditorGUILayout.ColorField(
                    "ShowClosestNodeVisualizerBlockedColor",
                    pathfindingData.ShowClosestNodeVisualizerBlockedColor);

            pathfindingData.ShowClosestToEntityWithTypeVisualizer
                = EditorGUILayout.ToggleLeft(
                    "ShowClosestToItemVisualizer",
                    pathfindingData.ShowClosestToEntityWithTypeVisualizer);

            pathfindingData.ShowClosestToEntityWithTypeVisualizerOnlyWhenBlocked
                = EditorGUILayout.ToggleLeft(
                    "ShowClosestToItemVisualizerOnlyWhenBlocked",
                    pathfindingData.ShowClosestToEntityWithTypeVisualizerOnlyWhenBlocked);

            pathfindingData.ShowClosestToEntityWithTypeVisualizerCastRadius
                = EditorGUILayout.FloatField(
                    "ShowClosestToItemVisualizerCastRadius",
                    pathfindingData.ShowClosestToEntityWithTypeVisualizerCastRadius);

            pathfindingData.ShowClosestToEntityWithTypeVisualizerClearColor
                = EditorGUILayout.ColorField(
                    "ShowClosestToItemVisualizerClearColor",
                    pathfindingData.ShowClosestToEntityWithTypeVisualizerClearColor);

            pathfindingData.ShowClosestToEntityWithTypeVisualizerBlockedColor
                = EditorGUILayout.ColorField(
                    "ShowClosestToItemVisualizerBlockedColor",
                    pathfindingData.ShowClosestToEntityWithTypeVisualizerBlockedColor);

            pathfindingData.OverlapSphereMaximumColliders
                = EditorGUILayout.IntField(
                    "OverlapSphereMaximumColliders",
                    pathfindingData.OverlapSphereMaximumColliders);

            pathfindingData.OverlapSphereRadius
                = EditorGUILayout.FloatField(
                    "OverlapSphereRadius",
                    pathfindingData.OverlapSphereRadius);

            EditorGUI.indentLevel -= 1;
        }

        #endregion Draw Pathfinding Data
        
        #region Draw Thinking Data

        void DrawThinkingData(ThinkingData thinkingData)
        {
            EditorGUI.indentLevel += 1;

            if (thinkingData.Brain != null)
            {
                if (thinkingData.TargetingSystem != null)
                {
                    showTargetingSystem
                        = EditorGUILayout.Foldout(showTargetingSystem, "Targeting System");

                    if (showTargetingSystem)
                    {
                        DrawTargetingSystem(thinkingData.TargetingSystem);
                    }
                }

                if (thinkingData.SensoryMemory != null)
                {
                    showSensoryMemory
                        = EditorGUILayout.Foldout(showSensoryMemory, "Sensory Memory");

                    if (showSensoryMemory)
                    {
                        DrawSensoryMemory(thinkingData.SensoryMemory);
                    }
                }

                showGoals
                    = EditorGUILayout.Foldout(showGoals, "Goal Stack");

                if (showGoals)
                {
                    for (var goalIndex = 0;
                         goalIndex < thinkingData.Brain.SubgoalStack.Count;
                         goalIndex++)
                    {
                        Goal goal = thinkingData.Brain.SubgoalStack[goalIndex];
                        GUIStyle windowStyle = new GUIStyle("window")
                        {
                            alignment = TextAnchor.UpperLeft
                        };

                        DrawGoal(goal, windowStyle, goalIndex);
                    }
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        void DrawGoal(Goal goal, GUIStyle windowStyle, int index)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Space(EditorGUI.indentLevel * 15f);
            GUILayout.BeginVertical(goal.GetType().Name, windowStyle);

            UnityEngine.GUI.enabled = false;

            EditorGUILayout.TextField("ID", goal.ID.ToString());
            EditorGUILayout.TextField($"{index}: ", goal.ToString());

            if (goal is CompositeGoal compositeGoal)
            {
                for (var subgoalIndex = 0; subgoalIndex < compositeGoal.SubgoalStack.Count; subgoalIndex++)
                {
                    DrawGoal(compositeGoal.SubgoalStack[subgoalIndex], windowStyle, subgoalIndex);
                }
            }
            
            UnityEngine.GUI.enabled = true;

            EditorGUILayout.EndVertical();
            GUILayout.EndHorizontal();
        }

        void DrawTargetingSystem(TargetingSystem targetingSystem)
        {
            if (targetingSystem.Target != null)
            {
                EditorGUILayout.TextField("Target", targetingSystem.Target.ShortName);
            }
            EditorGUILayout.ToggleLeft("Is Target Present", targetingSystem.IsTargetPresent);
            EditorGUILayout.ToggleLeft(
                "Is Target Within Field Of View", 
                targetingSystem.IsTargetWithinFieldOfView);
            EditorGUILayout.ToggleLeft("Is Target Shootable", targetingSystem.IsTargetShootable);
            if (targetingSystem.LastRecordedPosition.HasValue)
            {
                EditorGUILayoutExtensions.VectorXZField(
                    "Last Recorded Position",
                    targetingSystem.LastRecordedPosition.Value);
            }
            EditorGUILayout.FloatField(
                "Time Target Visible",
                targetingSystem.TimeTargetVisible);
            EditorGUILayout.FloatField(
                "Time Target Out Of View",
                targetingSystem.TimeTargetOutOfView);
        }
        
        void DrawSensoryMemory(SensoryMemory sensoryMemory)
        {
            EditorGUILayout.FloatField(
                "Memory Span",
                sensoryMemory.MemorySpan);

            foreach (var kvp in sensoryMemory.MemoryMap)
            {
                var movingAgent = kvp.Key;
                var sensoryMemoryRecord = kvp.Value;

                EditorGUILayout.TextField("Moving Agent", movingAgent.ShortName);
                EditorGUILayout.FloatField(
                    "Time Last Sensed",
                    sensoryMemoryRecord.TimeLastSensed);
                EditorGUILayout.FloatField(
                    "Time Became Visible",
                    sensoryMemoryRecord.TimeBecameVisible);
                EditorGUILayout.FloatField(
                    "Time Last Visible",
                    sensoryMemoryRecord.TimeLastVisible);
                EditorGUILayoutExtensions.VectorXZField(
                    "Last Sensed Position",
                    sensoryMemoryRecord.LastSensedPosition);
                EditorGUILayout.ToggleLeft(
                    "Is Within Field Of View", 
                    sensoryMemoryRecord.IsWithinFieldOfView);
                EditorGUILayout.ToggleLeft(
                    "Is Shootable", 
                    sensoryMemoryRecord.IsShootable);
            }
        }

        #endregion Draw Thinking Data
    }
}
#endif